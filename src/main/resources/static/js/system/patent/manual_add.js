layui.use(['form', 'layer', 'laydate'], function () {
    var form = layui.form,
        $ = layui.jquery,
        laydate = layui.laydate;

    laydate.render({
        elem: "#appDateDto",
        type: "date",
        trigger: 'click'
    });
    laydate.render({
        elem: "#grantDateDto",
        type: "date",
        trigger: 'click'
    });
    laydate.render({
        elem: "#publishDateDto",
        type: "date",
        trigger: 'click'
    });

    //提交
    $("#save").on('click', function () {
        $("#save-submit").click();
    });
    //表单提交
    form.on('submit(save-submit)', function (data) {
        $.ajax({
            url: '/patent/saveManualAdd.html',
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.msg === "专利已存在") {
                    layer.msg('提交失败，专利已存在！', {icon: 2, time: 1500, shade: 0.4}, function () {
                        var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
                        parent.layui.table.reload('patentList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
                        parent.layer.close(index); //再执行关闭
                        // $("#reset").show();
                        // $("#cancel_btn").show();
                    })
                }
                if (res.code === 0) {
                    layer.msg('提交成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
                        parent.layui.table.reload('patentList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
                        parent.layer.close(index); //再执行关闭
                    })
                }
            }
        });
        return false;
    });

    // $("#cancel_btn").on('click', function () {
    //     var index = parent.layer.getFrameIndex(window.name);
    //     parent.layui.table.reload('patentList', {page: {curr: $(".layui_laypage-em").next().html()}})
    //     parent.layer.close(index);
    // })

    $("#addInput1").on('click', function () {
        var html = "<div class='divClass1' style='margin: 10px'><input style='width: 150px;margin-left: 140px'" +
            " name='inventPerson[]' class='layui-input inputClass'></div>";
        $("#div1").append(html);
    })
    $("#deleteInput1").on('click', function () {
        $(".divClass1").last().remove();
    })

    $("#addInput2").on('click', function () {
        var html = "<div class='divClass2 layui-col-space10' style='margin: 10px'><input style='width:" +
            " 150px;margin-left: 140px' name='appPerson[]' class='layui-input'></div>";
        $("#div2").append(html);
    })
    $("#deleteInput2").on('click', function () {
        $(".divClass2").last().remove();
    })

    $("#addInput3").on('click', function () {
        var html = "<div class='divClass3 layui-col-space10' style='margin: 10px'><input style='width:" +
            " 150px;margin-left: 100px' name='proxyPerson[]' class='layui-input'></div>";
        $("#div3").append(html);
    })
    $("#deleteInput3").on('click', function () {
        $(".divClass3").last().remove();
    })

    form.verify({
        personVerify1: function (value, item) {
            if (inputVerify(document.getElementsByName("inventPerson[]"))) {
                return "必填项不能为空";
            }
        },
        personVerify2: function (value, item) {
            if (inputVerify(document.getElementsByName("appPerson[]"))) {
                return "必填项不能为空";
            }
        },
        personVerify3: function (value, item) {
            if (inputVerify(document.getElementsByName("proxyPerson[]"))) {
                return "必填项不能为空";
            }
        }
    });

    function inputVerify(inputName) {
        var j = 0;
        var inputValue = '';
        for (var i = 0; i < inputName.length; i++) {
            inputValue = inputName[i].value;
            if (/^\s+$/g.test(inputValue)) {
                inputValue = inputValue.replace(/(^\s*)|(\s*$)/g, '');
            }
            if (inputValue === '') {
                j++;
            }
        }
        if (j === inputName.length) {
            return true;
        }
    }
});