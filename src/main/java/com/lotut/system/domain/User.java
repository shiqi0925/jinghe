package com.lotut.system.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *  
 * @date 2020/3/4
 */
public class User implements Serializable {
    private static final long serialVersionUID = 8621574906385264739L;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 姓名
     */
    private String name;
    
    /**
     * 姓名
     */
    private Long websiteId;  
    
    /**
     * 密码
     */
    private String password;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 电话
     */
    private String phone;
    /**
     * 创建时间
     */
    private LocalDateTime joinDate;
    /**
     * 显式密码
     */
    private String visiblePassword;
    /**
     * qq
     */
    private String qq;
    /**
     * 微信
     */
    private String weChat;
    /**
     * 头像地址
     */
    private String avatarUrl;
    /**
     * vip等级
     */
    private Integer vip;
    /**
     * 客服
     */
    private String customer;
    /**
     * 来源
     */
    private String sources;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDateTime getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(LocalDateTime joinDate) {
        this.joinDate = joinDate;
    }

    public String getVisiblePassword() {
        return visiblePassword;
    }

    public void setVisiblePassword(String visiblePassword) {
        this.visiblePassword = visiblePassword;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWeChat() {
        return weChat;
    }

    public void setWeChat(String weChat) {
        this.weChat = weChat;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Integer getVip() {
        return vip;
    }

    public void setVip(Integer vip) {
        this.vip = vip;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getSources() {
        return sources;
    }

    public void setSources(String sources) {
        this.sources = sources;
    }

	public Long getWebsiteId() {
		return websiteId;
	}

	public void setWebsiteId(Long websiteId) {
		this.websiteId = websiteId;
	}
    
}
