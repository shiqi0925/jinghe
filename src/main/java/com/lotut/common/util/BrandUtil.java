package com.lotut.common.util;

 
import com.lotut.common.util.aes.AESUtil;
import com.lotut.common.constants.PatternConstants;
 
import java.time.LocalDateTime;
 

/**
 * 搜索工具类
 *
 * @author shiqi
 * @date 2019/5/11.
 */
public class BrandUtil {
   
    /**
     * 获得远程图片
     *
     * @param str
     * @return
     */
    public static String getOnlineImg(String appNo,LocalDateTime appDate) {
    	String secret = AESUtil.encryptParam(appNo, LocalDateTimeUtil.formatLocalDateTime2Str(appDate));
        //System.out.println("secret"+secret);
        String token= PatternConstants.PATTERN_ADDRESS+secret;
    	return token;
    }
    public static String getLocalImg(String appNo) {
    	String path="";//如果8位数。取前四位
		long number;
		number=Integer.valueOf(appNo);
		if(number<9999999) {
			path = appNo.substring(0, 3);
		}else if (number>9999999) {
			path = appNo.substring(0, 4);
		}
		String imgPath = "http://images.tmsearch.com.cn/"+path+"/"+appNo+".jpg";
		return imgPath;
    }
    
}
