layui.use(['form', 'layer'], function () {
    var form = layui.form
    layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    var id = $('#patentId').val();
    var appUserId = $('#appUserId').val();
    // 审批通过
    form.on("submit(pass)", function (data) {
        var data = {
            id: id,
            appUserId: appUserId,
            supportStatus: 2,
            approvalAmount: data.field.approvalAmount
        };
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/grant/approval.html",
            type: "post",
            data: data,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("操作成功");
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        parent.document.getElementById("searchBtn").click();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    // 驳回申请
    form.on("submit(rejected)", function (data) {
        var data = {
            id: id,
            appUserId: appUserId,
            supportStatus: 3,
            approvalAmount: data.field.approvalAmount
        };
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/grant/approval.html",
            type: "post",
            data: data,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("操作成功");
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        parent.document.getElementById("searchBtn").click();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    // 视为放弃
    form.on("submit(abandon)", function (data) {
        var data = {
            id: id,
            appUserId: appUserId,
            supportStatus: 4,
            approvalAmount: data.field.approvalAmount
        };
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/grant/approval.html",
            type: "post",
            data: data,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("操作成功");
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        parent.document.getElementById("searchBtn").click();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    // 资助完成
    form.on("submit(success)", function (data) {
        var data = {
            id: id,
            appUserId: appUserId,
            supportStatus: 5,
            approvalAmount: data.field.approvalAmount
        };
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/grant/approval.html",
            type: "post",
            data: data,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("操作成功");
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        parent.document.getElementById("searchBtn").click();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    $("#cancel_btn").click(function () {
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    });

    // 身份证正面预览
    $("#identityCardFrontPathBtn").on("click", function () {
        var identityCardFrontPath = $('#identityCardFrontPath').val();
        previewImg(identityCardFrontPath);
    });
    // 身份证反面预览
    $("#identityCardBackPathBtn").on("click", function () {
        var identityCardBackPath = $('#identityCardBackPath').val();
        previewImg(identityCardBackPath);
    });
    // 证件图片预览
    $("#certificatePathBtn").on("click", function () {
        var certificatePath = $('#certificatePath').val();
        previewImg(certificatePath);
    });

    // 图片预览
    function previewImg(imgSrc) {
        if (imgSrc == null || imgSrc == '') {
            layer.msg('暂无图片');
        } else {
            //页面层
            layer.open({
                title: "预览",
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['30%', '60%'], //宽高
                shadeClose: true, //开启遮罩关闭
                end: function (index, layero) {
                    return false;
                },
                content: '<div style="text-align:center"><img src="' + imgSrc + '" /></div>'
            });
        }
    }
})

