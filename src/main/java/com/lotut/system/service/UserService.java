package com.lotut.system.service;

import com.lotut.common.dto.ReturnResult;
import com.lotut.system.domain.User;

/**
 * @author xwj
 * @date 2020/3/4
 */
public interface UserService {
    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    ReturnResult login(String username, String password);
    ReturnResult websiteLogin(String username, String password, String sources);

    /**
     * 根据用户名查询用户
     *
     * @param username
     * @return
     */
    User findByUsername(String username);
    /**
     * 根据用户名和来源查询用户
     *
     * @param username
     * @return
     */
    User findByUsernameSource(String username, String sources);

    
    /**
     * 保存用户
     *
     * @param user
     * @return
     */
    int save(User user);
}
