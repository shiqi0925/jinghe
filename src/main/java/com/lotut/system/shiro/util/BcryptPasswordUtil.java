package com.lotut.system.shiro.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mindrot.jbcrypt.BCrypt;

import java.util.regex.Pattern;

/**
 * 模拟 spring security密码加密
 *
 * @author Wang JunJie
 * @date 2018/9/11 11:28.
 */
public class BcryptPasswordUtil {
    private static Pattern BCRYPT_PATTERN = Pattern.compile("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}");
    private static final Log logger = LogFactory.getLog(BcryptPasswordUtil.class);

    public static String encode(CharSequence rawPassword) {
        String salt = BCrypt.gensalt();
        return BCrypt.hashpw(rawPassword.toString(), salt);
    }

    public static boolean matches(CharSequence rawPassword, String encodedPassword) {
        if (encodedPassword != null && encodedPassword.length() != 0) {
            if (!BCRYPT_PATTERN.matcher(encodedPassword).matches()) {
                logger.warn("Encoded password does not look like BCrypt");
                return false;
            } else {
                return BCrypt.checkpw(rawPassword.toString(), encodedPassword);
            }
        } else {
            logger.warn("Empty encoded password");
            return false;
        }
    }
}
