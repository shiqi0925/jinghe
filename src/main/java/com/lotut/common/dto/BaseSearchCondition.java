/*
 * Project: ipcommunity
 * File: BaseSearchCondition.java
 * Date: 2018/7/19
 * Copyright 2018 
 */
package com.lotut.common.dto;

import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * @author Administrator
 * @date 2018/7/19 9:59
 */
public class BaseSearchCondition implements Serializable {
    private static final long serialVersionUID = 8273800701051690401L;
    /**
     * 搜索关键字
     */
    private String keyword;
    /**
     * 当前页
     */
    private Integer page;
    /**
     * 每页数据条数
     */
    private Integer limit;

    public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
        if (StringUtils.isEmpty(keyword)){
            this.keyword=null;
        }else{
            this.keyword = keyword.trim();
        }
	}

	public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public int getPageIndex() {
        return (this.getPage() - 1) * this.getLimit();
    }

    @Override
    public String toString() {
        return "BaseSearchCondition{" +
                "keyword='" + keyword + '\'' +
                ", page=" + page +
                ", limit=" + limit +
                '}';
    }


}
