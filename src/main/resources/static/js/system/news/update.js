layui.use(['form', 'layer', 'upload'], function () {
    var form = layui.form
    layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;
    var upload = layui.upload;

    // 监听一级频道选择事件
    form.on('select(parentChannel)', function (data) {
        var value = data.value;
        $.ajax({
            url: "/website/channel/secondList",
            type: "get",
            data: {parentId: value},
            success: function (res) {
                var data = res.data;
                $('#secondChannel').empty();
                for (var len = data.length, i = 0; i < len; i++) {
                    $('#secondChannel').append("<option value='" + data[i].id + "'>" + data[i].name + "</option>")
                }
                form.render();
            }
        });
    })

    // 上传封面图片
    var uploadThumbnail = upload.render({
        elem: '#thumbnailBtn'
        , field: 'img'
        , size: 1048576
        , accept: 'images'
        , url: '/ueditor/uploadImg'
        , before: function (obj) {
        }
        , done: function (res) {
            if (res.state == 'SUCCESS') {
                layer.msg('上传成功');
                $('#thumbnailPath').val(res.url);
                $('#slideshowFlagTrue').attr("checked", "checked");
                $('#thumbnailImg').attr('src', res.url);
                $('#thumbnailImg').attr('width', '400px');
                $('#thumbnailImg').attr('height', '150px');
            } else {
                layer.msg('上传失败，请重试');
            }
        }
        , error: function () {
            //请求异常回调
        }
    });

    // 上传附件
    var uploadAttachment = upload.render({
        elem: '#attachmentBtn'
        , field: 'file'
        , size: 1048576
        , accept: 'file'
        , url: '/ueditor/uploadFile'
        , before: function (obj) {
        }
        , done: function (res) {
            console.log(res);
            if (res.state == 'SUCCESS') {
                layer.msg('上传成功');
                $('#attachmentPath').val(res.url);
            } else {
                layer.msg('上传失败，请重试');
            }
        }
        , error: function () {
            //请求异常回调
        }
    });

    // 直接保存
    form.on("submit(addNews)", function (data) {
        var content = ""
        ue.ready(function () {
            content = ue.getContent();
        });
        if (content == '') {
            layer.msg("请输入新闻内容");
            return false;
        }
        data.field.content = content;
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/news/save",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("新闻保存成功！");
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    // 发布新闻
    form.on("submit(publishNews)", function (data) {
        //弹出loading
        data.field.status = true;
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/news/save",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("新闻发布成功！");
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    $("#cancel_btn").click(function () {
        layer.closeAll("iframe");
        //刷新父页面
        parent.location.reload();
    });
})