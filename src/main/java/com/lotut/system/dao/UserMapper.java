package com.lotut.system.dao;

import com.lotut.system.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author xwj
 * @date 2020/3/5
 */
@Mapper
@Repository
public interface UserMapper {
    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    User findByUsername(String username);
    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    User findByUsernameSource(@Param("username") String username,@Param("sources") String sources);

    
    /**
     * 保存用户
     * @param user
     * @return
     */
    int save(User user);
}
