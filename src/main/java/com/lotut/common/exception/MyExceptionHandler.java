package com.lotut.common.exception;

import com.lotut.common.dto.ReturnResult;
import com.lotut.common.util.HttpServletUtil;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 统一异常处理
 *
 * @author xwj
 * @date 2019/7/3
 */
@RestControllerAdvice
public class MyExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(MyExceptionHandler.class);

    /**
     * 后台访问权限异常处理
     *
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler({AuthorizationException.class})
    public Object handleAuthorizationException(AuthorizationException e, HttpServletRequest request) {
        if (HttpServletUtil.isAjax(request)) {
            return ReturnResult.errorResult("您没有访问权限");
        }
        return new ModelAndView("error/404");
    }

}
