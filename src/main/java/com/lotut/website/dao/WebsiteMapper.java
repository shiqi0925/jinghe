package com.lotut.website.dao;

import com.lotut.website.domain.Website;
import com.lotut.website.dto.WebsiteDto;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface WebsiteMapper {
    int deleteByPrimaryKey(Long recordId);

    int insert(Website record);

    int insertSelective(Website record);

    Website selectByPrimaryKey(Long recordId);

    Website selectByDomainUrl(String domainUrl);

    int updateByPrimaryKeySelective(Website record);

    int updateByPrimaryKey(Website record);

    Website selectByUserId(Long userId);
    
    List<Website> selectWebsiteByUserId(Long userId);
    
    int insertSelective(WebsiteDto dto);
    
    int selectWebsiteCountByUserId(Long userId); 
}