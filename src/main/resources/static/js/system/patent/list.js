layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        // layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    var seniorSearchCondition = null;

    laydate.render({
        elem: '#searchStartGrantDate',
        type: 'date',
        format: 'yyyy-MM-dd'
    });
    laydate.render({
        elem: '#searchEndGrantDate',
        type: 'date',
        format: 'yyyy-MM-dd'
    });

    laydate.render({
        elem: '#startPublishDate',
        type: 'date',
        format: 'yyyy-MM-dd',
        trigger: 'click'
    });
    laydate.render({
        elem: '#endPublishDate',
        type: 'date',
        format: 'yyyy-MM-dd',
        trigger: 'click'
    });
    laydate.render({
        elem: '#startAppDate',
        type: 'date',
        format: 'yyyy-MM-dd',
        trigger: 'click'
    });
    laydate.render({
        elem: '#endAppDate',
        type: 'date',
        format: 'yyyy-MM-dd',
        trigger: 'click'
    });
    laydate.render({
        elem: '#startGrantDate',
        type: 'date',
        format: 'yyyy-MM-dd',
        trigger: 'click'
    });
    laydate.render({
        elem: '#endGrantDate',
        type: 'date',
        format: 'yyyy-MM-dd',
        trigger: 'click'
    });
    // 标签表格
    table.render({
        elem: '#patentList',
        url: '/patent/search.html',
        method: 'post',
        where: {statisticsStatus: $('#navigationStatisticsStatus').val()},
        height: "full-180",  //-80
        id: "patentList",
        page: true,
        limits: [20, 50, 100],
        limit: 20,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
            {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
            {field: 'lprs', title: '授权日/法律状态', width: 150, align: "center", templet: '#lprsBar'},
            {field: 'appPerson', title: '申请人/发明人', width: 180, align: "center", templet: '#appPersonBar'},
            {field: 'proxyOrg', title: '代理机构/共享人', width: 180, align: "center", templet: '#proxyBar'},
            {field: 'address', title: '详细地址', align: "center", templet: '#addressBar'}
        ]]
    });

    // 普通搜索
    function normalSearch() {
        $('#statisticsStatus').val("");
        $('.status').removeClass("hover");
        seniorSearchCondition = {
            patentTypes: $('#searchPatentType').val(),
            statusCodes: $('#searchStatusCode').val(),
            startGrantDate: $('#searchStartGrantDate').val(),
            endGrantDate: $('#searchEndGrantDate').val(),
            keyword: $('#keyword').val().trim()
        };
        table.render({
            elem: '#patentList',
            url: '/patent/search.html',
            method: 'post',
            height: "full-180",
            where: seniorSearchCondition,
            id: "patentList",
            page: true,
            limits: [20, 50, 100],
            limit: 20,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
                {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
                {field: 'lprs', title: '授权日/法律状态', align: "center", templet: '#lprsBar'},
                {field: 'appPerson', title: '申请人/发明人', width: 240, align: "center", templet: '#appPersonBar'},
                {field: 'proxyOrg', title: '代理机构/共享人', width: 240, align: "center", templet: '#proxyBar'},
                {field: 'address', title: '详细地址', align: "center", templet: '#addressBar'}
            ]]
        });
    }

    // 普通搜索
    $(".search_btn").on("click", function () {
        normalSearch();
    });

    $("#keyword").keypress(function (e) {
        if (e.which == 13) {
            normalSearch();
        }
    });

    // 点击统计查询
    $(".status").on("click", function () {
        $('.status').removeClass("hover");
        $(this).addClass('hover');
        $("#keyword").val('');
        $("#searchPatentType").val('');
        $("#searchStatusCode").val('');
        $("#searchStartGrantDate").val('');
        $("#searchEndGrantDate").val('');
        form.render();
        var statisticsStatus = $(this).attr("status");
        $('#statisticsStatus').val(statisticsStatus);
        seniorSearchCondition = {
            statisticsStatus: statisticsStatus
        };
        table.render({
            elem: '#patentList',
            url: '/patent/search.html',
            method: 'post',
            height: "full-180",
            where: seniorSearchCondition,
            id: "patentList",
            page: true,
            limits: [20, 50, 100],
            limit: 20,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
                {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
                {field: 'lprs', title: '授权日/法律状态', align: "center", templet: '#lprsBar'},
                {field: 'appPerson', title: '申请人/发明人', width: 240, align: "center", templet: '#appPersonBar'},
                {field: 'proxyOrg', title: '代理机构/共享人', width: 240, align: "center", templet: '#proxyBar'},
                {field: 'address', title: '详细地址', align: "center", templet: '#addressBar'}
            ]]
        });
    })

    // 高级检索弹窗
    $("#seniorSearch").on("click", function () {
        $('#statisticsStatus').val("");
        var index = layui.layer.open({
            title: '高级检索',
            type: 1,
            area: ['60%', '100%'],
            content: $("#seniorSearchForm"),
            success: function (layero, index) {
                $('#seniorSearchForm')[0].reset();
                $("#keyword").val('');
                $("#searchPatentType").val('');
                $("#searchStatusCode").val('');
                $("#searchStartGrantDate").val('');
                $("#searchEndGrantDate").val('');
                $('#statisticsStatus').val("");
                form.render();
                setTimeout(function () {
                    layui.layer.tips('点击此处返回专利列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500);
            },
            shade: 0,
            btn: ['确定', '取消'],
            btnAlign: 'c',
            yes: function (index, layero) {
                layer.close(index);
                $('.status').removeClass("hover");
                $('#seniorSearchForm').hide();
                var patentTypes = [];
                var statusCodes = [];
                $('input[name=patentTypes]:checked').each(function () {
                    patentTypes.push($(this).val());
                });
                $('input[name=statusCodes]:checked').each(function () {
                    statusCodes.push($(this).val());
                });
                seniorSearchCondition = {
                    patentTypes: patentTypes.join(','),
                    statusCodes: statusCodes.join(','),
                    startAppDate: $('#startAppDate').val(),
                    endAppDate: $('#endAppDate').val(),
                    startPublishDate: $('#startPublishDate').val(),
                    endPublishDate: $('#endPublishDate').val(),
                    startGrantDate: $('#startGrantDate').val(),
                    endGrantDate: $('#endGrantDate').val(),
                    appNo: $('#appNo').val().trim(),
                    appPerson: $('#appPerson').val().trim(),
                    inventPerson: $('#inventPerson').val().trim(),
                    address: $('#address').val().trim(),
                    proxyOrg: $('#proxyOrg').val().trim(),
                    existTown: $('input[name="existTown"]:checked').val()
                };
                table.render({
                    elem: '#patentList',
                    url: '/patent/search.html',
                    method: 'post',
                    height: "full-180",
                    where: seniorSearchCondition,
                    id: "patentList",
                    page: true,
                    limits: [20, 50, 100],
                    limit: 20,
                    cols: [[
                        {align: 'center', type: 'checkbox'},
                        {align: 'center', type: 'numbers', title: '序号'},
                        {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
                        {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
                        {field: 'lprs', title: '授权日/法律状态', align: "center", templet: '#lprsBar'},
                        {field: 'appPerson', title: '申请人/发明人', width: 240, align: "center", templet: '#appPersonBar'},
                        {field: 'proxyOrg', title: '代理机构/共享人', width: 240, align: "center", templet: '#proxyBar'},
                        {field: 'address', title: '详细地址', align: "center", templet: '#addressBar'}
                    ]]
                });
            },
            btn2: function (index, layero) {
            },
            cancel: function (layero, index) {
            }
        })
    });

    // 添加选中发明授权资助库
    $("#addSelectGrant").on("click", function () {
        var checkStatus = table.checkStatus('patentList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var patentIds = [];
            $.each(checkStatus.data, function (i, val) {
                patentIds.push(val.id);
            });
            addSelectedGrant(patentIds);
        } else {
            top.layer.msg("请先选择要添加的专利");
        }
    });

    // 添加选中年费资助库
    $("#addSelectAnnualFee").on("click", function () {
        var checkStatus = table.checkStatus('patentList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var patentIds = [];
            $.each(checkStatus.data, function (i, val) {
                patentIds.push(val.id);
            });
            addSelectedAnnualFee(patentIds);
        } else {
            top.layer.msg("请先选择要添加的专利");
        }
    });

    // 添加选中代理机构资助库
    $("#addSelectProxy").on("click", function () {
        var checkStatus = table.checkStatus('patentList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var patentIds = [];
            $.each(checkStatus.data, function (i, val) {
                patentIds.push(val.id);
            });
            addSelectedProxyOrg(patentIds);
        } else {
            top.layer.msg("请先选择要添加的专利");
        }
    });

    // 添加选中发明授权资助库
    function addSelectedGrant(patentIds) {
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/grant/addSelectedPatents.html",
            type: "post",
            data: {patentIds: patentIds.join(",")},
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("添加成功");
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        // top.layer.prompt({
        //     formType: 0,
        //     title: '输入申请金额-发明授权资助库-添加选中'
        // }, function (value, index, elem) {
        //     var appAmount = value.trim();
        //     if (!checkMoney(appAmount)) {
        //         top.layer.msg("请输入有效金额，例如：2000");
        //         return;
        //     } else {
        //         top.layer.close(index);
        //         var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        //         $.ajax({
        //             url: "/support/grant/addSelectedPatents.html",
        //             type: "post",
        //             data: {patentIds: patentIds.join(","), appAmount: appAmount},
        //             success: function (res) {
        //                 if (res.code == 0) {
        //                     setTimeout(function () {
        //                         top.layer.close(index);
        //                         top.layer.msg("添加成功");
        //                     }, 2000);
        //                 } else {
        //                     setTimeout(function () {
        //                         top.layer.msg(res.msg);
        //                     }, 2000);
        //                 }
        //             },
        //         });
        //     }
        // });
    }

    // 添加选中发明年费资助库
    function addSelectedAnnualFee(patentIds) {
        var index = layer.open({
            title: "发明专利年费资助库-添加选中",
            type: 1,
            area: ['600px', '320px'],
            content: $("#annualFeeForm"),
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回专利列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500);
            },
            shade: 0,
            btn: ['确定', '取消'],
            btnAlign: 'c',
            btn1: function (index, layero) {
                // var appAmount = $("#annualFeeAppAmount").val();
                var annualFeeYear = $("#annualFeeYear").val();
                layer.close(index);
                var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
                $.ajax({
                    url: "/support/annualFee/addSelectedPatents.html",
                    type: "post",
                    data: {patentIds: patentIds.join(","), annualFeeYear: annualFeeYear},
                    success: function (res) {
                        if (res.code == 0) {
                            setTimeout(function () {
                                top.layer.close(index);
                                top.layer.msg("添加成功");
                            }, 2000);
                        } else {
                            setTimeout(function () {
                                top.layer.msg(res.msg);
                            }, 2000);
                        }
                    },
                });
                // if (!checkMoney(appAmount)) {
                //     layer.msg("请输入有效金额，例如：2000");
                //     return;
                // } else {
                //     layer.close(index);
                //     var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
                //     $.ajax({
                //         url: "/support/annualFee/addSelectedPatents.html",
                //         type: "post",
                //         data: {patentIds: patentIds.join(","), appAmount: appAmount, annualFeeYear: annualFeeYear},
                //         success: function (res) {
                //             if (res.code == 0) {
                //                 setTimeout(function () {
                //                     top.layer.close(index);
                //                     top.layer.msg("添加成功");
                //                 }, 2000);
                //             } else {
                //                 setTimeout(function () {
                //                     top.layer.msg(res.msg);
                //                 }, 2000);
                //             }
                //         },
                //     });
                // }
            },
            btn2: function (index, layero) {
                layer.close(index);
            },
            cancel: function (layero, index) {
                layer.closeAll();
            }
        });
    }

    // 添加选中代理机构资助库
    function addSelectedProxyOrg(patentIds) {
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/proxyOrg/addSelectedPatents.html",
            type: "post",
            data: {patentIds: patentIds.join(",")},
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("添加成功");
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        // top.layer.prompt({
        //     formType: 0,
        //     title: '输入申请金额-代理机构资助库-添加选中'
        // }, function (value, index, elem) {
        //     var appAmount = value.trim();
        //     if (!checkMoney(appAmount)) {
        //         top.layer.msg("请输入有效金额，例如：2000");
        //         return;
        //     } else {
        //         top.layer.close(index);
        //         var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        //         $.ajax({
        //             url: "/support/proxyOrg/addSelectedPatents.html",
        //             type: "post",
        //             data: {patentIds: patentIds.join(","), appAmount: appAmount},
        //             success: function (res) {
        //                 if (res.code == 0) {
        //                     setTimeout(function () {
        //                         top.layer.close(index);
        //                         top.layer.msg("添加成功");
        //                     }, 2000);
        //                 } else {
        //                     setTimeout(function () {
        //                         top.layer.msg(res.msg);
        //                     }, 2000);
        //                 }
        //             },
        //         });
        //     }
        // });
    }

    // 一键添加发明授权资助库
    $("#addAllGrant").on("click", function () {
        addAllGrant();
    })

    function addAllGrant() {
        var searchCondition = seniorSearchCondition;
        if (searchCondition == null) {
            searchCondition = {};
        }
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/grant/addAllPatents.html",
            type: "post",
            data: searchCondition,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("正在添加，稍候前往资助平台-发明授权资助库刷新");
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        // top.layer.prompt({
        //     formType: 0,
        //     title: '输入申请金额-发明授权资助库-一键添加'
        // }, function (value, index, elem) {
        //     var appAmount = value.trim();
        //     if (!checkMoney(appAmount)) {
        //         top.layer.msg("请输入有效金额，例如：2000");
        //         return;
        //     } else {
        //         top.layer.close(index);
        //         var searchCondition = seniorSearchCondition;
        //         if (searchCondition == null) {
        //             searchCondition = {};
        //         }
        //         searchCondition.appAmount = appAmount;
        //         var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        //         $.ajax({
        //             url: "/support/grant/addAllPatents.html",
        //             type: "post",
        //             data: searchCondition,
        //             success: function (res) {
        //                 if (res.code == 0) {
        //                     setTimeout(function () {
        //                         top.layer.close(index);
        //                         top.layer.msg("正在添加，稍候前往资助平台-发明授权资助库刷新");
        //                     }, 2000);
        //                 } else {
        //                     setTimeout(function () {
        //                         top.layer.msg(res.msg);
        //                     }, 2000);
        //                 }
        //             },
        //         });
        //     }
        // });
    }

    // 一键添加发明专利年费资助库
    $("#addAllAnnualFee").on("click", function () {
        addAllAnnualFee();
    })

    function addAllAnnualFee() {
        var idx = layui.layer.open({
            title: "发明专利年费资助库-一键添加",
            type: 1,
            area: ['600px', '320px'],
            content: $("#annualFeeForm"),
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回专利列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500);
            },
            shade: 0,
            btn: ['确定', '取消'],
            btnAlign: 'c',
            btn1: function (index, layero) {
                var annualFeeYear = $("#annualFeeYear").val();
                var searchCondition = seniorSearchCondition;
                if (searchCondition == null) {
                    searchCondition = {};
                }
                searchCondition.annualFeeYear = annualFeeYear;
                var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
                $.ajax({
                    url: "/support/annualFee/addAllPatents.html",
                    type: "post",
                    data: searchCondition,
                    success: function (res) {
                        if (res.code == 0) {
                            setTimeout(function () {
                                top.layer.close(index);
                                top.layer.msg("正在添加，稍候前往资助平台-发明专利年费资助库刷新");
                            }, 2000);
                        } else {
                            setTimeout(function () {
                                top.layer.msg(res.msg);
                            }, 2000);
                        }
                    },
                });
                // var appAmount = $("#annualFeeAppAmount").val();
                // var annualFeeYear = $("#annualFeeYear").val();
                // if (!checkMoney(appAmount)) {
                //     layer.msg("请输入有效金额，例如：2000");
                //     return;
                // } else {
                //     layer.close(index);
                //     var searchCondition = seniorSearchCondition;
                //     if (searchCondition == null) {
                //         searchCondition = {};
                //     }
                //     searchCondition.appAmount = appAmount;
                //     searchCondition.annualFeeYear = annualFeeYear;
                //     var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
                //     $.ajax({
                //         url: "/support/annualFee/addAllPatents.html",
                //         type: "post",
                //         data: searchCondition,
                //         success: function (res) {
                //             if (res.code == 0) {
                //                 setTimeout(function () {
                //                     top.layer.close(index);
                //                     top.layer.msg("正在添加，稍候前往资助平台-发明专利年费资助库刷新");
                //                 }, 2000);
                //             } else {
                //                 setTimeout(function () {
                //                     top.layer.msg(res.msg);
                //                 }, 2000);
                //             }
                //         },
                //     });
                // }
            },
            btn2: function (index, layero) {
                layer.close(index);
            },
            cancel: function (layero, index) {
                layer.closeAll();
            }
        });
    }

    // 一键添加代理机构资助库
    $("#addAllProxy").on("click", function () {
        addAllProxyOrg();
    })

    function addAllProxyOrg() {
        var searchCondition = seniorSearchCondition;
        if (searchCondition == null) {
            searchCondition = {};
        }
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/proxyOrg/addAllPatents.html",
            type: "post",
            data: searchCondition,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("正在添加，稍候前往资助平台-代理机构资助库刷新");
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        // top.layer.prompt({
        //     formType: 0,
        //     title: '输入申请金额-代理机构资助库-一键添加'
        // }, function (value, index, elem) {
        //     var appAmount = value.trim();
        //     if (!checkMoney(appAmount)) {
        //         top.layer.msg("请输入有效金额，例如：2000");
        //         return;
        //     } else {
        //         top.layer.close(index);
        //         var searchCondition = seniorSearchCondition;
        //         if (searchCondition == null) {
        //             searchCondition = {};
        //         }
        //         searchCondition.appAmount = appAmount;
        //         var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        //         $.ajax({
        //             url: "/support/proxyOrg/addAllPatents.html",
        //             type: "post",
        //             data: searchCondition,
        //             success: function (res) {
        //                 if (res.code == 0) {
        //                     setTimeout(function () {
        //                         top.layer.close(index);
        //                         top.layer.msg("正在添加，稍候前往资助平台-代理机构资助库刷新");
        //                     }, 2000);
        //                 } else {
        //                     setTimeout(function () {
        //                         top.layer.msg(res.msg);
        //                     }, 2000);
        //                 }
        //             },
        //         });
        //     }
        // });
    }

    //列表操作
    table.on('tool(patentList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'update') {
        }
        if (layEvent === 'delete') {
        }
    });

    // 点击乡镇弹窗
    $(document).on('click', '.update-town', function () {
        var id = $(this).attr("patentId");
        var town = $(this).attr("town");
        var idx = layui.layer.open({
            title: "修改乡镇",
            type: 1,
            area: ['600px', '500px'],
            content: $("#updateTownForm"),
            success: function (layero, index) {
                $('#town').val('');
                if (town != null && town != '') {
                    $('#town').val(town);
                }
                form.render('select');
            },
            shade: 0,
            btn: ['确定', '取消'],
            btnAlign: 'c',
            btn1: function (index, layero) {
                layer.confirm('确定修改乡镇吗', {icon: 3, title: '提示信息'}, function (index, row) {
                    $.post("/patent/updateTown.html", {
                        id: id,
                        town: $("#town").val()
                    }, function (data) {
                        if (data.code == 0) {
                            layer.close(index);
                            location.reload();
                        } else {
                            layer.msg(data.msg);
                        }
                    })
                });
            },
            btn2: function (index, layero) {
                layer.close(index);
            },
            cancel: function (layero, index) {
                layer.closeAll();
            }
        });
    });

    // 批量修改乡镇
    $("#batchUpdateTown").on("click", function () {
        var checkStatus = table.checkStatus('patentList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var patentIds = [];
            $.each(checkStatus.data, function (i, val) {
                patentIds.push(val.id);
            });
            var idx = layui.layer.open({
                title: "修改乡镇",
                type: 1,
                area: ['600px', '500px'],
                content: $("#updateTownForm"),
                success: function (layero, index) {
                    form.render();
                },
                shade: 0,
                btn: ['确定', '取消'],
                btnAlign: 'c',
                btn1: function (index, layero) {
                    top.layer.confirm('确定修改乡镇吗', {icon: 3, title: '提示信息'}, function (index, row) {
                        $.post("/patent/batchUpdateTown.html", {
                            patentIds: patentIds.join(','),
                            town: $("#town").val()
                        }, function (data) {
                            if (data.code == 0) {
                                top.layer.close(index);
                                location.reload();
                            } else {
                                top.layer.msg(data.msg);
                            }
                        })
                    });
                },
                btn2: function (index, layero) {
                    layer.close(index);
                },
                cancel: function (layero, index) {
                    layer.closeAll();
                }
            });
        } else {
            top.layer.msg("请先选择要修改的专利");
        }
    });

    // 一键推送
    $("#push").on("click", function () {
        $.get("/user/patent/push.html", {}, function (data) {
            if (data.code == 0) {
                top.layer.msg("推送成功")
            } else {
                top.layer.msg(data.msg);
            }
        })
    });

    // 点击共享人删除
    $(document).on('click', '.share-user', function () {
        var userId = $(this).attr("userId");
        var patentId = $(this).attr("patentId");
        top.layer.confirm('确定删除该共享记录吗', {icon: 3, title: '提示信息'}, function (index, row) {
            $.post("/user/patent/deleteByUserId.html", {
                userId: userId,
                patentId: patentId
            }, function (data) {
                if (data.code == 0) {
                    top.layer.close(index);
                    location.reload();
                } else {
                    top.layer.msg(data.msg);
                }
            })
        });
    });

    //手动添加专利
    $("#addPatent").on("click", function () {
        var index = layui.layer.open({
            title: '添加专利',
            type: 2,
            area: ['100%', '100%'],
            content: '/patent/manualAddPage.html',
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回专利总库', '.layui-layer-setwin .layui-layer-close', {tips: 3});
                }, 500);
            }
        });
    });

    // 构造搜索条件
    function buildSearchCondition() {
        if ($('#searchPatentType').val() != '' || $('#searchStatusCode').val() != '' || $('#searchStartGrantDate').val() != '' ||
            $('#searchEndGrantDate').val() != '' || $('#keyword').val() != '') {
            var condition = {
                patentTypes: $('#searchPatentType').val(),
                statusCodes: $('#searchStatusCode').val(),
                startGrantDate: $('#searchStartGrantDate').val(),
                endGrantDate: $('#searchEndGrantDate').val(),
                keyword: $('#keyword').val().trim()
            };
            return condition;
        }
        var patentTypes = [];
        var statusCodes = [];
        $('input[name=patentTypes]:checked').each(function () {
            patentTypes.push($(this).val());
        });
        $('input[name=statusCodes]:checked').each(function () {
            statusCodes.push($(this).val());
        });
        var condition = {
            patentTypes: patentTypes.join(','),
            statusCodes: statusCodes.join(','),
            startAppDate: $('#startAppDate').val(),
            endAppDate: $('#endAppDate').val(),
            startPublishDate: $('#startPublishDate').val(),
            endPublishDate: $('#endPublishDate').val(),
            startGrantDate: $('#startGrantDate').val(),
            endGrantDate: $('#endGrantDate').val(),
            appNo: $('#appNo').val().trim(),
            appPerson: $('#appPerson').val().trim(),
            inventPerson: $('#inventPerson').val().trim(),
            address: $('#address').val().trim(),
            proxyOrg: $('#proxyOrg').val().trim(),
            existTown: $('input[name="existTown"]:checked').val()
        };
        return condition;
    }

    //导出excel表格
    $(".getCheckData").on("click", function () { //获取选中数据
        var data = table.checkStatus('patentList').data;
        var statisticsStatus = $('#statisticsStatus').val();
        var condition, index;
        if (statisticsStatus !== null && statisticsStatus !== '') {
            condition = {statisticsStatus: statisticsStatus}
        } else {
            condition = buildSearchCondition();
        }
        if (data.length === 0) {
            //为选择数据，导出全部
            $.ajax({
                url: "/patent/exportAll.html",
                type: "post",
                data: condition,
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
            return false;
        } else if (data.length !== 0) {
            var ids = new Array();
            for (var i = 0; i < data.length; i++) {
                ids.push(data[i].id);
            }
            $.ajax({
                url: "/patent/excel.html",
                type: "post",
                data: {"ids": ids.join(',')},
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
        }
    })

    $("#batchDelete").on("click", function () {
        var checkStatus = table.checkStatus('patentList');
        var len = checkStatus.data.length;
        if(len > 0) {
            layer.confirm('专利资助、年费等信息将同步删除，确定删除选中的专利', {icon: 3, title: '提示信息'}, function (index, row) {
                var ids = [];
                var appNos = [];
                $.each(checkStatus.data, function (i, val) {
                    ids.push(val.id);
                    appNos.push(val.appNo);
                });
                $.post("/patent/batchDelete.html", {
                    ids: ids.join(","), appNos: appNos.join(",")
                }, function (data) {
                    layer.msg(data.msg);
                })
            });
        } else {
            layer.msg("请选择要删除的专利");
        }
    });
});