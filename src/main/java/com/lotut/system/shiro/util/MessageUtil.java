package com.lotut.system.shiro.util;

import com.lotut.common.util.ContextUtil;
import org.springframework.context.MessageSource;

/**
 * @author Wang JunJie
 * @date 2018/9/4 15:56.
 */
public class MessageUtil {

    /**
     * 根据消息键和参数 获取消息 委托给spring messageSource
     *
     * @param code 消息键
     * @param args 参数
     */
    public static String message(String code, Object... args){
        MessageSource messageSource = ContextUtil.getBean(MessageSource.class);
        return messageSource.getMessage(code, args,null);
    }
}
