package com.lotut.news.dto;

import com.lotut.common.dto.BaseSearchCondition;

/**
 * 网站新闻搜索条件
 * @author xwj
 * @date 2020/3/11
 */
public class WebsiteNewsSearchCondition extends BaseSearchCondition {

    private static final long serialVersionUID = 2152214602717491416L;

    private Long newsId;

    private Long userId;

    private Long websiteId;

    private Long channelId;

    private  long thumbnailStatus;/** 0 为没有缩略图  1为有缩略图**/
    
    private Boolean slideshowFlag;

    private String startDate;

    private String endDate;

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Boolean getSlideshowFlag() {
        return slideshowFlag;
    }

    public void setSlideshowFlag(Boolean slideshowFlag) {
        this.slideshowFlag = slideshowFlag;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

	public long getThumbnailStatus() {
		return thumbnailStatus;
	}

	public void setThumbnailStatus(long thumbnailStatus) {
		this.thumbnailStatus = thumbnailStatus;
	}
    
}
