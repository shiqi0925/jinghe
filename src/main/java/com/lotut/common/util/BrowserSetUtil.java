package com.lotut.common.util;


import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * @author sq
 * @date 2018/7/9 17:20.
 * 
 */
public class BrowserSetUtil {

    private static final String MSIE = "MSIE";
    private static final String TRIDENT = "Trident";
    private static final String MOZILLA = "Mozilla";
    private static final String EDGE = "Edge";
    public static void browserDownload(String fileName, HttpServletRequest request, HttpServletResponse response) throws IOException{
        //获得浏览器代理信息
        final String userAgent = request.getHeader("USER-AGENT");
        //判断浏览器代理并分别设置响应给浏览器的编码格式
        String finalFileName = null;
        //IE浏览器
        if(StringUtils.contains(userAgent, MSIE)|| StringUtils.contains(userAgent,TRIDENT) || StringUtils.contains(userAgent,EDGE)){
            finalFileName = URLEncoder.encode(fileName,"UTF8");
            //google,火狐浏览器
        }else if(StringUtils.contains(userAgent, MOZILLA)){
            finalFileName = new String(fileName.getBytes(), "ISO8859-1");
        }else{
            //其他浏览器
            finalFileName = URLEncoder.encode(fileName,"UTF8");
        }
        //设置HTTP响应头

        //重置 响应头
        response.reset();
        //告知浏览器下载文件，而不是直接打开，浏览器默认为打开
        response.setContentType("application/x-download");
        //下载文件的名称
        response.addHeader("Content-Disposition" ,"attachment;filename=\"" +finalFileName+ "\"");
    }

}
