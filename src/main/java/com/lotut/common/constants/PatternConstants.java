package com.lotut.common.constants;

/**
 * @author shiqi
 * @date 2019/3/15 11:04
 */
public class PatternConstants {

    /**
     * 商标图样请求地址
     */
    public static final String PATTERN_ADDRESS = "http://60.174.231.53:8888/tradeMark/getTmPattern?q=";
}
