layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        // layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    // 标签表格
    table.render({
        elem: '#patentList',
        url: '/patent/recycle/list.html',
        method: 'post',
        height: "full-80",
        id: "patentList",
        page: true,
        limits: [20, 50, 100],
        limit: 20,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
            {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
            {field: 'lprs', title: '授权日/法律状态', width: 150, align: "center", templet: '#lprsBar'},
            {field: 'appPerson', title: '申请人/发明人', width: 180, align: "center", templet: '#appPersonBar'},
            {field: 'proxyOrg', title: '代理机构/共享人', width: 180, align: "center", templet: '#proxyBar'},
            {field: 'address', title: '详细地址', align: "center"},
            {title: '操作', minWidth: 50, templet: '#optionBar', fixed: "right", align: "center"}
        ]]
    });


    function reload() {
        table.render({
            elem: '#patentList',
            url: '/patent/recycle/list.html',
            method: 'post',
            where: {keyword: $('#keyword').val()},
            height: "full-80",
            id: "patentList",
            page: true,
            limits: [20, 50, 100],
            limit: 20,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
                {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
                {field: 'lprs', title: '授权日/法律状态', width: 150, align: "center", templet: '#lprsBar'},
                {field: 'appPerson', title: '申请人/发明人', width: 180, align: "center", templet: '#appPersonBar'},
                {field: 'proxyOrg', title: '代理机构/共享人', width: 180, align: "center", templet: '#proxyBar'},
                {field: 'address', title: '详细地址', align: "center"},
                {title: '操作', minWidth: 50, templet: '#optionBar', fixed: "right", align: "center"}
            ]]
        });
    }

    // 普通搜索
    $(".search_btn").on("click", function () {
        reload();
    });

    $("#keyword").keypress(function (e) {
        if (e.which == 13) {
            reload();
        }
    });

    function recover(data) {
        $.post("/patent/recycle/recover.html", {
            appNo: data.appNo
        }, function (data) {
            if (data.code == 0) {
                layer.msg("恢复成功");
                location.reload();
            } else {
                layer.msg(data.msg);
            }
        })
    }

    //列表操作
    table.on('tool(patentList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'recover') {
            recover(data);
        }
    });
});