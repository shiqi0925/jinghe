package com.lotut.news.dto;

import com.lotut.news.domain.WebsiteNews;

import java.io.Serializable;
import java.util.List;

/**
 * @author lidc
 * @date 2020-03-12 11:20
 */
public class WebsiteNewsChannelDto implements Serializable {

    private static final long serialVersionUID = 2780192908782828914L;

    /**
     * 自增主键
     */
    private Long channelId;
    /**
     * 一级栏目id
     */
    private Long parentChannelId; 
    
    /**
     * 频道名称
     */
    private String channelName;
    /**
     * 显示状态，false：隐藏，true：显示
     */
    private Boolean channelStatus;
    /**
     * 显示顺序
     */
    private Integer displayOrder;
    /**
     * 是否默认，0：否，1：是
     */
    private Boolean defaultFlag;

    /**
     * 二级频道新闻
     */
    private List<WebsiteNews> channelNews;
    /**
     * 二级频道图片新闻
     */
    private List<WebsiteNews> channelImagesNews;
    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Boolean getChannelStatus() {
        return channelStatus;
    }

    public void setChannelStatus(Boolean channelStatus) {
        this.channelStatus = channelStatus;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Boolean getDefaultFlag() {
        return defaultFlag;
    }

    public void setDefaultFlag(Boolean defaultFlag) {
        this.defaultFlag = defaultFlag;
    }

    public List<WebsiteNews> getChannelNews() {
        return channelNews;
    }

    public void setChannelNews(List<WebsiteNews> channelNews) {
        this.channelNews = channelNews;
    }

	public Long getParentChannelId() {
		return parentChannelId;
	}

	public void setParentChannelId(Long parentChannelId) {
		this.parentChannelId = parentChannelId;
	}

	public List<WebsiteNews> getChannelImagesNews() {
		return channelImagesNews;
	}

	public void setChannelImagesNews(List<WebsiteNews> channelImagesNews) {
		this.channelImagesNews = channelImagesNews;
	}


}
