package com.lotut.news.service.impl;


import com.lotut.news.dao.SeoMapper;
import com.lotut.news.domain.Seo;
import com.lotut.news.service.SeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public class SeoServiceImpl implements SeoService {

    @Autowired
    private SeoMapper seoMapper;

    @Override
    public List<Seo> selectList(){
        return seoMapper.selectList();
    }

	@Override
	public Seo getById(Long id) {
		return seoMapper.getById(id);
	}

	@Override
	public boolean save(Seo seo) {
		return seoMapper.save(seo);
	}

 
  
    
}
