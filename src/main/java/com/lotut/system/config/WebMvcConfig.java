package com.lotut.system.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author xwj
 * @date 2019/9/6
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    /**
     * 新闻图片
     */
    @Value("${news.img.path}")
    private String newsImgPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
/*        registry.addResourceHandler("/website/img/**").addResourceLocations("file:" + websiteImgPath);*/
        registry.addResourceHandler("/news/img/**").addResourceLocations("file:" + newsImgPath);
/*        registry.addResourceHandler("/certification/img/**").addResourceLocations("file:" + certificationImgPath);*/
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    }

}
