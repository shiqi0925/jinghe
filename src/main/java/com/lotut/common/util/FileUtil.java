package com.lotut.common.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

/**
 * @author Administrator
 */
public class FileUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 下载文件
     *
     * @param path     文件路径
     * @param response http响应
     */
    public static void download(String path, HttpServletResponse response) {
        try (InputStream is = new FileInputStream(path);
             OutputStream os = response.getOutputStream()) {
            IOUtils.copy(is, os);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * 上传文件
     *
     * @param file     文件
     * @param basePath 文件存储基本路径
     * @return 文件存储全路径
     */
    public static String upload(MultipartFile file, String basePath) {
        try {
            String filename = rename(file);
            String fullPath = basePath + filename;
            createFileDir(basePath);
            InputStream input = null;
            FileOutputStream fos = null;
            try {
                input = file.getInputStream();
                fos = new FileOutputStream(fullPath);
                IOUtils.copy(input, fos);
            } catch (Exception e) {
                logger.error("上传文件失败:{}", e.getMessage());
                return null;
            } finally {
                IOUtils.closeQuietly(input);
                IOUtils.closeQuietly(fos);
            }
            return fullPath;
        } catch (Exception e) {
            logger.error("上传文件失败:{}", e.getMessage());
            return null;
        }
    }

    /**
     * 缓冲流上传文件
     *
     * @param file     文件
     * @param basePath 上传基本路径
     * @return
     */
    public static String bufferUpload(MultipartFile file, String basePath) {
        BufferedOutputStream bos = null;
        try {
            String filename = rename(file);
            createFileDir(basePath);
            String fullPath = basePath + filename;
            byte[] bytes = file.getBytes();
            bos = new BufferedOutputStream(new FileOutputStream(new File(fullPath)));
            bos.write(bytes);
            return fullPath;
        } catch (Exception e) {
            logger.error("上传文件失败:{}", e.getMessage());
            return null;
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * transfer上传文件
     *
     * @param file     文件
     * @param basePath 上传基本路径
     * @return
     */
    public static String transferUpload(MultipartFile file, String basePath) {
        try {
            String filename = rename(file);
            createFileDir(basePath);
            String fullPath = basePath + filename;
            file.transferTo(new File(fullPath));
            return fullPath;
        } catch (Exception e) {
            logger.error("上传文件失败:{}", e.getMessage());
            return null;
        }
    }

    /**
     * 非阻塞io上传文件
     *
     * @param file
     * @param basePath
     * @return
     */
    public static String nioUpload(MultipartFile file, String basePath) {
        try {
            String filename = rename(file);
            createFileDir(basePath);
            String fullPath = basePath + filename;
            Path target = Files.createFile(Paths.get(fullPath));
            Files.copy(file.getInputStream(), target, StandardCopyOption.REPLACE_EXISTING);
            return fullPath;
        } catch (Exception e) {
            logger.error("上传文件失败:{}", e.getMessage());
            return null;
        }
    }

    /**
     * 非阻塞拷贝文件
     *
     * @param sourcePath 源文件路径
     * @param destPath   目标文件路径
     * @throws Exception
     */
    private static boolean nioTransferCopy(String sourcePath, String destPath) {
        try {
            File source = new File(sourcePath);
            File dest = new File(destPath);
            if (!dest.exists()) {
                dest.createNewFile();
            }
            FileInputStream fis = new FileInputStream(source);
            FileOutputStream fos = new FileOutputStream(dest);
            FileChannel sourceCh = fis.getChannel();
            FileChannel destCh = fos.getChannel();
            destCh.transferFrom(sourceCh, 0, sourceCh.size());
            sourceCh.close();
            destCh.close();
            return true;
        } catch (Exception e) {
            logger.error("文件拷贝失败:{}", e.getMessage());
            return false;
        }
    }

    /**
     * 文件重命名
     *
     * @param file
     * @return
     */
    private static String rename(MultipartFile file) {
        String suffix = FilenameUtils.getExtension(file.getOriginalFilename());
        String filename = UUID.randomUUID().toString() + "." + suffix;
        return filename;
    }

    /**
     * 创建文件目录
     *
     * @param basePath
     */
    private static void createFileDir(String basePath) {
        File fileDir = new File(basePath);
        if (!fileDir.exists())
            fileDir.mkdirs();
    }


}
