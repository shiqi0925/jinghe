package com.lotut.website.domain;

import java.io.Serializable;

/**
 * 用户网站关联实体
 * @author Administrator
 */
public class Website implements Serializable {
    private static final long serialVersionUID = 8364417542793540730L;
    /**
     * 自增主键
     */
    private Long recordId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 站点名
     */
    private String websiteName; 
    
    /**
     * 网站域名地址
     */
    private String domainUrl;

    /**
     * 模板id
     */
    private Long templateId;
    /**
     * 站点类型   默认1 商城系统 ,2代理平台
     */
    private Long siteType;
    /**
     * 模板文件名
     */
    private String templateFileName; 
    /**
     * 网站背景图地址
     */
    private String backgroundPath;
    /**
     * 网站置顶图地址
     */
    private String topImagePath;
    /**
     * 网站置顶图链接
     */
    private String topImageUrl;
    /**
     * es索引名称
     */
    private String esIndex;
    /**
     * es类型名称
     */
    private String esType;

    public Website(Long recordId, Long userId, String username, String domainUrl, String backgroundPath,
                   String topImagePath, String topImageUrl, String esIndex, String esType,Long templateId,String templateFileName) {
        this.recordId = recordId;
        this.userId = userId;
        this.username = username;
        this.domainUrl = domainUrl;
        this.backgroundPath = backgroundPath;
        this.topImagePath = topImagePath;
        this.topImageUrl = topImageUrl;
        this.esIndex = esIndex;
        this.esType = esType;
        this.templateId = templateId;
        this.templateFileName = templateFileName;
    }

    public Website() {
        super();
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getDomainUrl() {
        return domainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        this.domainUrl = domainUrl == null ? null : domainUrl.trim();
    }

    public String getBackgroundPath() {
        return backgroundPath;
    }

    public void setBackgroundPath(String backgroundPath) {
        this.backgroundPath = backgroundPath == null ? null : backgroundPath.trim();
    }

    public String getTopImagePath() {
        return topImagePath;
    }

    public void setTopImagePath(String topImagePath) {
        this.topImagePath = topImagePath == null ? null : topImagePath.trim();
    }

    public String getTopImageUrl() {
        return topImageUrl;
    }

    public void setTopImageUrl(String topImageUrl) {
        this.topImageUrl = topImageUrl == null ? null : topImageUrl.trim();
    }

    public String getEsIndex() {
        return esIndex;
    }

    public void setEsIndex(String esIndex) {
        this.esIndex = esIndex;
    }

    public String getEsType() {
        return esType;
    }

    public void setEsType(String esType) {
        this.esType = esType;
    }

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public String getTemplateFileName() {
		return templateFileName;
	}

	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}

	public Long getSiteType() {
		return siteType;
	}

	public void setSiteType(Long siteType) {
		this.siteType = siteType;
	}

	public String getWebsiteName() {
		return websiteName;
	}

	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

 
    
}