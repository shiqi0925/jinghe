package com.lotut.news.service;

import java.util.List;
import java.util.Map;

import com.lotut.common.dto.Page;
import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.dto.WebsiteNewsDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;

/**
 * @author xwj
 * @date 2020/3/11
 */
public interface WebsiteNewsService {
    /**
     * 分页查询新闻列表
     * @param condition
     * @return
     */
    Page<WebsiteNews> getNewsListByPage(WebsiteNewsSearchCondition condition);

    /**
     * 保存新闻
     * @param newsDto
     * @return
     */
    boolean save(WebsiteNewsDto newsDto);

    /**
     * 查询新闻列表
     * @param condition
     * @return
     */
    List<WebsiteNews> selectList(WebsiteNewsSearchCondition condition);

    /**
     * 根据主键查询新闻详情
     * @param recordId
     * @param websiteId
     * @return
     */
    WebsiteNews selectByPrimaryKey(Long recordId);
    /**
     * 查询新闻内容
     * @param newsId
     * @return
     */
    String getNewsContent(Long newsId);

    /**
     * 查询新闻附件
     * @param newsId
     * @return
     */
    List<Map<String,String>> getNewsAttachments(Long newsId);

    /**
     * 获取上一条或者下一条新闻
     * @param condition
     * @return
     */
    WebsiteNews getBeforeOrNextNews(WebsiteNewsSearchCondition condition);
    /**
     * 查询当前用户新闻列表
     * @param condition
     * @return
     */
    Page<WebsiteNews> getUserNewsListByPage(WebsiteNewsSearchCondition condition);
    /**
     * 获取新闻详情
     * @param websiteId
     * @param newsId
     * @return
     */
    WebsiteNews getNewsById(Long newsId);
    /**
     * 更改新闻显示状态status
     * 0：隐藏，1：显示
     *
     * @param recordId
     * @param status
     * @return
     */
    int updateStatus(Long recordId, Boolean status); 
    
    /**
     * 查询一级栏目下的数据
     * @param condition
     * @return
     */
    Page<WebsiteNews> getNewsListByParentId(long parentId,int pageNo); 
    
    
}
