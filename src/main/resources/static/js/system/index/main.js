//获取系统时间
var newDate = '';
// getLangDate();

//值小于10时，在前面补0
function dateFilter(date) {
    if (date < 10) {
        return "0" + date;
    }
    return date;
}

function getLangDate() {
    var dateObj = new Date(); //表示当前系统时间的Date对象
    var year = dateObj.getFullYear(); //当前系统时间的完整年份值
    var month = dateObj.getMonth() + 1; //当前系统时间的月份值
    var date = dateObj.getDate(); //当前系统时间的月份中的日
    var day = dateObj.getDay(); //当前系统时间中的星期值
    var weeks = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    var week = weeks[day]; //根据星期值，从数组中获取对应的星期字符串
    var hour = dateObj.getHours(); //当前系统时间的小时值
    var minute = dateObj.getMinutes(); //当前系统时间的分钟值
    var second = dateObj.getSeconds(); //当前系统时间的秒钟值
    var timeValue = "" + ((hour >= 12) ? (hour >= 18) ? "晚上" : "下午" : "上午"); //当前时间属于上午、晚上还是下午
    newDate = dateFilter(year) + "年" + dateFilter(month) + "月" + dateFilter(date) + "日 " + " " + dateFilter(hour) + ":" + dateFilter(minute) + ":" + dateFilter(second);
    document.getElementById("nowTime").innerHTML = "亲爱的用户，" + timeValue + "好！ 欢迎使用象山县知识产权监测管理平台。当前时间为： " + newDate + "　" + week;
    setTimeout("getLangDate()", 1000);
}

layui.use(['form', 'element', 'layer', 'jquery', 'carousel',], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        element = layui.element,
        carousel = layui.carousel;
    $ = layui.jquery;
    //上次登录时间【此处应该从接口获取，实际使用中请自行更换】
    $(".loginTime").html(newDate.split("日")[0] + "日</br>" + newDate.split("日")[1]);
    //icon动画
    $(".panel a").hover(function () {
        $(this).find(".layui-anim").addClass("layui-anim-scaleSpring");
    }, function () {
        $(this).find(".layui-anim").removeClass("layui-anim-scaleSpring");
    })
    $(".panel a").click(function () {
        parent.addTab($(this));
    })

    //外部图标
    $.get(iconUrl,function(data){
        $(".outIcons span").text(data.split(".icon-").length-1);
    })
    // 主页请求数据
    var isAdmin = $('#isAdmin').val();
    if(isAdmin) {
        //专利总库
        $.ajax({
            url: "/patent/statistics.html",
            type: "post",
            data: {date: new Date()},
            success: function (res) {
                if (res.code == 0) {
                    var data = res.data;
                    var htmls = $(".patent-ku").text()
                    $("#zlzl").html(data[0]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#yxfmzl").html(data[1]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#zsfmzl").html(data[2]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#qnfmsqzl").html(data[3]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#jnfmsqzl").html(data[4]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                }
            },
        });
        //年费监控
        $.ajax({
            url: "/patent/annual/fee/statistics.html",
            type: "post",
            data: {date: new Date()},
            success: function (res) {
                if (res.code == 0) {
                    var data = res.data;
                    $("#beyond").html(data[0]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>')
                    $("#lateThreeToFiveMonth").html(data[1]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>')
                    $("#lateTwoMonth").html(data[2]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>')
                    $("#lateOneMonth").html(data[3]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>')
                    $("#emergency").html(data[4]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>')
                }
            },
        });
        // //发明授权资助
        // numApi("/support/grant/statistics.html",{date: new Date()},"invent-grant",'<cite>发明授权资助</cite>');
        // //发明年费资助
        // numApi("/support/annualFee/statistics.html",{date: new Date()},"invent-money",'<cite>发明年费资助</cite>');
        // //代理机构资助
        // numApi("/support/proxyOrg/statistics.html",{date: new Date()},"agency-help",'<cite>代理机构资助</cite>');
        // //示范企业申请
        // numApi("/enterpriseApp/statistics.html",{date: new Date()},"company-apply",'<cite>示范企业申请</cite>');
        // //专利组合培育
        // numApi("/patentCultivationApp/statistics.html",{date: new Date()},"patent-portfolio",'<cite>专利组合培育</cite>');
        // function numApi(url,data,ele,datahtml){
        //     $.ajax({
        //         url: url,
        //         type: "post",
        //         data: data,
        //         success: function (res) {
        //             if (res.code == 0) {
        //                 var data = res.data;
        //                 $("."+ele+" a").eq(0).html(!data[0]?'-':data[0]+datahtml)
        //                 $("."+ele+" a").eq(1).html(!data[1]?'-':data[1]+datahtml)
        //                 $("."+ele+" a").eq(2).html(!data[2]?'-':data[2]+datahtml)
        //                 $("."+ele+" a").eq(3).html(!data[3]?'-':data[3]+datahtml)
        //                 $("."+ele+" a").eq(4).html(!data[4]?'-':data[4]+datahtml)
        //                 $("."+ele+" a").eq(5).html(!data[5]?'-':data[5]+datahtml)
        //             }
        //         },
        //     });
        // }
    }

    var isNormalUser = $('#isNormalUser').val();
    if(isNormalUser) {
        // 普通用户我的专利
        $.ajax({
            url: "/user/patent/statistics.html",
            type: "post",
            data: {date: new Date()},
            success: function (res) {
                if (res.code == 0) {
                    var data = res.data;
                    var htmls = $(".patent-ku").text()
                    $("#myzlzl").html(data[0]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#myyxfmzl").html(data[1]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#myzsfmzl").html(data[2]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#myqnfmsqzl").html(data[3]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                    $("#myjnfmsqzl").html(data[4]+'<i class="layui-icon" data-icon="&#xe656;">&#xe656;</i><cite>'+htmls+'</cite>')
                }
            },
        });
        // 普通用户年费监控
        $.ajax({
            url: "/user/patent/annual/fee/statistics.html",
            type: "post",
            data: {date: new Date()},
            success: function (res) {
                if (res.code == 0) {
                    var data = res.data;
                    $("#normal-beyond").html(data[0]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>');
                    $("#normal-lateThreeToFiveMonth").html(data[1]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>');
                    $("#normal-lateTwoMonth").html(data[2]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>');
                    $("#normal-lateOneMonth").html(data[3]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>');
                    $("#normal-emergency").html(data[4]+'<i class="layui-icon" data-icon="&#xe65e;">&#xe65e;</i><cite>年费监控</cite>');
                }
            },
        });
    }

    $(".mainindex a.blanklink").on("click",function(){
        window.parent.tab.tabAdd($(this));

    })
    $(".main-bottom a").on("click",function(){
        if($(this).html()!="-"){
            window.parent.tab.tabAdd($(this));
        }
    })
})
