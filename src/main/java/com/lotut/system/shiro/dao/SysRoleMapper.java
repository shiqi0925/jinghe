package com.lotut.system.shiro.dao;

import com.lotut.system.shiro.domain.SysRole;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysRoleMapper {
    @Insert("insert into sys_role(role_name,role_desc,remark,create_user,create_time,update_user,update_time) " +
            "values(#{roleName},#{roleDesc},#{remark},#{createUser},now(),#{updateUser},now())")
    int save(SysRole sysRole);

    @Delete("delete from sys_role where record_id=#{recordId}")
    int remove(Long recordId);

    @Update("update sys_role set role_name = #{roleName},role_desc=#{roleDesc},remark=#{remark},update_user=#{updateUser},update_time=now() " +
            "where record_id = #{recordId}")
    int update(SysRole sysRole);

    @Select("select * from sys_role")
    @Results(id = "list", value = {
            @Result(column = "record_id", property = "recordId", id = true),
            @Result(column = "role_name", property = "roleName"),
            @Result(column = "role_desc", property = "roleDesc"),
            @Result(column = "remark", property = "remark"),
            @Result(column = "create_time", property = "createTime"),
            @Result(column = "update_time", property = "updateTime")
    })
    List<SysRole> list();

    @Select("select r.record_id,r.role_name,r.role_desc " +
            "from sys_role r " +
            "left join sys_user_role ur on r.record_id = ur.role_id " +
            "where ur.user_id = #{userId}")
    @Results(id = "selectRolesByUserId", value = {
            @Result(column = "record_id", property = "recordId", id = true),
            @Result(column = "role_name", property = "roleName"),
            @Result(column = "role_desc", property = "roleDesc")
    })
    List<SysRole> selectRolesByUserId(Long userId);

    @Select("select record_id from sys_role where role_name = #{roleName}")
    Long getRoleIdByRoleName(String roleName);
}
