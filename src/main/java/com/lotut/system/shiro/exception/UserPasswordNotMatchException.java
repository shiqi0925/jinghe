package com.lotut.system.shiro.exception;

/**
 * 密码错误
 *
 * @author xwj
 * @date 2019/5/29
 */
public class UserPasswordNotMatchException extends UserException {
    public UserPasswordNotMatchException() {
        super("user.password.not.match", null);
    }
}
