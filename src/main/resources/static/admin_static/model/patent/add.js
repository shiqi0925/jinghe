  layui.config({
    base: '/admin_static/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'layer', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;


    function search(){
    	
        //代理机构列表
        table.render({
            elem: '#contentList',
            url: '/admin/patent/getPatent',
            method: 'post',
            cellMinWidth: 95,
            page: true,
            height: "full-125",
            where:{
            	keyword:$("#keyword").val()
            },
            limit: 20,
            id: "contentList",
            cols: [[
            	{type: 'checkbox', fixed: 'left'},
                {align: 'center', type: 'numbers', title: '编号'},
                {field: 'appNo', title: '申请号', align: "center"},
                {field: 'patentName', title: '专利名', align: 'center'} 
            ]]
        });
    }
    // 刷新表格
    function reload() {
        table.reload("newsList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $("#keyword").val()
            }
        })
    }
    $('.add_btn').on('click',function (){
    	//如果没有选中的，就提示选中  否则把 申请号的数据ajax提交到后端。
    	console.log("进入 add btn")
        var checkStatus = table.checkStatus('contentList');
        var idArray = [];
        for (var i = 0; i < checkStatus.data.length; i++) {
            idArray.push(checkStatus.data[i].appNo);
        }
        var recordIds = idArray.join(",");
        console.log("即将提交ajax")
            $.ajax({
                url: "/admin/patent/addPatent",
                type: "post",
                data: {appNos: recordIds},
                success: function (data) {
                    if (data.code == 0) {
/*                        location.reload();
                        layer.close(index);*/
                    }
                }
            });
        return false;
    })
    
    
    
    //搜索
    $(".search_btn").on("click", function () {
    	console.log("1:"+$("#keyword").val());
    	if($("#keyword").val() !=''){
    		search()
    	}
        //reload();
    });
 

 
 

});