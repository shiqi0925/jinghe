layui.use(['layer', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;

    // 标签表格
    table.render({
        elem: '#enterpriseAppList',
        url: '/enterpriseApp/getListByPage.html',
        method: 'post',
        where: {supportStatus: $('#navigationSupportStatus').val()},
        height: "full-135",
        id: "enterpriseAppList",
        page: true,
        limits: [10, 20, 50, 100],
        limit: 10,
        done: function (res, curr, count) {
            if (curr > 1 && res.data.length === 0) {
                //通过class属性获取元素
                var hoverArr = document.getElementsByClassName("hover");
                var searchStatus = '';
                //通过class属性获取元素
                if (hoverArr.length !== 0) {
                    //获得data-status值
                    searchStatus = hoverArr[0].getAttribute('data-status');
                }
                reload(curr - 1, searchStatus);
            }
        },
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', width: 30, title: '序号'},
            {
                field: 'declarationUnit', title: '申报单位', align: "center",
                templet: function (d) {
                    return "<span class='line-text' title='" + d.declarationUnit + "'>" + d.declarationUnit + "</span>";
                }
            },
            {
                field: 'product', title: '主要产品', width: 120, align: "center",
                templet: function (d) {
                    return "<span class='line-text' title='" + d.product + "'>" + d.product + "</span>";
                }

            },
            {
                field: 'lastYearFinance.yearSalesVolume',
                title: '企业上年销售额',
                width: 130,
                align: "center",
                templet: function (d) {
                    if (d.lastYearFinance != null) {
                        return d.lastYearFinance.yearSalesVolume;
                    }
                }
            },
            // {
            //     field: 'lastYearFinance.patentYearSalesVolume',
            //     title: '专利产品年销售额',
            //     width: 145,
            //     align: "center",
            //     templet: function (d) {
            //         if (d.lastYearFinance != null) {
            //             return d.lastYearFinance.patentYearSalesVolume;
            //         }
            //     }
            // },
            {
                field: 'patentDepartment.contacts',
                title: '部门联系人',
                width: 105,
                align: "center",
                templet: function (d) {
                    if (d.patentDepartment != null) {
                        return d.patentDepartment.contacts;
                    }
                }
            },
            {
                field: 'patentDepartment.phone', title: '部门电话', width: 120, align: "center", templet: function (d) {
                    if (d.patentDepartment != null) {
                        return d.patentDepartment.phone;
                    }
                }
            },
            {field: 'insertTime', title: '提交日期', width: 170, align: "center"},
            {
                field: 'supportStatus', title: '申请状态', width: 100, align: "center", templet: function (d) {
                    if (d.supportStatus === 0) {
                        return "未申请";
                    }
                    if (d.supportStatus === 1) {
                        return "申请中";
                    }
                    if (d.supportStatus === 2) {
                        return "审批通过";
                    }
                    if (d.supportStatus === 3) {
                        return "驳回申请";
                    }
                    if (d.supportStatus === 4) {
                        return "资助完成";
                    }
                    if (d.supportStatus === 5) {
                        return "未提交";
                    }
                }
            },
            {
                title: '操作',
                width: 100,
                templet: '#enterpriseAppListBar',
                fixed: "right",
                align: "center"
            }
        ]]
    });

    // table.on('checkbox(enterpriseAppList)', function (obj) {
    //     console.log(obj.checked); //当前是否选中状态
    //     console.log(obj.data); //选中行的相关数据
    //     console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one
    //     id = table.checkStatus('enterpriseAppList').data[0].id; // 获取表格中选中行的数据
    //     console.log(table.checkStatus('enterpriseAppList').data)
    //     console.log(id)
    // });

    // 根据资助状态刷新表格
    // function reloadBySupportStatus(supportStatus) {
    //     table.render({
    //         elem: '#enterpriseAppList',
    //         url: '/enterpriseApp/getListByPage.html',
    //         method: 'post',
    //         where: {supportStatus: supportStatus},
    //         height: "full-150",
    //         id: "enterpriseAppList",
    //         skin: 'line',
    //         page: true,
    //         limits: [10, 20, 50, 100],
    //         limit: 10,
    //         cols: [[
    //             {align: 'center', type: 'numbers', title: '序号'},
    //             {field: 'declarationUnit', title: '申报单位', align: "center"},
    //             {field: 'product', title: '主要产品', align: "center"},
    //             {
    //                 field: 'lastYearFinance.yearSalesVolume', title: '上年销售额', align: "center", templet: function (d) {
    //                     if (d.lastYearFinance != null) {
    //                         return d.lastYearFinance.yearSalesVolume;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'lastYearFinance.patentYearSalesVolume',
    //                 title: '专利产品年销售额',
    //                 align: "center",
    //                 templet: function (d) {
    //                     if (d.lastYearFinance != null) {
    //                         return d.lastYearFinance.patentYearSalesVolume;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.departmentName', title: '专利部门名称', align: "center", templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.departmentName;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.contacts', title: '部门联系人', align: "center", templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.contacts;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.phone', title: '部门电话', align: "center", templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.phone;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.personInChargeName',
    //                 title: '部门负责人',
    //                 align: "center",
    //                 templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.personInChargeName;
    //                     }
    //                 }
    //             },
    //             {field: 'insertTime', title: '申请日期', align: "center"},
    //             {
    //                 field: 'supportStatus', title: '申请状态', align: "center", templet: function (d) {
    //                     if (d.supportStatus === 0) {
    //                         return "未申请";
    //                     }
    //                     if (d.supportStatus === 1) {
    //                         return "申请中";
    //                     }
    //                     if (d.supportStatus === 2) {
    //                         return "审批通过";
    //                     }
    //                     if (d.supportStatus === 3) {
    //                         return "驳回申请";
    //                     }
    //                     if (d.supportStatus === 4) {
    //                         return "资助完成";
    //                     }
    //                 }
    //             },
    //             {title: '操作', minWidth: 180, templet: '#enterpriseAppListBar', fixed: "right", align: "center"}
    //         ]]
    //     });
    // }

    // 刷新表格 keyword搜索
    // function reloadByKeyword() {
    //     table.render({
    //         elem: '#enterpriseAppList',
    //         url: '/enterpriseApp/getListByPage.html',
    //         method: 'post',
    //         where: {keyword: $("#keyword").val()},
    //         height: "full-150",
    //         id: "enterpriseAppList",
    //         skin: 'line',
    //         page: true,
    //         limits: [10, 20, 50, 100],
    //         limit: 10,
    //         cols: [[
    //             {align: 'center', type: 'numbers', title: '序号'},
    //             {field: 'declarationUnit', title: '申报单位', align: "center"},
    //             {field: 'product', title: '主要产品', align: "center"},
    //             {
    //                 field: 'lastYearFinance.yearSalesVolume', title: '上年销售额', align: "center", templet: function (d) {
    //                     if (d.lastYearFinance != null) {
    //                         return d.lastYearFinance.yearSalesVolume;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'lastYearFinance.patentYearSalesVolume',
    //                 title: '专利产品年销售额',
    //                 align: "center",
    //                 templet: function (d) {
    //                     if (d.lastYearFinance != null) {
    //                         return d.lastYearFinance.patentYearSalesVolume;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.departmentName', title: '专利部门名称', align: "center", templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.departmentName;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.contacts', title: '部门联系人', align: "center", templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.contacts;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.phone', title: '部门电话', align: "center", templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.phone;
    //                     }
    //                 }
    //             },
    //             {
    //                 field: 'patentDepartment.personInChargeName',
    //                 title: '部门负责人',
    //                 align: "center",
    //                 templet: function (d) {
    //                     if (d.patentDepartment != null) {
    //                         return d.patentDepartment.personInChargeName;
    //                     }
    //                 }
    //             },
    //             {field: 'insertTime', title: '申请日期', align: "center"},
    //             {
    //                 field: 'supportStatus', title: '申请状态', align: "center", templet: function (d) {
    //                     if (d.supportStatus === 0) {
    //                         return "未申请";
    //                     }
    //                     if (d.supportStatus === 1) {
    //                         return "申请中";
    //                     }
    //                     if (d.supportStatus === 2) {
    //                         return "审批通过";
    //                     }
    //                     if (d.supportStatus === 3) {
    //                         return "驳回申请";
    //                     }
    //                     if (d.supportStatus === 4) {
    //                         return "资助完成";
    //                     }
    //                 }
    //             },
    //             {title: '操作', minWidth: 180, templet: '#enterpriseAppListBar', fixed: "right", align: "center"}
    //         ]]
    //     });
    // }

    $('.support-status').on("click", function () {
        $('.support-status').removeClass("hover");
        $(this).addClass('hover');
        $("#keyword").val('');
    });
    // 点击未申请
    $("#notApply").on("click", function () {
        // reloadBySupportStatus(0);
        reload(1, 0);
        $("#status").val(0);
        $("#keyword").val('');
    });
    // 点击申请中
    $("#applying").on("click", function () {
        // reloadBySupportStatus(1);
        reload(1, 1);
        $("#status").val(1);
        $("#keyword").val('');
    });
    // 点击审批通过
    $("#approve").on("click", function () {
        // reloadBySupportStatus(2);
        reload(1, 2);
        $("#status").val(2);
        $("#keyword").val('');
    });
    // 点击驳回申请
    $("#rejected").on("click", function () {
        // reloadBySupportStatus(3);
        reload(1, 3);
        $("#status").val(3);
        $("#keyword").val('');
    });
    // 点击资助完成
    $("#success").on("click", function () {
        // reloadBySupportStatus(4);
        reload(1, 4);
        $("#status").val(4);
        $("#keyword").val('');
    });

    function reload(currPage, searchStatus) {
        if (currPage === '' || currPage == null) {
            currPage = 1;
        }
        // //通过class属性获取元素
        // var hoverArr = document.getElementsByClassName("hover");
        // var searchStatus = '';
        // //通过class属性获取元素
        // if (hoverArr.length !== 0) {
        //     //获得data-status值
        //     searchStatus = hoverArr[0].getAttribute('data-status');
        // }
        table.reload("enterpriseAppList", {
            page: {
                curr: currPage
            },
            where: {
                keyword: $("#keyword").val(),
                supportStatus: searchStatus
            }
        });
    }

    //搜索
    $(".search_btn").on("click", function () {
        $("#status").val('');
        reload(1, '');
        $('.support-status').removeClass("hover");
    });

    //回车键  搜索
    $("#keyword").keypress(function (e) {
        $("#status").val('');
        if (e.which === 13) {
            reload(1, '');
            $('.support-status').removeClass("hover");
            // tableRight();
        }
    });

    //清空搜索项
    // $("#resetSearch").on('click', function () {
    //     $("#keyword").val('');
    //     $(".support-status").removeClass('hover')
    //     reload();
    // });

    //列表操作
    table.on('tool(enterpriseAppList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        //审批详情页
        if (layEvent === 'approval') {
            var index = layui.layer.open({
                title: "审批页面",
                type: 2,
                area: ['100%', '100%'],
                content: '/enterpriseApp/approvalPage.html?id=' + data.id,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);
                }
            });
        }

        //查看详情
        if (layEvent === 'show') {
            var index = layui.layer.open({
                title: "查看详情",
                type: 2,
                area: ['100%', '100%'],
                content: '/enterpriseApp/toShowPage.html?id=' + data.id,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);
                }
            });
        }
    });

    //导出excel表格
    $(".getCheckData").on("click", function () { //获取选中数据
        var data = table.checkStatus('enterpriseAppList').data;
        if (data.length === 0) {
            //为选择数据，导出全部
            $.ajax({
                url: "/enterpriseApp/exportAll.html",
                type: "post",
                data: {supportStatus: $("#status").val(), keyword: $("#keyword").val()},
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
            return false;
        } else if (data.length !== 0) {
            var arrTableData = JSON.parse(JSON.stringify(data));
            var ids = '', index;
            for (var i = 0; i < arrTableData.length; i++) {
                ids += arrTableData[i].id + ",";
            }
            $.ajax({
                url: "/enterpriseApp/excel.html",
                type: "post",
                data: {"ids": ids},
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
        }
    })
});