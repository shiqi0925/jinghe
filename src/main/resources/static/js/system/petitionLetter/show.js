layui.use(['layer', 'laytpl'], function () {
    var layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery

    if ($("#readStatus").val() === '1') {
        $("#reply").attr("readonly", "readonly");
    }

    //客服回复
    $("#reply_btn").click(function () {
        // alert($("#recordId").val());
        // alert($("#reply").val());
        $.post("/petitionLetter/saveReplyByRecordId.html", {
                recordId: $("#recordId").val(), reply: $("#reply").val()
            }, function (data) {
                if (data.code === 0) {
                    layer.msg('回复成功！', {icon: 1, time: 1500, shade: 0.4});
                    var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
                    parent.layui.table.reload('petitionLetterList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
                    parent.layer.close(index); //再执行关闭
                }
            }
        );
    });

    //返回
    $("#cancel_btn").click(function () {
        var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
        parent.layui.table.reload('petitionLetterList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
        parent.layer.close(index); //再执行关闭
    });
})
