package com.lotut.news.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.news.dto.WebsiteNewsChannelDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface WebsiteNewsChannelMapper {
    int deleteByPrimaryKey(Long recordId);

    int insert(WebsiteNewsChannel record);

    int insertSelective(WebsiteNewsChannel record);

    WebsiteNewsChannel selectByPrimaryKey(@Param("recordId") Long recordId,@Param("websiteId") Long websiteId);

    List<WebsiteNewsChannel> selectParentChannelByWebsiteId(Long websiteId);

    List<WebsiteNewsChannelDto> selectSecondChannelByParentId(Long parentId);

    int updateByPrimaryKeySelective(WebsiteNewsChannel record);

    int updateByPrimaryKey(WebsiteNewsChannel record);

    /**
     * 查询网站一级频道
     * @param websiteId
     * @return
     */
    @Select("select record_id,name from user_website_channel " +
            "where user_website_id = #{websiteId} and status = true and parent_channel_id = 0 order by display_order asc")
    @Results(id = "selectParentChannelList", value = {
            @Result(id = true, column = "record_id", property = "id"),
            @Result(column = "name", property = "name")
    })
    List<WebsiteNewsChannel> selectParentChannelList(Long websiteId);

    /**
     * 查询二级频道
     * @param parentId
     * @return
     */
/*    @Select("select record_id,name from user_website_channel where parent_channel_id = #{parentId} order by display_order asc")
    @Results(id = "selectSecondChannelList", value = {
            @Result(id = true, column = "record_id", property = "id"),
            @Result(column = "name", property = "name")
    })*/
    List<WebsiteNewsChannel> selectSecondChannelList(Long websiteId);
    /**
     * 获取当前用户所有的频道
     * @param userId
     * @return
     */
    List<WebsiteNewsChannel> getUserAllChannel(WebsiteNewsSearchCondition searchCondition);
    /**
     * 根据Id获取频道
     * @param recordId
     * @return
     */
    WebsiteNewsChannel getUserChannelById(Long recordId);
    /**
     * 查询当前用户所有的一级频道
     * @param userId
     * @return
     */
    List<WebsiteNewsChannel> getParentChannel(Long websiteId);
    /**
     * 更新排序
     * @param recordId
     * @return
     */
    int updateDisplayOrder(@Param("displayOrder") Integer displayOrder,@Param("recordId") Long recordId);
    /**
     * 更新名称
     * @param name
     * @param recordId
     * @return
     */
    int updateChannelName(@Param("name") String name,@Param("recordId") Long recordId);
    /**
     * 更新状态
     * @param status
     * @param recordId
     * @return
     */
    int updateStatus(@Param("status") Integer status,@Param("recordId") Long recordId);
    /**
     * 
     * @param defaultFlag
     * @param recordId
     * @return
     */
    int updateDefaultFlag(@Param("defaultFlag") Integer defaultFlag,@Param("recordId") Long recordId);
    /**
     * 添加频道
     * @param channel
     * @return
     */
    int addUserWebsiteChannel(WebsiteNewsChannel channel);
}