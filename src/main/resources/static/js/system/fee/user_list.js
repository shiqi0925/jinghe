layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    laydate.render({
        elem: '#startDeadLine',
        type: 'date',
        format: 'yyyy-MM-dd',
    });
    laydate.render({
        elem: '#endDeadLine',
        type: 'date',
        format: 'yyyy-MM-dd',
    });

    // 标签表格
    table.render({
        elem: '#annualFeeList',
        url: '/user/patent/annual/fee/list.html',
        method: 'post',
        where: {statisticsStatus: $('#navigationStatisticsStatus').val()},
        height: "full-140",
        id: "annualFeeList",
        skin: 'line',
        page: true,
        limits: [10, 20, 50, 100],
        limit: 10,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
            {field: 'appPerson', title: '申请人', width: 240, align: "center"},
            {field: 'appDate', title: '申请日/授权日', align: "center", templet: '#appDateBar'},
            {field: 'deadLine', title: '年度/提醒日',width:140, align: "center",templet: function (d) {
                return '第' + d.annual + '年'+ '<br>'+d.deadLine;
            }
            },
            {field: 'statusStr', title: '缴费状态', width: 90, align: "center",templet: function (d) {
                return '<div style="margin-top:18px;">' + d.statusStr +'</div>' ;
            	}
            },
            {field: 'option', title: '操作', align: "center", templet: '#annualFeeListBar'}
        ]]
    });

    // 构造搜索条件
    function buildSearchCondition() {
        var condition = {
            keyword: $('#keyword').val(),
            patentType: $('#patentType').val(),
            status: $('#status').val(),
            startDeadLine: $('#startDeadLine').val(),
            endDeadLine: $('#endDeadLine').val(),
        }
        return condition;
    }

    // 刷新表格
    function reload() {
        $('.status').removeClass("hover");
        table.render({
            elem: '#annualFeeList',
            url: '/user/patent/annual/fee/list.html',
            method: 'post',
            where: buildSearchCondition(),
            height: "full-140",
            id: "annualFeeList",
            skin: 'line',
            page: true,
            limits: [10, 20, 50, 100],
            limit: 10,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
                {field: 'appPerson', title: '申请人', width: 240, align: "center"},
                {field: 'appDate', title: '申请日/授权日', align: "center", templet: '#appDateBar'},
                {field: 'deadLine', title: '年度/提醒日',width:140, align: "center",templet: function (d) {
                    return '第' + d.annual + '年'+ '<br>'+d.deadLine;
                }
                },
                {field: 'statusStr', title: '缴费状态', width: 90, align: "center",templet: function (d) {
                    return '<div style="margin-top:18px;">' + d.statusStr +'</div>' ;
                	}
                },
                {field: 'option', title: '操作', align: "center", templet: '#annualFeeListBar'}
            ]]
        });
    }

    // 根据统计状态刷新表格
    function reloadByStatisticsStatus(statisticsStatus) {
        $("#keyword").val('');
        $("#patentType").val('');
        $("#status").val('');
        $("#startDeadLine").val('');
        $("#endDeadLine").val('');
        table.render({
            elem: '#annualFeeList',
            url: '/user/patent/annual/fee/list.html',
            method: 'post',
            where: {statisticsStatus: statisticsStatus},
            height: "full-190",
            id: "annualFeeList",
            skin: 'line',
            page: true,
            limits: [10, 20, 50, 100],
            limit: 10,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
                {field: 'appPerson', title: '申请人', width: 240, align: "center"},
                {field: 'appDate', title: '申请日/授权日', align: "center", templet: '#appDateBar'},
                {field: 'deadLine', title: '年度/提醒日',width:140, align: "center",templet: function (d) {
                    return '第' + d.annual + '年'+ '<br>'+d.deadLine;
                }
                },
                {field: 'statusStr', title: '缴费状态', width: 90, align: "center",templet: function (d) {
                    return '<div style="margin-top:18px;">' + d.statusStr +'</div>' ;
                	}
                },
                {field: 'option', title: '操作', align: "center", templet: '#annualFeeListBar'}
            ]]
        });
    }

    //搜索
    $(".search_btn").on("click", function () {
        reload();
    });

    $("#keyword").keypress(function (e) {
        if (e.which == 13) {
            reload();
        }
    });

    $('.status').on("click", function () {
        $('.status').removeClass("hover");
        $(this).addClass('hover');
    })

    // 点击统计
    $(".status").on("click", function () {
        var status = $(this).attr("status");
        reloadByStatisticsStatus(status);
    });

    //列表操作
    table.on('tool(annualFeeList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'payed') {
            $.post("/user/patent/annual/fee/payed.html", {
                id: data.id
            }, function (data) {
                if (data.code == 0) {
                    location.reload();
                } else {
                    layer.msg(data.msg);
                }
            })
        }
        if (layEvent === 'not-pay') {
            $.post("/user/patent/annual/fee/notPay.html", {
                id: data.id
            }, function (data) {
                if (data.code == 0) {
                    location.reload();
                } else {
                    layer.msg(data.msg);
                }
            })
        }
        if (layEvent === 'abandon') {
            layer.confirm('确定置为放弃吗', {icon: 3, title: '提示信息'}, function (index, row) {
                $.post("/user/patent/annual/fee/abandon.html", {
                    patentId: data.patentId
                }, function (data) {
                    layer.close(index);
                    if (data.code == 0) {
                        location.reload();
                    } else {
                        layer.msg(data.msg);
                    }
                })
            });
        }
    });

});