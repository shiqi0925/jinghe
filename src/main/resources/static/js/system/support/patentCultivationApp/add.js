layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    //保存
    $("#save").on('click', function () {
        $("#save-submit").click();
    });

    //监听提交-保存
    form.on('submit(save-submit)', function (data) {
        data.field.supportStatus = 1;
        $.ajax({
            url: "/patentCultivationApp/saveApply.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('提交成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        closePage()
                    });
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    //临时保存
    $("#check-save").on('click', function () {
        $(":input").removeAttr("lay-verify");
        var textArea = document.getElementsByClassName("layui-textarea");
        for (var i = 0; i++; i < textArea.length) {
            textArea[i].removeAttr("lay-verify");
        }
        $("#check-save-submit").click();
    });

    //提交临时保存
    form.on('submit(check-save-submit)', function (data) {
        data.field.supportStatus = 5;
        $.ajax({
            url: "/patentCultivationApp/saveApply.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('临时保存成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        closePage()
                    });
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    //提示
    $("#prompt").click(function () {
        layer.open({
            title: '提示',
            area: ['450px', 'auto'],
            content: "主要填写实施该项目的背景意义、总体目标、主要任务、保障措施等内容。突出项目实施的对企业发展的作用和意义，项目已有专利储备和其对产品的支撑作用，技术面临的风险和对策，项目预期目标、成果和具体可考核指标（如高价值专利组合构成、技术进步程度、预期经济效益、市场竞争力等），项目实施的思路和构想。",
            btn: ['我知道了'],
            yes: function (index, layero) {
                layer.close(index);
            },
            success: function (layero) {
                layero.find('.layui-layer-btn').css('text-align', 'center')
            }
        });
    });

    form.verify({
        number: function (value, item) {
            if (/^$/.test(value)) {
                return "必填项不能为空";
            }
            if (/^\s$/.test(value)) {
                return "不能含有空格！";
            }
            if (/^0\d+$/.test(value)) {
                return "第一个数不能为0！";
            }
            if (/^(([1-9]\d*)|0+)(\.\d*)$/.test(value)) {
                return "不能输入小数！";
            }
            if (/^-(([1-9]\d*)|0)(\.\d*)?$/.test(value)) {
                return "不能输入负数！";
            }
            if (isNaN(value)) {
                return "请输入正确的数字！";
            }
        },
        phone: function (value, item) {
            if (/^$/.test(value)) {
                return "必填项不能为空";
            }
            if (!(/^1[3456789]\d{9}$/.test(value))) {
                return "请输入正确的手机号！";
            }
        },

        // number: [
        //     /^(([1-9]\d*)|0)$/, "输入的数字不正确，请重新输入！"
        // ],
        // phone: function (value, item) {
        //     if (!(/^1[3456789]\d{9}$/.test(value))) {
        //         return "请输入正确的手机号！";
        //     }
        // },
        // phonee: function (value, item) {
        //     if (!/^(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/.test(value)) {
        //         return "固定电话输入错误！";
        //     }
        // },
        // percentage: [
        //     /^(100|(([1-9]\d|\d)(\.\d{1,2})?))$/, "请输入正确的百分数！<br>(只能保留两位小数)"
        // ]
    });

    function closePage() {
        var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
        parent.layui.table.reload('patentCultivationAppList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
        parent.layer.close(index); //再执行关闭
    }
});