package com.lotut.news.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lotut.common.constants.WebsiteConstants;
import com.lotut.common.dto.ReturnResult;
import com.lotut.website.domain.Website;
import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.news.dto.WebsiteNewsSearchCondition;
import com.lotut.news.service.WebsiteNewsChannelService;
import com.lotut.website.service.WebsiteService;
import com.lotut.system.shiro.util.ShiroUtil;

/**
 * @author xwj
 * @date 2020/3/12
 */
@RequestMapping("/admin/news")
@Controller
public class WebsiteChannelController {
    @Autowired
    private WebsiteNewsChannelService websiteNewsChannelService;
    @Autowired
    private WebsiteService websiteService;
    
    /**
     * 用于递归时存储上级服务
     */
    private static final Set<WebsiteNewsChannel> parentTree = new TreeSet<>();
    
    private static final int PARENT_ID = 0;
    /**
     * 新闻频道tml
     *
     * @param parentId
     * @return
     */
	@GetMapping("/edit_channel")
    public String editChannel(Model model) {
		return "admin/news/add_channel";
	}
	
    /**
     * 新增和修改接口 
     *
     * @param parentId
     * @return
     */
    @PostMapping(value = "/addNewsChannel")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult addNewsChannel(WebsiteNewsChannel channel,HttpServletRequest request) {
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
    	channel.setWebsiteId(website.getRecordId());
    	System.out.print("AA:"+channel.getParentChannelId()+"SS");
    	channel.setDisplayOrder(100);
    	if(channel.getParentChannelId()==null) {
    		channel.setParentChannelId((long) 0);
    	}
    	websiteNewsChannelService.addUserWebsiteChannel(channel);
        return ReturnResult.successResult();
    }
	
	
    /**
     * 编辑频道tml
     *
     * @param parentId
     * @return
     */
	@GetMapping("/news_channel")
    public String getChannel(Model model) {
		return "admin/news/channel";
	}
    /**
     * 查询二级频道
     *
     * @param parentId
     * @return
     */
    @PostMapping(value = "/channel_list")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult getSecondChannelList(Model model,HttpServletRequest request) {
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
    	List<WebsiteNewsChannel> channels = websiteNewsChannelService.selectSecondChannelList(website.getRecordId());
        
        
        //model.addAttribute("channels",channels); 
        return ReturnResult.successResult(channels);
        //return "html/news_detail";
    }
    
    @GetMapping(value = "/userChannelList")
/*    @RequiresRoles("sys_news_admin")*/
    public String getUserChannelList() {
        return "user/user_channel_list";
    }
    
    /**
     * 加载频道服务树
     *
     */
    @PostMapping(value = "/userChannelListData")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult getChannelTree(WebsiteNewsSearchCondition searchCondition){
    	Website website = websiteService.selectByUserId(ShiroUtil.getUserId());
    	searchCondition.setWebsiteId(website.getRecordId());
        List<WebsiteNewsChannel> channelList = websiteNewsChannelService.getUserAllChannel(searchCondition);
        Set<WebsiteNewsChannel> channelSet = new TreeSet<>();
        for (WebsiteNewsChannel channel : channelList) {
            Set<WebsiteNewsChannel> parentChannelSet = recursiveChannel(channel);
            for (WebsiteNewsChannel parent : parentChannelSet) {
            	channelSet.add(parent);
            }
            parentTree.clear();
            channelSet.add(channel);
        }
        List<WebsiteNewsChannel> channels = convertSetToList(channelSet);
        return ReturnResult.successResult(channels);
    }
    
    private Set<WebsiteNewsChannel> recursiveChannel(WebsiteNewsChannel channel) {
    	WebsiteNewsChannel record =websiteNewsChannelService.getUserChannelById(channel.getParentChannelId());
        if (record != null) {
        	parentTree.add(record);
        	recursiveChannel(record);
        }
        return parentTree;
    }
    
    private List<WebsiteNewsChannel> convertSetToList(Set<WebsiteNewsChannel> channelSet) {
        List<WebsiteNewsChannel> firstChanneList = new ArrayList<>();
        for (WebsiteNewsChannel channel : channelSet) {
			 if (channel.getParentChannelId() == PARENT_ID) {
				 firstChanneList.add(channel); 
			 }
        }
		for(WebsiteNewsChannel websiteChannel :firstChanneList) {
			List<WebsiteNewsChannel> secondChanneList = new ArrayList<>(); 
			for(WebsiteNewsChannel channel :channelSet) { 
				if(websiteChannel.getId().intValue() ==channel.getParentChannelId().intValue()) { 
					List<WebsiteNewsChannel> list = new ArrayList<WebsiteNewsChannel>();
					channel.setChildren(list);
					secondChanneList.add(channel); 
				} 
			}
			 websiteChannel.setChildren(secondChanneList);
		 }
        return firstChanneList;
    }
    /**
     * 更新排序
     * @param id
     * @return
     */
    @PostMapping(value = "/updateDisplayOrder")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult updateDisplayOrder(Integer displayOrder,Long recordId) {
    	websiteNewsChannelService.updateDisplayOrder(displayOrder,recordId);
    	return ReturnResult.successResult();
    }
    /**
     * 更新名称
     * @param name
     * @param recordId
     * @return
     */
    @PostMapping(value = "/updateChannelName")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult updateChannelName(String name,Long recordId) {
    	websiteNewsChannelService.updateChannelName(name,recordId);
    	return ReturnResult.successResult();
    }
    /**
     * 更新状态
     * @param status
     * @param recordId
     * @return
     */
    @PostMapping(value = "/updateStatus")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult updateStatus(Integer status,Long recordId) {
    	websiteNewsChannelService.updateStatus(status,recordId);
    	return ReturnResult.successResult();
    	
    }
    /**
     * 是否默认
     * @param defaultFlag
     * @param recordId
     * @return
     */
    @PostMapping(value = "/updateDefaultFlag")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult updateDefaultFlag(Integer defaultFlag,Long recordId) {
    	websiteNewsChannelService.updateDefaultFlag(defaultFlag,recordId);
    	return ReturnResult.successResult();
    }
    
    @GetMapping(value = "/addFirst")
/*    @RequiresRoles("sys_news_admin")*/
    public String addFirst() {
        return "user/add_first_channel";
    }
    
    @PostMapping(value = "/addFirstChannel")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult addFirstChannel(WebsiteNewsChannel channel) {
    	Website website = websiteService.selectByUserId(ShiroUtil.getUserId());
    	channel.setUserWebsiteId(website.getRecordId());
    	channel.setParentChannelId(0L);
    	websiteNewsChannelService.addUserWebsiteChannel(channel);
        return ReturnResult.successResult();
    }
    
    @GetMapping(value = "/addSecond")
/*    @RequiresRoles("sys_news_admin")*/
    public String addSecond(Model model) {
    	Website website = websiteService.selectByUserId(ShiroUtil.getUserId());
    	List<WebsiteNewsChannel>parentChannel = websiteNewsChannelService.getParentChannel(website.getRecordId());
    	model.addAttribute("parentChannel", parentChannel);
        return "user/add_second_channel";
    }
    
    @PostMapping(value = "/addSecondChannel")
    @ResponseBody
/*    @RequiresRoles("sys_news_admin")*/
    public ReturnResult addSecondChannel(WebsiteNewsChannel channel) {
    	Website website = websiteService.selectByUserId(ShiroUtil.getUserId());
    	channel.setUserWebsiteId(website.getRecordId());
    	websiteNewsChannelService.addUserWebsiteChannel(channel);
        return ReturnResult.successResult();
    }
 
}
