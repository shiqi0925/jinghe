package com.lotut.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lidc
 * @date 2019-09-03 15:51
 */
public class ZipUtil {

    private final static Logger logger = LoggerFactory.getLogger(ZipUtil.class);

    /**
     *
     * @param file 需要打包的文件
     * @param zipPath  生成zip文件的路径
     * @param zipPath  生成zip文件父级目录
     * @param zipName  压缩包名
     * @return
     */
    public static String singleFileToZip(File file,String zipPath,String zipName){
        File zipFile = new File(zipPath);
        if(!zipFile.exists()) zipFile.mkdirs();
        String zipRealPath = zipPath + zipName + ".zip";
        File zip = new File(zipRealPath);
        if(zip.exists()) zip.delete();
        FileOutputStream fos;
        CheckedOutputStream cos;
        ZipOutputStream zos = null;
        FileInputStream fis;
        BufferedInputStream bis = null;
        try {
            fos = new FileOutputStream(zipRealPath);
            cos = new CheckedOutputStream(fos, new CRC32());
            zos = new ZipOutputStream(cos);
            //创建压缩文件
            zos.putNextEntry(new ZipEntry(file.getName()));

            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);

            byte [] bytes = new byte[1024];
            IOUtils.copyLarge(bis,zos,bytes);
            return zipRealPath;
        }catch (Exception e){
            e.getStackTrace();
            return null;
        }finally {
            if(bis != null)
                IOUtils.closeQuietly(bis);
            if(zos != null)
                IOUtils.closeQuietly(zos);
        }

    }

    /**
     * 将filePaths 文件集合打包到zipPath 这个zip文件中
     * @param zipPath  压缩后的文件路径
     * @param files 需要压缩的文件集合
     *
     */
    public static String multipleFileToZip(String zipPath,List<File> files,List<String> fileNameList) {
        File fileParent = new File(zipPath);
        if(!fileParent.exists()) fileParent.mkdirs();
        String zipRealPath = zipPath + "ip-" + UUID.randomUUID().toString() + ".zip";
        try(FileOutputStream fos=new FileOutputStream(zipRealPath); ZipOutputStream zos= new ZipOutputStream(new BufferedOutputStream(fos))){
            for(int i=0;i<files.size();i++) {
            	try(FileInputStream fis = new FileInputStream(files.get(i))){
            		String extensionName = files.get(i).getName().substring(files.get(i).getName().lastIndexOf('.'));
            		String fileName = fileNameList.get(i)+extensionName;
                    zos.putNextEntry(new ZipEntry(fileName));
                    IOUtils.copy(fis,zos);
                    zos.closeEntry();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
            
            return zipRealPath;
        }catch (IOException e){
            logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * @param zipPath  压缩后的文件路径
     * @param fileMap 需要压缩的文件集合
     *
     */
    public static boolean zipMultiFiles(String zipPath,List<Map<String,String>> fileMap) {
        try(FileOutputStream fos=new FileOutputStream(zipPath);
            ZipOutputStream zos= new ZipOutputStream(new BufferedOutputStream(fos))){
            for (Map<String,String> map : fileMap){
                try(FileInputStream fis = new FileInputStream(map.get("path"))){
                    zos.putNextEntry(new ZipEntry(map.get("name")));
                    IOUtils.copy(fis,zos);
                    zos.closeEntry();
                } catch (IOException e) {
                    logger.error(map.get("name") + "打包失败：{}",e.getMessage());
                }
            }
            return true;
        }catch (IOException e){
            logger.error("打包失败：{}",e.getMessage());
        }
        return false;
    }

}

