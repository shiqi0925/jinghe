layui.use(['form', 'layer', 'laydate'], function () {
    var form = layui.form,
        $ = layui.jquery,
        laydate = layui.laydate;

    laydate.render({
        elem: "#grantDate",
        type: "date",
        trigger: 'click'
    });

    form.on("submit(addGrant)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: "/support/grant/apply/add.html",
            type: "post",
            data: $(data.form).serialize(),
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("添加成功");
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    $("#cancel_btn").click(function () {
        layer.closeAll("iframe");
        //刷新父页面
        parent.location.reload();
    });
});