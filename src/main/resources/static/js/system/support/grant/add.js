layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    laydate.render({
        elem: '#startPublishDate',
        type: 'date',
        format: 'yyyy-MM-dd'
    });
    laydate.render({
        elem: '#endPublishDate',
        type: 'date',
        format: 'yyyy-MM-dd'
    });

    // 标签表格
    table.render({
        elem: '#patentList',
        url: '/patent/search.html',
        method: 'post',
        height: "full-100",
        id: "patentList",
        page: true,
        limits: [20, 50, 100],
        limit: 20,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '申请号', align: "center"},
            {field: 'patentName', title: '专利名称', align: "center"},
            {field: 'publishNo', title: '公开号', align: "center"},
            {field: 'publishDate', title: '公开日', align: "center"},
            {field: 'grantDate', title: '授权公告日', align: "center"},
            {field: 'appPerson', title: '申请人', align: "center"},
            {field: 'inventPerson', title: '发明人', align: "center"},
            {field: 'patentType', title: '专利类型', align: "center"},
            {field: 'lprs', title: '法律状态', align: "center"},
            {field: 'address', title: '地址', align: "center"}
        ]]
    });

    // 刷新表格
    function reload() {
        var startPublishDate = $('#startPublishDate').val();
        var endPublishDate = $('#endPublishDate').val();
        var patentTypes = [];
        $('input[name="patentTypes"]:checked').each(function () {
            patentTypes.push($(this).val());
        });
        table.reload("patentList", {
            page: {
                curr: 1
            },
            where: {
                "startPublishDate": startPublishDate,
                "endPublishDate": endPublishDate,
                "patentTypes": patentTypes.join(',')
            }
        })
    }

    // 搜索
    $(".search_btn").on("click", function () {
        reload();
    });

    // 添加选中
    $("#addSelect").on("click", function () {
        var checkStatus = table.checkStatus('patentList');
        var len = checkStatus.data.length;
        if (len > 0) {
            layer.prompt({
                formType: 0,
                title: '添加选中'
            }, function (value, index, elem) {
                var appAmount = value.trim();
                if(!checkMoney(appAmount)) {
                    layer.msg("请输入有效金额");
                    return;
                } else {
                    layer.close(index);
                    var patentIds = [];
                    $.each(checkStatus.data, function (i, val) {
                        patentIds.push(val.id);
                    });
                    var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
                    $.ajax({
                        url: "/support/grant/addSelectedPatents.html",
                        type: "post",
                        data: {patentIds: patentIds.join(","), appAmount: appAmount},
                        success: function (res) {
                            if (res.code == 0) {
                                setTimeout(function () {
                                    top.layer.close(index);
                                    top.layer.msg("添加成功");
                                    layer.closeAll("iframe");
                                    //刷新父页面
                                    parent.location.reload();
                                }, 1000);
                            } else {
                                setTimeout(function () {
                                    top.layer.msg(res.msg);
                                }, 2000);
                            }
                        },
                    });
                }
            });
        } else {
            layer.msg("请先选择要添加的专利");
        }
    });

    // 一键添加
    $("#addAll").on("click", function () {
        var startPublishDate = $('#startPublishDate').val();
        var endPublishDate = $('#endPublishDate').val();
        var patentTypes = [];
        $('input[name="patentTypes"]:checked').each(function () {
            patentTypes.push($(this).val());
        });
        if(startPublishDate != '' || endPublishDate != '' || patentTypes.length > 0) {
            layer.prompt({
                formType: 0,
                title: '请输入申请金额'
            }, function (value, index, elem) {
                var appAmount = value.trim();
                if(!checkMoney(appAmount)) {
                    layer.msg("请输入有效金额");
                    return;
                } else {
                    layer.close(index);
                    var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
                    $.ajax({
                        url: "/support/grant/addAllPatents.html",
                        type: "post",
                        data: {
                            startPublishDate: startPublishDate,
                            endPublishDate: endPublishDate,
                            patentTypes: patentTypes.join(','),
                            appAmount: appAmount
                        },
                        success: function (res) {
                            if (res.code == 0) {
                                setTimeout(function () {
                                    top.layer.close(index);
                                    top.layer.msg("正在添加，稍候刷新页面");
                                    layer.closeAll("iframe");
                                    //刷新父页面
                                    parent.location.reload();
                                }, 2000);
                            } else {
                                setTimeout(function () {
                                    top.layer.msg(res.msg);
                                }, 2000);
                            }
                        },
                    });
                }
            });
        } else {
            layer.msg("请输入搜索条件");
        }
    });

});