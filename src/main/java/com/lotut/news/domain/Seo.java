package com.lotut.news.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * seo
 *
 * @author Administrator
 */
public class Seo implements Serializable {
    private static final long serialVersionUID = 4959485506060450737L;
    /**
     * 自增主键
     */
    private Long id;

    /**
     *  
     */
    private String pageName;
    /**
     *  
     */
    private String title;
    /**
     *  
     */
    private String keywords;
    /**
     *  
     */
    private String description;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Seo [id=" + id + ", pageName=" + pageName + ", title=" + title + ", keywords=" + keywords
				+ ", description=" + description + "]";
	}
    

}