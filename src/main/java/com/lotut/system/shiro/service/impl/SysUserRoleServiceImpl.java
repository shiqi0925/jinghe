package com.lotut.system.shiro.service.impl;

import com.lotut.system.shiro.dao.SysUserRoleMapper;
import com.lotut.system.shiro.domain.SysUserRole;
import com.lotut.system.shiro.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public int batchSave(List<SysUserRole> sysUserRoleList) {
        return sysUserRoleMapper.batchSave(sysUserRoleList);
    }

    @Override
    public int removeByUserId(Long userId) {
        return sysUserRoleMapper.removeByUserId(userId);
    }
    
    @Override
    public int save(SysUserRole role) {
        return sysUserRoleMapper.save(role);
    }    
}
