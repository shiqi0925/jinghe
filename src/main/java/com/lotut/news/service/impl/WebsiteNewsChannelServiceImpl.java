package com.lotut.news.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lotut.news.dao.WebsiteNewsChannelMapper;
import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.news.dto.WebsiteNewsChannelDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;
import com.lotut.news.service.WebsiteNewsChannelService;	

/**
 * @author lidc
 * @date 2020-03-12 13:38
 */
@Service
public class WebsiteNewsChannelServiceImpl implements WebsiteNewsChannelService {

    @Autowired
    private WebsiteNewsChannelMapper websiteNewsChannelMapper;

    @Override
    public List<WebsiteNewsChannelDto> selectSecondChannelByParentId(Long parentId){
        return websiteNewsChannelMapper.selectSecondChannelByParentId(parentId);
    }

    @Override
    public List<WebsiteNewsChannel> selectParentChannelList(Long websiteId) {
        return websiteNewsChannelMapper.selectParentChannelList(websiteId);
    }

    @Override
    public List<WebsiteNewsChannel> selectSecondChannelList(Long websiteId) {
        return websiteNewsChannelMapper.selectSecondChannelList(websiteId);
    }

    @Override
    public WebsiteNewsChannel selectByPrimaryKey(Long recordId,Long websiteId){
        return websiteNewsChannelMapper.selectByPrimaryKey(recordId,websiteId);
    }

	@Override
	public List<WebsiteNewsChannel> getUserAllChannel(WebsiteNewsSearchCondition searchCondition) {
		return websiteNewsChannelMapper.getUserAllChannel(searchCondition);
	}

	@Override
	public WebsiteNewsChannel getUserChannelById(Long recordId) {
		return websiteNewsChannelMapper.getUserChannelById(recordId);
	}

	@Override
	public List<WebsiteNewsChannel> getParentChannel(Long websiteId) {
		return websiteNewsChannelMapper.getParentChannel(websiteId);
	}

	@Override
	public int updateDisplayOrder( Integer displayOrder,Long recordId) {
		return websiteNewsChannelMapper.updateDisplayOrder(displayOrder,recordId);
	}

	@Override
	public int updateChannelName(String name, Long recordId) {
		return websiteNewsChannelMapper.updateChannelName(name, recordId);
	}

	@Override
	public int updateStatus(Integer status, Long recordId) {
		return websiteNewsChannelMapper.updateStatus(status, recordId);
	}

	@Override
	public int updateDefaultFlag(Integer defaultFlag, Long recordId) {
		return websiteNewsChannelMapper.updateDefaultFlag(defaultFlag, recordId);
	}

	@Override
	public int addUserWebsiteChannel(WebsiteNewsChannel channel) {
		return websiteNewsChannelMapper.addUserWebsiteChannel(channel);
	}
}
