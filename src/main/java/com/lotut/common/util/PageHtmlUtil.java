package com.lotut.common.util;

 

import com.lotut.common.dto.Page;

public class PageHtmlUtil {
    /*
     * @auther: shiqi
     * @describe: 生成页面分页html代码
     * @param Page page 分页对象
     * @param UrlBegin  页数前的url
     * @param UrlEnd	页数后的url
     * @return: java.lang.String
     * @date: 2019/1/16
     */
	public static String getPageHtml(Page page, String urlBegin, String urlend) {
		int pageNo = page.getPageNo(); //当前页
		int totalCount = page.getTotalCount(); //总条数
		int totalPage = page.getTotalPage(); //总页数
		//int totalPage = 100; //总页数
		int pageSize = page.getPageSize(); //每页多少条
		int previousPageNo = pageNo - 1;
		int nextPageNo = pageNo + 1;
		String pageNoPage = "";
		String previousPage="";String firstPage="";String nextPage="";String lastPage="";
		String middlePage = "";
		
		if(totalPage > 15) {
			if(pageNo<5) {
				for(int i=0;i<5;i++) {
					if((i+1) == pageNo) {
						middlePage = middlePage + "<a class='active disabled' href='javascript:;'>"+pageNo+"</a>";	
					}else {
						middlePage = middlePage + "<a href='"+urlBegin+(i+1)+urlend+"'>"+(i+1)+"</a>";
					}
				}
				middlePage = middlePage + "<span class='disabled'>…</span>"+"<a href='"+urlBegin+totalPage+urlend+"'>"+totalPage+"</a>";	
			}else {
				middlePage = middlePage + "<a href='"+urlBegin+"1"+urlend+"'>1</a>"+"<span class='disabled'>…</span>";
				if(totalPage < pageNo+4) {
					//第一页 ... 循环最后五页 
					
					for(int i=(totalPage-5);i<totalPage;i++) {
						if((i+1) == pageNo) {
							middlePage = middlePage + "<a class='active disabled' href='javascript:;'>"+pageNo+"</a>";	
						}else {
							middlePage = middlePage + "<a href='"+urlBegin+(i+1)+urlend+"'>"+(i+1)+"</a>";
						}
					}
				}else {
					//第一页  ... 5页  ... 最后一页
					middlePage = middlePage + "<a href='"+urlBegin+(pageNo-2)+urlend+"'>"+(pageNo-2)+"</a>";
					middlePage = middlePage + "<a href='"+urlBegin+(pageNo-1)+urlend+"'>"+(pageNo-1)+"</a>";
					middlePage = middlePage + "<a class='active disabled' href='javascript:;'>"+pageNo+"</a>";	
					middlePage = middlePage + "<a href='"+urlBegin+(pageNo+1)+urlend+"'>"+(pageNo+1)+"</a>";
					middlePage = middlePage + "<a href='"+urlBegin+(pageNo+2)+urlend+"'>"+(pageNo+2)+"</a>";
					middlePage = middlePage +"<span class='disabled'>…</span>";
					middlePage = middlePage + "<a href='"+urlBegin+totalPage+urlend+"'>"+totalPage+"</a>";
				}
			}
		}else {
			//循环所有页
			for(int i=0;i<totalPage;i++) {
				if((i+1) == pageNo) {
					middlePage = middlePage + "<a class='active disabled' href='javascript:;'>"+pageNo+"</a>";
					
				}else {
					middlePage = middlePage + "<a href='"+urlBegin+(i+1)+urlend+"'>"+(i+1)+"</a>";
				}
			}
		}
		if(pageNo == 1) {
			previousPage = "<a class='disabled' href='javascript:;'>上一页</a>";
			firstPage = "<a class='disabled' href='javascript:;'>首页</a>";
		}else {
			previousPage = "<a href='"+urlBegin+previousPageNo+urlend+"'>上一页</a>";
			firstPage = "<a href='"+urlBegin+previousPageNo+urlend+"'>首页</a>";
		}
		if(pageNo == totalPage) {
			nextPage = "<a class='disabled' href='javascript:;'>下一页</a>";
			lastPage = "<a class='disabled' href='javascript:;'>尾页</a>";
		}else {
			nextPage = "<a href='"+urlBegin+nextPageNo+urlend+"'>下一页</a>";
			lastPage = "<a href='"+urlBegin+previousPageNo+urlend+"'>尾页</a>";
		}	
		String html = "<span>共"+totalCount+"条</span>"+firstPage + previousPage + middlePage + nextPage + lastPage;
		return html;
		
	}
	
     
}
