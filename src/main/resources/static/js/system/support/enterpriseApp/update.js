layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    //保存修改
    $("#save").on('click', function () {
        $("#save-submit").click();
    });

    //保存修改
    form.on('submit(save-submit)', function (data) {
        data.field.supportStatus = 1;
        data.field.id = $("#id").val();
        $.ajax({
            url: "/enterpriseApp/updateApplicationForm.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('提交成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        closePage();
                    });
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    //临时保存
    $("#check-save").on('click', function () {
        $(":input").removeAttr("lay-verify");
        var textArea = document.getElementsByClassName("layui-textarea");
        for (var i = 0; i++; i < textArea.length) {
            textArea[i].removeAttr("lay-verify");
        }
        $("#check-save-submit").click();
    });

    //提交临时保存
    form.on('submit(check-save-submit)', function (data) {
        data.field.supportStatus = 5;
        data.field.id = $("#id").val();
        $.ajax({
            url: "/enterpriseApp/saveApplicationForm.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('临时保存成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        closePage();
                    });
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    form.verify({
        number: function (value, item) {
            if (/^$/.test(value)) {
                return "必填项不能为空";
            }
            if (/^\s$/.test(value)) {
                return "不能含有空格！";
            }
            if (/^0\d+$/.test(value)) {
                return "第一个数不能为0！";
            }
            if (/^(([1-9]\d*)|0)(\.\d*)$/.test(value)) {
                return "不能输入小数！";
            }
            if (/^-(([1-9]\d*)|0)(\.\d*)?$/.test(value)) {
                return "不能输入负数！";
            }
            if (isNaN(value)) {
                return "请输入正确的数字！";
            }
        },
        phone: function (value, item) {
            if (/^$/.test(value)) {
                return "必填项不能为空";
            }
            if (!(/^1[3456789]\d{9}$/.test(value))) {
                return "请输入正确的手机号！";
            }
        },
        // percentage: [
        //     /^(100|(([1-9]\d|\d)(\.\d{1,2})?))$/, "请输入正确的百分数！<br>(且只能保留两位小数)"
        // ]
    });

    //刷新重置
    // $("#res").click(function () {
    //     $("#saveForm")[0].reset();
    //     layui.form.render();
    // });

    function closePage() {
        var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
        parent.layui.table.reload('enterpriseAppList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
        parent.layer.close(index); //再执行关闭
    }
});