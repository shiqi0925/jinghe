/**
 * 校验金额
 * @param money
 * @returns {boolean}
 */
function checkMoney(money) {
    var reg = /(^[1-9]([0-9]+)?$)/;
    if (!reg.test(money)) {
        return false;
    }
    if(parseInt(money) > 99999999) {
        return false;
    }
    return true;
}