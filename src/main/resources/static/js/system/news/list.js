layui.use(['form', 'layer', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;

    //代理机构列表
    var tableIns = table.render({
        elem: '#newsList',
        url: '/news/list',
        method: 'post',
        cellMinWidth: 95,
        page: true,
        height: "full-85",
        limits: [20, 50, 100],
        limit: 20,
        id: "newsList",
        cols: [[
            {align: 'center', type: 'numbers', title: '序号'},
            {
                field: 'title', title: '标题', align: "center",templet:function(d){
                    return '<a style="color: #2177E5;cursor:pointer;" href="/news/preview.html?newsId='+d.recordId+'" target="_blank">'+d.title+'</a>'
                }
            },
            {field: 'thumbnailPath', title: '频道',width:170, align: "center",templet: function (d) {
            	return d.firstChannelName+'>'+d.secondChannelName;
            }
            },
            {
                field: 'slideshowFlag', title: '是否轮播',width:100, align: 'center', templet: function (d) {
                    if (d.slideshowFlag) {
                        return '<input type="checkbox" value="' + d.recordId + '" name="slideshowFlagSwitch"' +
                            ' lay-skin="switch" thumbnailPath="'+d.thumbnailPath+'" lay-filter="slideshowFlag" lay-text="是|否" checked>';
                    }
                    return '<input type="checkbox" value="' + d.recordId + '" name="slideshowFlagSwitch"' +
                        ' lay-skin="switch" thumbnailPath="'+d.thumbnailPath+'" lay-filter="slideshowFlag" lay-text="是|否">';
                }
            },
            {field: 'publishTime', title: '发布时间',width:140, align: 'center'},
            {
                field: 'status', title: '是否发布',width:140, align: 'center', templet: function (d) {
                    if (d.status) {
                        return '<input type="checkbox" value="' + d.recordId + '" name="statusSwitch" lay-skin="switch"' +
                            ' lay-filter="statusSwitch" lay-text="是|否" checked>';
                    }
                    return '<input type="checkbox" value="' + d.recordId + '" name="statusSwitch" lay-skin="switch"' +
                        ' lay-filter="statusSwitch" lay-text="是|否">';
                }
            },
            /*{field: 'viewCount', title: '浏览量', align: 'center'},*/
            {title: '操作', width: 150, templet: '#newsListBar', fixed: "right", align: "center"}
        ]]
    });

    // 刷新表格
    function reload() {
        table.reload("newsList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $("#keyword").val()
            }
        })
    }

    //搜索
    $("#searchBtn").on("click", function () {
        reload();
    });

    $(window).keydown(function (e) {
        var key = window.event ? e.keyCode : e.which;
        if (key.toString() == "13") {
            return false;
        }
    });

    //添加新闻
    $(".add_news_btn").on("click", function () {
        var index = layui.layer.open({
            title: "添加新闻",
            type: 2,
            content: "/admin/news/add.html",
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回新闻列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        });
        layui.layer.full(index);
    });

    function editUser(edit) {
        var recordId = edit.recordId;
        var index = layui.layer.open({
            title: "编辑用户",
            type: 2,
            content: "/news/update?newsId=" + recordId,
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回新闻列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        });
        layui.layer.full(index);
    }

    //列表操作
    table.on('tool(newsList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'edit') {
            editUser(data);
        } else if (layEvent === 'delete') {
            layer.confirm('确定删除该新闻吗', {icon: 3, title: '提示信息'}, function (index, row) {
                $.post("/news/delete.html", {
                    recordId: data.recordId
                }, function (data) {
                    if (data.code == 0) {
                        reload();
                        layer.close(index);
                    }
                })
            });
        }
    });

    //监听是否默认
    form.on('switch(statusSwitch)', function (data) {
        var checked = data.elem.checked;
        if (checked) {
            $.ajax({
                url: "/news/updateStatus.html",
                type: "post",
                data: {recordId: data.value, status: true},
                success: function (res) {
                    // reload();
                }
            });
        } else {
            $.ajax({
                url: "/news/updateStatus.html",
                type: "post",
                data: {recordId: data.value, status: false},
                success: function (res) {
                    // reload();
                }
            });
        }
    });

    //监听是否默认
    form.on('switch(slideshowFlag)', function (data) {
        var checked = data.elem.checked;
        var thumbnailpath = $(data.elem).attr("thumbnailpath");
        if(thumbnailpath == null || thumbnailpath == "") {
            layer.msg("请先上传轮播图");
        } else {
            if (checked) {
                $.ajax({
                    url: "/news/udateSlideshowFlag.html",
                    type: "post",
                    data: {recordId: data.value, slideshowFlag: true},
                    success: function (res) {
                        // reload();
                    }
                });
            } else {
                $.ajax({
                    url: "/news/udateSlideshowFlag.html",
                    type: "post",
                    data: {recordId: data.value, slideshowFlag: false},
                    success: function (res) {
                    }
                });
            }
        }
    });

});