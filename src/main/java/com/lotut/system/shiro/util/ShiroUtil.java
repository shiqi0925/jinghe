package com.lotut.system.shiro.util;

import com.lotut.system.domain.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

/**
 * shiro用户工具类
 *
 * @author xwj
 * @date 2020/3/4
 */
public class ShiroUtil {
    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static Session getSession() {
        return SecurityUtils.getSubject().getSession();
    }

    public static void logout() {
        getSubject().logout();
    }

    public static User getUser() {
        return (User) getSubject().getPrincipal();
    }

    public static Long getUserId() {
        return getUser().getUserId();
    }

    public static String getUsername() {
        return getUser().getUsername();
    }
}
