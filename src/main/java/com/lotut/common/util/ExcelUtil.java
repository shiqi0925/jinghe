package com.lotut.common.util;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.lotut.common.util.LocalDateTimeUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

/**
 * 分析报告excel工具类
 *
 * @author xwj
 * @date 2019/12/25
 */
public class ExcelUtil {
    private static Logger logger = LoggerFactory.getLogger(ExcelUtil.class);

    /**
     * 基于excel模板表格生成excel报告表格
     *
     * @param excelTemplatePath excel模板路径
     * @param excelTemplateName excel模板名称
     * @param excelFileFullPath excel报告表格全路径
     * @param excelName         excel报告表格名称
     * @param data              表格渲染数据
     * @return 生成成功返回excel报告表格全路径，否则返回null
     */
    public static String createExcel(String excelTemplatePath, String excelTemplateName, String excelFileFullPath, String excelName, Map<String, Object> data) {
        TemplateExportParams params = new TemplateExportParams(excelTemplatePath + excelTemplateName, true);
        params.setColForEach(true);
        Workbook workbook = ExcelExportUtil.exportExcel(params, data);
        File saveFile = new File(excelFileFullPath);
        if (!saveFile.exists()) {
            saveFile.mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(excelFileFullPath + excelName)) {
            workbook.write(fos);
            fos.close();
            return excelFileFullPath + excelName;
        } catch (Exception e) {
            logger.error("{}报告excel生成异常:{}", excelTemplatePath, e.getMessage());
        }
        return null;
    }

    /**
     * 构造报告excel路径
     *
     * @param userId  用户id
     * @param orderNo 订单号
     * @return 报告excel路径
     */
    public static String buildExcelPath(String basePath, Long userId, String orderNo) {
        StringBuilder path = new StringBuilder();
        path.append(basePath).append(LocalDateTimeUtil.dateByFormatOther()).append("/");
        path.append(userId).append("/").append(orderNo).append("/");
        return path.toString();
    }
    /**
     * 构造商标excel路径
     *
     * @param userId  用户id
     * @param websiteId 站点id    年月日  时分秒加随机数
     * @return 报告excel路径
     */
    public static String buildBrandExcelPath(String basePath, Long userId, Long websiteId) {
        StringBuilder path = new StringBuilder();
        path.append(basePath);
        path.append(websiteId).append("/");
        path.append(LocalDateTimeUtil.dateByFormatOther()).append("/");
        path.append(userId).append("/");
        return path.toString();
    }
}
