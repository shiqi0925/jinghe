layui.use(['layer', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;

    var cols, tableData;
    $(document).ready(function () {
        $("#apply").hide();
        $.get("/patentCultivationApp/certification.html", {date: new Date()}, function (res) {
            if (res.data === '已认证') {
                $("#apply").show();
                // 标签表格
                table.render({
                    elem: '#patentCultivationAppList',
                    url: '/patentCultivationApp/userListByPage.html',
                    method: 'post',
                    height: "full-85",
                    id: "patentCultivationAppList",
                    page: true,
                    limits: [10, 20, 50, 100],
                    limit: 10,
                    done: function (res, curr, count) {
                        certification(res.msg);
                        tableData = res.data;
                    },
                    cols: [[
                        {align: 'center', type: 'checkbox'},
                        {align: 'center', type: 'numbers', title: '序号'},
                        {
                            field: 'unitName', title: '单位名称', width: 240, align: "center",
                            templet: function (d) {
                                return "<span class='line-text' title='" + d.unitName + "'>" + d.unitName + "</span>";
                            }
                        },
                        {field: 'projectLeader', title: '项目负责人', width: 160, align: "center"},
                        {field: 'phone', title: '联系电话', width: 140, align: "center"},
                        {field: 'totalNumber', title: '总人数', width: 120, align: "center"},
                        {field: 'devNumber', title: '研发人员', width: 120, align: "center"},
                        {field: 'insertTime', title: '填写/提交日期', width: 190, align: "center"},
                        {
                            field: 'supportStatus', title: '申请状态', width: 130, align: "center", templet: function (d) {
                                if (d.supportStatus === 0) {
                                    return "未申请";
                                }
                                if (d.supportStatus === 1) {
                                    return "申请中";
                                }
                                if (d.supportStatus === 2) {
                                    return "审批通过";
                                }
                                if (d.supportStatus === 3) {
                                    return "驳回申请";
                                }
                                if (d.supportStatus === 4) {
                                    return "资助完成";
                                }
                                if (d.supportStatus === 5) {
                                    return "未提交";
                                }
                            }
                        },
                        {
                            title: '操作',
                            width: 160,
                            templet: '#patentCultivationAppListBar',
                            fixed: "right",
                            align: "center"
                        }
                    ]],
                    parseData: function (res) {
                        cols = this.cols[0]; //当前表格的表头
                    }
                });
            } else {
                certification(res.data);
            }
        });
    });

    //列表操作
    table.on('tool(patentCultivationAppList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        //修改
        if (layEvent === 'update') {
            var index = layui.layer.open({
                title: "修改",
                type: 2,
                area: ['100%', '100%'],
                content: '/patentCultivationApp/updatePage.html?id=' + data.id,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);
                }
            });
        }

        //查看详情
        if (layEvent === 'show') {
            var index = layui.layer.open({
                title: "查看详情",
                type: 2,
                area: ['100%', '100%'],
                content: '/patentCultivationApp/toShowPage.html?id=' + data.id,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);
                }
            });
        }
    });

    //填写申请
    $("#apply").on("click", function () {
        // 判断是否通过实名认证
        $.get("/patentCultivationApp/certification.html", {date: new Date()}, function (res) {
            if (res.data === "已认证") {
                var index = layui.layer.open({
                    title: "填写申请",
                    type: 2,
                    area: ['100%', '100%'],
                    content: '/patentCultivationApp/toSavePage.html',
                    success: function (layero, index) {
                        setTimeout(function () {
                            layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        }, 500);
                    }
                });
            } else {
                certification(res.data);
            }
        });
    });

    function certification(msg) {
        if (msg === '个人认证') {
            layer.open({
                title: '提示'
                , content: '抱歉，您是个人认证不能填写申请表！'
                , btn: ['返回']
                , yes: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === '未认证') {
            layer.open({
                title: '提示'
                , content: "抱歉，您未进行企业认证无法填写！<br/>（注意:非个人认证才可填写申请表）"
                , btn: ['实名认证', '取消']
                , yes: function (index, layero) {
                    layer.close(index);
                    location.href = '/certification/indexPage.html';
                }
                , btn2: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "个人认证审核中") {
            layer.open({
                title: '提示'
                , content: "抱歉，您提交的待审核信息为个人认证，无法填写申请表！<br/>（注意:非个人认证才可填写申请表）"
                , btn: ['取消']
                , yes: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "非个人认证审核中") {
            layer.open({
                title: '提示'
                , content: "抱歉，您的认证信息正在审核中！"
                , btn: ['取消']
                , yes: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "驳回") {
            layer.open({
                title: "提示",
                content: "抱歉，你提交的审核被驳回，请重新填写！<br/><span" +
                    " style='color:rgba(242,179,68,1);font-size:12px;'>（为了确保您的认证顺利通过，请认真填写信息）</span> ",
                btn: ['实名认证', '取消'],
                yes: function (index, layero) {
                    layer.close(index);
                    location.href = '/certification/indexPage.html';
                },
                btn2: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                },
                success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "临时保存") {
            layer.open({
                title: "提示"
                , content: "请完成认证信息的填写！"
                , btn: ['去完成', '取消']
                , yes: function (index, layero) {
                    layer.close(index);
                    location.href = '/certification/indexPage.html';
                },
                btn2: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                },
                success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        }
    }

    //导出excel表格
    $(".getCheckData").on("click", function () { //获取选中数据
        var cols2 = JSON.stringify(cols);
        var tableHead = cols2;//表头数据
        var data = table.checkStatus('patentCultivationAppList').data;
        if (data.length === 0) {
            //为选择数据，导出全部
            $.ajax({
                url: "/patentCultivationApp/exportAll.html",
                type: "post",
                data: {"tableHead": tableHead, "supportStatus": $("#status").val(), keyword: $("#keyword").val()},
                success: function (res) {
                    if (res.code === 0) {
                        // layer.msg("表格导出成功！");
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
            return false;
        } else if (data.length !== 0) {
            var arrTableData = JSON.parse(JSON.stringify(data));
            var ids = '';
            for (var i = 0; i < arrTableData.length; i++) {
                ids += arrTableData[i].id + ",";
            }
            $.ajax({
                url: "/patentCultivationApp/excel.html",
                type: "post",
                data: {tableHead: tableHead, "ids": ids},
                success: function (res) {
                    if (res.code === 0) {
                        // layer.msg("表格导出成功！");
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
        }
    })
});