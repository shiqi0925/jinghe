  layui.config({
    base: '/admin_static/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'layer', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;
    
    //代理机构列表
    var tableIns = table.render({
        elem: '#newsList',
        url: '/admin/flink/list',
        method: 'post',
        where: {
        	channelId: $('#type').val()
        },
        cellMinWidth: 95,
        page: false,
        height: "full-125",
        limits: [20, 50, 100],
        limit: 20,
        id: "newsList",
        cols: [[
            {align: 'center', type: 'numbers', title: '编号'},
            {field: 'title', title: '标题', align: 'center'},
            {field: 'furl', title: '链接', align: 'center'},
            {title: '操作', width: 100, templet: '#newsListBar', fixed: "right", align: "center"}
        ]]
    });

    // 刷新表格
    function reload() {
        table.reload("newsList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $("#keyword").val()
            }
        })
    }

    //搜索
    $("#keyword").on("click", function () {
        reload();
    });

    $(window).keydown(function (e) {
        var key = window.event ? e.keyCode : e.which;
        if (key.toString() == "13") {
            return false;
        }
    });

    function editUser(edit) {
    	var id = edit.id;
        var index = layer.open({
            title: "编辑页面",
            area: ['60%', '360px'],
            type: 2,
            content: "/admin/flink/update?id="+id,
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回新闻列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        });
        
    }
    //添加
    $(".add_news_btn").on("click", function () {
        var index =layer.open({
            title: "添加",
            area: ['60%', '220px'],
            type: 2,
            content: "/admin/flink/add",
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                    location.reload();
                }, 500)
                
            }
        });
        
    });
    //列表操作
    table.on('tool(newsList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'edit') {
            editUser(data);
        }
    });

    

});