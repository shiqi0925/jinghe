package com.lotut.system.shiro.service;

import com.lotut.system.shiro.domain.SysRole;

import java.util.List;
import java.util.Set;

public interface SysRoleService {
    int save(SysRole sysRole);

    List<SysRole> list();

    /**
     * 根据用户id查询角色
     *
     * @param userId 用户id
     * @return
     */
    Set<String> selectRoleNamesByUserId(Long userId);

    boolean deleteById(Long roleId);

    int update(SysRole role);
}
