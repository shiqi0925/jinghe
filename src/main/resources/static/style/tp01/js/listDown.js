// requestAnimationFrame兼容性处理
!function () {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame']
        window.cancelAnimationFrame =
            window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame']
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime))
            var id = window.setTimeout(function () {
                callback(currTime + timeToCall)
            }, timeToCall)
            lastTime = currTime + timeToCall
            return id
        }

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id)
        }
}()

function listDown(obj) {
    if (obj instanceof Object) {
        if (!obj.selectDom) {
            throw new Error('请定义您要点击的对象，selectDom不能为空')
        } else {
            this.selectDom = $(obj.selectDom)
        }
        if (!obj.optionDom) {
            throw new Error('请定义您要展开的对象，optionDom')
        } else {
            this.optionDom = $(obj.optionDom)
        }
        this.orderCloseDom = $(obj.orderCloseDom)
        if (obj.acquireDataDom) {
            if (!obj.showDataDom) {
                this.showDataDom = this.selectDom
            } else {
                this.showDataDom = $(obj.showDataDom)
            }
            if (!obj.hideSelectDom) {
                this.hideSelectDom = $(obj.hideSelectDom)
            } else {
                this.hideSelectDom = ''
            }
            this.acquireDataDom = obj.acquireDataDom
            this.addData()
        }
    } else {
        throw new Error('传入的值必须是个对象')
    }
}

listDown.prototype.init = function (obj) {
    if (obj instanceof Object) {
        this.unfoldOrClose = obj.unfoldOrClose
        obj.eventType ? this.eventType = 'hover' : this.eventType = 'click'
        this.orderClose = obj.orderClose
        this.f = obj.fun || function () {
            }
        this.h = obj.h || false
        this.v = obj.v || 5
        this.timer = null
        this.addHeight = [0, 1]
        if (!obj.eventType) {
            if (this.orderClose) {
                this.orderCloseEvent = obj.orderCloseEvent
                if (!this.orderCloseDom[0]) {
                    this.orderCloseDom = this.selectDom.parent()
                }
                if (this.orderCloseEvent) {
                    this.orderCloseFun()
                } else {
                    this.moveOrderCloseFun()
                }
            }
        }
        this.clickHandle()
    } else {
        throw new Error('init参数必须是个对象，哪怕是个空对象也行')
    }
}
listDown.prototype.orderCloseFun = function () {
    var _this = this
    this.orderCloseDom.on('click', function (e) {
        e.preventDefault()
    })
    $(document).on('click', function () {
        if (!_this.unfoldOrClose) {
            _this.closeFun()
        }
    })
}
listDown.prototype.moveOrderCloseFun = function () {
    var _this = this
    this.orderCloseDom.on('click', function (ev) {
        ev.preventDefault()
    })
    this.orderCloseDom.on('dblclick', function (ev) {
        ev.preventDefault()
    })
    this.orderCloseDom.on('mouseleave', function (e) {
        if (!_this.unfoldOrClose) {
            _this.closeFun()
        }
    })
}
listDown.prototype.clickHandle = function () {
    var _this = this
    this.selectDom.on(this.eventType, function (e) {
        if (_this.unfoldOrClose) {
            // 展开
            _this.openFun()
        } else {
            // 闭合
            _this.closeFun()
        }
    })
}
listDown.prototype.closeFun = function () {
    var h = this.optionDom.outerHeight() + 1
    // 闭合
    this.f(1)
    this.unfoldAnimate(h, 1)
}
listDown.prototype.openFun = function () {
    var h = this.optionDom.outerHeight() + 1
    // 展开
    this.f()
    this.unfoldAnimate(h)
}
listDown.prototype.unfoldAnimate = function (h, x) {
    var _this = this
    if (x) {
        h -= this.v
        if (h <= 0) {
            window.cancelAnimationFrame(this.timer)
            this.unfoldOrClose = true
        } else {
            this.timer = window.requestAnimationFrame(function (time) {
                _this.unfoldAnimate(h, x)
            })
        }
    } else {
        h += this.v
        if (this.h) {
            if (h >= this.h) {
                window.cancelAnimationFrame(this.timer)
                this.unfoldOrClose = false
            } else {
                this.timer = window.requestAnimationFrame(function (time) {
                    _this.unfoldAnimate(h, x)
                })
            }
        } else {
            this.addHeight[0] = this.addHeight[1]
            this.addHeight[1] = this.optionDom.height()
            if (this.addHeight[0] === this.addHeight[1]) {
                window.cancelAnimationFrame(this.timer)
                this.unfoldOrClose = false
            } else {
                this.timer = window.requestAnimationFrame(function (time) {
                    _this.unfoldAnimate(h, x)
                })
            }
        }

    }
    if (this.h) {
        if (h < 0) {
            this.optionDom.css({'height': 0, 'opacity': 0})
        } else {
            this.optionDom.css({'height': h + 'px', 'opacity': 1})
        }
    } else {
        if (h < 0) {
            this.optionDom.css('max-height', 0)
        } else {
            this.optionDom.css('max-height', h + 'px')
        }
    }


}
// adddata
listDown.prototype.addData = function () {
    var _this = this
    this.optionDom.on('click', function (e) {
        var target = e.target
        if (_this.hideSelectDom[0]) {
            _this.hideSelectDom[0].value = _this.hideSelectDom.children().eq($(target).index()).val()
            if (window.dispatchEvent) {
                var event = document.createEvent('HTMLEvents')
                event.initEvent('change', true, true)
                _this.hideSelectDom[0].children[$(target).index()].dispatchEvent(event)
            } else {
                _this.hideSelectDom[0].children[$(target).index()].fireEvents(event)
            }
            _this.hideSelectDom.children().eq($(target).index())
        }
        _this.showDataDom.text($(target).text())
        _this.closeFun()
    })
}
