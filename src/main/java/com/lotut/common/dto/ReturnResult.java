package com.lotut.common.dto;


import com.lotut.common.constants.ResultConstants;

import java.util.List;
import java.util.Map;

import static com.lotut.common.constants.ResultConstants.FAILURE;


/**
 * 服务器返回客户端的结果对象，该对象为JSON格式
 *
 * @author zjliu
 * @copyright 
 * @date 2018/6/8 10:23
 */
public class ReturnResult {
    public static final int ERROR_CODE = 500;
    public static final int SUCCESS_CODE = 0;
    public static final int REPEAT_SUBMIT_CODE = 300;
    public static final int REPEAT_UPLOAD_CODE = 400;

    private static final ReturnResult SUCCESS_RESULT = new ReturnResult(SUCCESS_CODE, ResultConstants.SUCCESS);
    private static final ReturnResult ERROR_RESULT = new ReturnResult(ERROR_CODE, FAILURE);
    private static final ReturnResult REPEAT_SUBMIT_RESULT = new ReturnResult(REPEAT_SUBMIT_CODE, FAILURE);
    private static final ReturnResult REPEAT_UPLOAD_RESULT = new ReturnResult(REPEAT_UPLOAD_CODE, FAILURE);

    /**
     * 响应状态
     */
    private int code;

    /**
     * 响应状态原因描述
     */
    private String msg;

    /**
     * 返回数据总条数
     */
    private Long count;

    /**
     * 返回请求端的具体业务数据
     */
    private Object data;

    /**
     * es查询总命中数
     */
    private Long totalHit;
    /**
     * es查询耗时
     */
    private Double searchTime;

    private Map<String, List<String>> highlightMap;

    public ReturnResult(int code) {
        this(code, null);
    }

    public ReturnResult(int code, String msg) {
        this(code, msg, null);
    }

    public ReturnResult(int code, Object data) {
        this(code, null, data);
    }

    public ReturnResult(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ReturnResult(int code, String msg, Long count, Object data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }

    public ReturnResult(int code, String msg, Long count, Object data, Map<String, List<String>> highlightMap) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
        this.highlightMap = highlightMap;
    }

    public ReturnResult(int code, String msg, Long count, Object data, Double searchTime, Long totalHit) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
        this.searchTime = searchTime;
        this.totalHit = totalHit;
    }

    public ReturnResult(int code, String msg, Long count, Object data, Double searchTime, Long totalHit, Map<String, List<String>> highlightMap) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
        this.searchTime = searchTime;
        this.totalHit = totalHit;
        this.highlightMap = highlightMap;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Long getTotalHit() {
        return totalHit;
    }

    public void setTotalHit(Long totalHit) {
        this.totalHit = totalHit;
    }

    public Double getSearchTime() {
        return searchTime;
    }

    public void setSearchTime(Double searchTime) {
        this.searchTime = searchTime;
    }

    public Map<String, List<String>> getHighlightMap() {
        return highlightMap;
    }

    public void setHighlightMap(Map<String, List<String>> highlightMap) {
        this.highlightMap = highlightMap;
    }

    public static ReturnResult successResult() {
        return SUCCESS_RESULT;
    }

    public static ReturnResult successResult(Object data) {
        return successResult(null, data);
    }

    public static ReturnResult successResult(String msg, Object data) {
        return new ReturnResult(SUCCESS_CODE, msg, data);
    }

    public static ReturnResult successResult(long count, Object data) {
        return successResult(null, count, data);
    }

    public static ReturnResult successResult(String msg, Long count, Object data) {
        return new ReturnResult(SUCCESS_CODE, msg, count, data);
    }

    public static <T> ReturnResult successResultByPage(Page<T> page) {
        return new ReturnResult(SUCCESS_CODE, ResultConstants.SUCCESS, (long) page.getTotalCount(), page.getList());
    }

    public static <T> ReturnResult successResultByPage(Page<T> page, Map<String, List<String>> highlightMap) {
        return new ReturnResult(SUCCESS_CODE, ResultConstants.SUCCESS, (long) page.getTotalCount(), page.getList(), highlightMap);
    }

    public static <T> ReturnResult successEsResultByPage(Page<T> page) {
        return new ReturnResult(SUCCESS_CODE, ResultConstants.SUCCESS, (long) page.getTotalCount(), page.getList(), page.getSearchTime(), page.getTotalHit());
    }

    public static <T> ReturnResult successEsResultByPage(Page<T> page, Map<String, List<String>> highlightMap) {
        return new ReturnResult(SUCCESS_CODE, ResultConstants.SUCCESS, (long) page.getTotalCount(), page.getList(), page.getSearchTime(), page.getTotalHit(), highlightMap);
    }

    public static <T> ReturnResult successSecretResultByPage(Page<T> page) {
        return new ReturnResult(SUCCESS_CODE, ResultConstants.SUCCESS, (long) page.getTotalCount(), page.getDataText(), page.getSearchTime(), page.getTotalHit());
    }

    public static <T> ReturnResult successSecretResultByPage(Page<T> page, Map<String, List<String>> highlightMap) {
        return new ReturnResult(SUCCESS_CODE, ResultConstants.SUCCESS, (long) page.getTotalCount(), page.getDataText(), page.getSearchTime(), page.getTotalHit(), highlightMap);
    }

    public static ReturnResult errorResult() {
        return ERROR_RESULT;
    }

    public static ReturnResult errorRepeatSubmitResult() {
        return REPEAT_SUBMIT_RESULT;
    }

    public static ReturnResult errorRepeatUploadResult() {
        return REPEAT_UPLOAD_RESULT;
    }

    public static ReturnResult errorResult(String msg) {
        return new ReturnResult(ERROR_CODE, msg);
    }

    public static ReturnResult errorResult(String msg, Object data) {
        return new ReturnResult(ERROR_CODE, msg, data);
    }

    public static ReturnResult result(boolean success) {
        if (success) {
            return SUCCESS_RESULT;
        }

        return ERROR_RESULT;
    }

    public static ReturnResult result(boolean success, String msg) {
        return result(success, msg, null);
    }

    public static ReturnResult result(boolean success, Object data) {
        return result(success, null, data);
    }

    public static ReturnResult result(boolean success, String msg, Object data) {
        if (success) {
            return successResult(msg, data);
        }

        return errorResult(msg, data);
    }

    public static ReturnResult result(int code, String msg) {
        return result(code, msg, null);
    }

    public static ReturnResult result(int code, String msg, Object data) {
        return new ReturnResult(code, msg, data);
    }
}
