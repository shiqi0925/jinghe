package com.lotut.news.service;

import java.util.List;

import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.news.dto.WebsiteNewsChannelDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;

/**
 * @author lidc
 * @date 2020-03-12 13:38
 */
public interface WebsiteNewsChannelService {

    /**
     * 查询二级频道，WebsiteChannelDto接收
     * @param parentId
     * @return
     */
    List<WebsiteNewsChannelDto> selectSecondChannelByParentId(Long parentId);

    /**
     * 查询网站一级频道
     * @param websiteId
     * @return
     */
    List<WebsiteNewsChannel> selectParentChannelList(Long websiteId);

    /**
     * 查询二级频道
     * @param parentId
     * @return
     */
    List<WebsiteNewsChannel> selectSecondChannelList(Long websiteId);

    /**
     * 查询频道详情
     * @param recordId
     * @return
     */
    WebsiteNewsChannel selectByPrimaryKey(Long recordId,Long websiteId);
    /**
     * 获取当前用户所有的频道
     * @param userId
     * @return
     */
    List<WebsiteNewsChannel> getUserAllChannel(WebsiteNewsSearchCondition searchCondition);
    /**
     * 根据Id获取频道
     * @param recordId
     * @return
     */
    WebsiteNewsChannel getUserChannelById(Long recordId);
    /**
     * 查询当前用户所有的一级频道
     * @param userId
     * @return
     */
    List<WebsiteNewsChannel> getParentChannel(Long websiteId);
    /**
     * 更新排序
     * @param recordId
     * @return
     */
    int updateDisplayOrder(Integer displayOrder,Long recordId);
    /**
     * 更新名称
     * @param name
     * @param recordId
     * @return
     */
    int updateChannelName(String name,Long recordId);
    /**
     * 更新状态
     * @param status
     * @param recordId
     * @return
     */
    int updateStatus(Integer status,Long recordId);
    /**
     * 
     * @param defaultFlag
     * @param recordId
     * @return
     */
    int updateDefaultFlag( Integer defaultFlag, Long recordId);
    /**
     * 添加频道
     * @param channel
     * @return
     */
    int addUserWebsiteChannel(WebsiteNewsChannel channel);
}
