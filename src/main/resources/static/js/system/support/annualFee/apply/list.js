layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    laydate.render({
        elem: '#startApplyTime',
        type: 'date',
        format: 'yyyy-MM-dd',
    });
    laydate.render({
        elem: '#endApplyTime',
        type: 'date',
        format: 'yyyy-MM-dd',
    });

    // 标签表格
    table.render({
        elem: '#annualFeeSupportAppList',
        url: '/support/annualFee/apply/list.html',
        method: 'post',
        height: "full-150",
        id: "annualFeeSupportAppList",
        page: true,
        limits: [20, 50, 100],
        limit: 20,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '申请号', align: "center"},
            {field: 'patentName', title: '名称', align: "center"},
            {field: 'appPerson', title: '申请（专利权）人', align: "center"},
            {field: 'patentType', title: '专利类型', align: "center"},
            {field: 'appDate', title: '申请日', align: "center"},
            {field: 'lprs', title: '法律状态', align: "center"},
            {field: 'annualFeeYear', title: '年费年度', align: "center"},
            {field: 'approvalAmount', title: '资助金额', align: "center"},
            {field: 'supportStatusStr', title: '资助状态', align: "center"},
            {title: '操作', minWidth: 100, templet: '#annualFeeSupportAppListBar', fixed: "right", align: "center"}
        ]]
    });

    // 刷新表格
    function reload() {
        table.reload("annualFeeSupportAppList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $('#keyword').val(),
                supportStatus: $('#supportStatus').val(),
                source: $('#searchSource').val(),
                startApplyTime: $('#startApplyTime').val(),
                endApplyTime: $('#endApplyTime').val(),
            }
        })
    }

    //搜索
    $(".search_btn").on("click", function () {
        $('.support-status').removeClass("hover");
        reload();
    });

    $('.support-status').on("click", function () {
        $('.support-status').removeClass("hover");
        $(this).addClass('hover');
    })

    // 点击未申请
    $("#notApply").on("click", function () {
        $("#status").val(0);
        reloadBySupportStatus(0);
    });
    // 点击申请中
    $("#applying").on("click", function () {
        $("#status").val(1);
        reloadBySupportStatus(1);
    });
    // 点击审批通过
    $("#approve").on("click", function () {
        $("#status").val(2);
        reloadBySupportStatus(2);
    });
    // 点击驳回申请
    $("#rejected").on("click", function () {
        $("#status").val(3);
        reloadBySupportStatus(3);
    });
    // 点击视为放弃
    $("#abandon").on("click", function () {
        $("#status").val(4);
        reloadBySupportStatus(4);
    });
    // 点击资助完成
    $("#success").on("click", function () {
        $("#status").val(5);
        reloadBySupportStatus(5);
    });

    // 根据资助状态刷新表格
    function reloadBySupportStatus(supportStatus) {
        $('#keyword').val('');
        $('#supportStatus').val('');
        $('#searchSource').val('');
        $('#startApplyTime').val('');
        $('#endApplyTime').val('');
        form.render();
        table.render({
            elem: '#annualFeeSupportAppList',
            url: '/support/annualFee/apply/list.html',
            method: 'post',
            where: {supportStatus: supportStatus},
            height: "full-150",
            id: "annualFeeSupportAppList",
            page: true,
            limits: [20, 50, 100],
            limit: 20,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '申请号', align: "center"},
                {field: 'patentName', title: '名称', align: "center"},
                {field: 'appPerson', title: '申请（专利权）人', align: "center"},
                {field: 'patentType', title: '专利类型', align: "center"},
                {field: 'appDate', title: '申请日', align: "center"},
                {field: 'lprs', title: '法律状态', align: "center"},
                {field: 'annualFeeYear', title: '年费年度', align: "center"},
                {field: 'approvalAmount', title: '资助金额', align: "center"},
                {field: 'supportStatusStr', title: '资助状态', align: "center"},
                {title: '操作', minWidth: 100, templet: '#annualFeeSupportAppListBar', fixed: "right", align: "center"}
            ]]
        });
    }

    // 申请
    function apply(data) {
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.post("/support/annualFee/apply/applySupport.html", {
            appNo: data.appNo,
            annualFeeYear: data.annualFeeYear
        }, function (data) {
            if (data.code == 0) {
                layer.msg("申请已提交");
                location.reload();
                layer.close(index);
            } else {
                layer.msg(data.msg);
            }
        })
        // layer.prompt({
        //     formType: 0,
        //     title: '请输入申请金额'
        // }, function (value, index, elem) {
        //     var appAmount = value.trim();
        //     if (!checkMoney(appAmount)) {
        //         layer.msg("请输入有效金额，例如：2000");
        //         return;
        //     } else {
        //         layer.close(index);
        //         var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        //         $.post("/support/annualFee/apply/applySupport.html", {
        //             appAmount: appAmount,
        //             patentId: data.patentId,
        //             annualFeeYear: data.annualFeeYear
        //         }, function (data) {
        //             if (data.code == 0) {
        //                 layer.msg("申请已提交");
        //                 reload();
        //                 layer.close(index);
        //             } else {
        //                 layer.msg(data.msg);
        //             }
        //         })
        //     }
        // });
    }

    //列表操作
    table.on('tool(annualFeeSupportAppList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'apply') {
            apply(data);
        }
    });

    //批量申请
    // $(".batchApply").on("click", function () { //获取选中数据
    //     var data = table.checkStatus('annualFeeSupportAppList').data;
    //
    //     var ids = new Array();
    //     for (var i = 0; i < data.length; i++) {
    //         ids.push(data[i].id);
    //     }
    //     if (data.length === 0) {
    //         //全部提交申请
    //         $.ajax({
    //             url: "/support/annualFee/apply/batchApply.html",
    //             type: "post",
    //             data: {ids: ids.join(",")},
    //             success: function (res) {
    //
    //             }
    //         });
    //     }
    //
    // });

    // 构造搜索条件
    // function buildSearchCondition() {
    //     var condition = {
    //         keyword: $('#keyword').val(),
    //         supportStatus: $('#supportStatus').val(),
    //     }
    //     return condition;
    // }

    // //导出excel表格
    // $(".getCheckData").on("click", function () { //获取选中数据
    //     var data = table.checkStatus('annualFeeSupportAppList').data;
    //     conditionValue = buildSearchCondition();
    //     var s = $("#status").val();
    //     if (s != null && s !== '') {
    //         conditionValue.supportStatus = s;
    //     }
    //     if (data.length === 0) {
    //         //为选择数据，导出全部
    //         $.ajax({
    //             url: "/support/annualFee/apply/exportAll.html",
    //             type: "post",
    //             data: conditionValue,
    //             success: function (res) {
    //                 if (res.code === 0) {
    //                     layer.msg("表格导出成功！");
    //                     $("#keyword").val('');
    //                     $("#searchSupportStatus").val('');
    //                     location.reload();
    //                 } else {
    //                     lay.msg("导出失败！");
    //                 }
    //             }
    //         });
    //         return false;
    //     } else if (data.length !== 0) {
    //         var arrTableData = JSON.parse(JSON.stringify(data));
    //         var ids = '';
    //         for (var i = 0; i < arrTableData.length; i++) {
    //             ids += arrTableData[i].id + ",";
    //         }
    //         $.ajax({
    //             url: "/support/annualFee/apply/excel.html",
    //             type: "post",
    //             data: {"ids": ids},
    //             success: function (res) {
    //                 if (res.code === 0) {
    //                     layer.msg("表格导出成功！");
    //                     location.reload();
    //                 } else {
    //                     lay.msg("导出失败！");
    //                 }
    //             }
    //         });
    //     }
    // })

    // 手动添加发明年费资助
    $("#addGrant").on("click", function () {
        var index = layui.layer.open({
            title: '添加发明年费资助',
            type: 2,
            area: ['100%', '100%'],
            content: '/support/annualFee/apply/add.html',
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回发明年费资助列表', '.layui-layer-setwin .layui-layer-close', {tips: 3});
                }, 500);
            }
        });
    });

    //批量申请
    $(".batchApply").on("click", function () { //获取选中数据
        var data = table.checkStatus('annualFeeSupportAppList').data;
        if (data.length === 0) {
            layer.msg("请选择要提交的申请", {time: 1500});
            return false;
        }
        var appNo = new Array(), annualFeeYear = new Array(), index;
        for (var i = 0; i < data.length; i++) {
            appNo.push(data[i].appNo);
            annualFeeYear.push(data[i].annualFeeYear);
        }
        $.ajax({
            url: "/support/annualFee/apply/batchApply.html",
            type: "post",
            data: {"appNos": appNo.join(','), "annualFeeYears": annualFeeYear.join(',')},
            beforeSend: function () {
                index = layer.msg('提交申请中，请稍后...', {
                    icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                    offset: 'auto',
                    time: 60 * 60 * 1000
                })
            },
            success: function (res) {
                if (res.code === 0) {
                    layer.close(index);
                    if (res.data !== null && res.data !== "") {
                        layer.open({
                            title: '提示'
                            , content: "以下专利已被申请(或您已提交申请),请勿再次提交" + res.data
                            , btn: ['确定']
                            , yes: function (index, layero) {
                                layer.close(index);
                                location.reload();
                            }, cancel: function () {
                                layer.close(index);
                                location.reload();
                            }
                        });
                        return false;
                    }
                    parent.layer.msg("已提交申请！", {time: 1500});
                    location.reload();
                } else {
                    lay.msg("申请失败！");
                }
            }
        });
    });
});