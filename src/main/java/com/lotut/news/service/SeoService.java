package com.lotut.news.service;

import java.util.List;

import com.lotut.news.domain.Seo;


public interface SeoService {
  
    /**
     * 查询列表
     * @param condition
     * @return
     */
    List<Seo> selectList();

   
    Seo getById(Long id);
    
    boolean save(Seo seo);
}
