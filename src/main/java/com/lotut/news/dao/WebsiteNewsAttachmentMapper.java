package com.lotut.news.dao;

import com.lotut.news.domain.WebsiteNewsAttachment;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface WebsiteNewsAttachmentMapper {
    int deleteByPrimaryKey(Long recordId);

    int insert(WebsiteNewsAttachment record);

    int insertSelective(WebsiteNewsAttachment record);

    WebsiteNewsAttachment selectByPrimaryKey(Long recordId);

    int updateByPrimaryKeySelective(WebsiteNewsAttachment record);

    int updateByPrimaryKey(WebsiteNewsAttachment record);
}