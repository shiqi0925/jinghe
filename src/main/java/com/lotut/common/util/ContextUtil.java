package com.lotut.common.util;


import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Wang JunJie
 * @date 2018/8/20 16:37.
 */
@Component
public class ContextUtil implements ApplicationContextAware {

    public static ApplicationContext current;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ContextUtil.current = applicationContext;
    }

    public static <T> T getBean(String name, Class<T> requiredType)
            throws BeansException {
        return current.getBean(name, requiredType);
    }

    public static <T> T getBean(Class<T> clz) throws BeansException {
        return (T) current.getBean(clz);
    }

}
