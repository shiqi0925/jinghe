package com.lotut.system.config;

import com.lotut.system.shiro.realm.UserRealm;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import net.sf.ehcache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.io.ResourceUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * shiro配置
 *
 * @author xwj
 * @date 2020/3/4
 */
@Configuration
public class ShiroConfig {
    /**
     * 自定义Realm
     */
    @Bean
    public UserRealm userRealm() {
        return new UserRealm();
    }

    /**
     * 安全管理器
     */
    @Bean
    public SecurityManager securityManager(UserRealm userRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        // 设置realm
        securityManager.setRealm(userRealm);
        // 设置缓存管理器
        securityManager.setCacheManager(ehCacheManager());
        // 设置session管理器
        securityManager.setSessionManager(sessionManager());
        return securityManager;
    }

    /**
     * Shiro过滤器配置
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // Shiro的核心安全接口,这个属性是必须的
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        // 身份认证失败，则跳转到登录页面的配置
        shiroFilterFactoryBean.setLoginUrl("/user/login.html");
        // 权限认证失败，则跳转到指定页面
        shiroFilterFactoryBean.setUnauthorizedUrl("/user/login.html");
        // Shiro连接约束配置，即过滤链的定义
        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<>();

        // 对静态资源设置匿名访问
        filterChainDefinitionMap.put("/static/**", "anon");
        filterChainDefinitionMap.put("/components/**", "anon");
        filterChainDefinitionMap.put("/html/**", "anon");
        filterChainDefinitionMap.put("/css/**", "anon");
        filterChainDefinitionMap.put("/layui/**", "anon");
        filterChainDefinitionMap.put("/js/**", "anon");
        filterChainDefinitionMap.put("/news/css/**", "anon");
        filterChainDefinitionMap.put("/news/images/**", "anon");
        filterChainDefinitionMap.put("/news/js/**", "anon");
        filterChainDefinitionMap.put("/bootstrap/**", "anon");
        filterChainDefinitionMap.put("/style/**", "anon");
        filterChainDefinitionMap.put("/images/**", "anon");
        filterChainDefinitionMap.put("/api/**", "anon");
        filterChainDefinitionMap.put("/favicon.ico", "anon");
        filterChainDefinitionMap.put("/xpp-pOzBykGF.site.verify.html", "anon");
        filterChainDefinitionMap.put("/sitemap.xml", "anon");
        filterChainDefinitionMap.put("/qzsitemap.html", "anon");
        // 附件下载接口
        filterChainDefinitionMap.put("/news/files/**", "anon");
        // 查看图片
        filterChainDefinitionMap.put("/news/img/**", "anon");
        filterChainDefinitionMap.put("/brand_img/**", "anon");
        /*=====================系统前台shiro访问控制=====================*/
        filterChainDefinitionMap.put("/service/**", "anon");
        filterChainDefinitionMap.put("/brand/**", "anon");
        /*filterChainDefinitionMap.put("/admin/**", "anon");*/
        filterChainDefinitionMap.put("/admin_static/**", "anon");
       /* filterChainDefinitionMap.put("/admin/login", "anon");*/
        filterChainDefinitionMap.put("/calculator/index", "anon");
        filterChainDefinitionMap.put("/trade/index", "anon");
        filterChainDefinitionMap.put("/goods/**", "anon");
        filterChainDefinitionMap.put("/market/**", "anon");
        filterChainDefinitionMap.put("/zging/**", "anon");
        filterChainDefinitionMap.put("/news/detail.html", "anon");
        filterChainDefinitionMap.put("/shop/**", "anon");
        filterChainDefinitionMap.put("/message/**", "anon");
        filterChainDefinitionMap.put("/generate/**", "anon");
      
 
        /*=====================系统前台shiro访问控制=====================*/
        filterChainDefinitionMap.put("/user/login.html", "anon");
        filterChainDefinitionMap.put("/login", "anon");
        // 用户注册相关
        filterChainDefinitionMap.put("/user/register.html/**", "anon");
        filterChainDefinitionMap.put("/user/save/**", "anon");
        filterChainDefinitionMap.put("/user/validate/**", "anon");//验证用户名是否重复
        filterChainDefinitionMap.put("/login_style/**", "anon");//验证用户名是否重复
        filterChainDefinitionMap.put("/user/loginApi", "anon");//登录接口
        
        
        // 找回密码相关
        filterChainDefinitionMap.put("/user/pass/**", "anon");
        // 新闻首页
        filterChainDefinitionMap.put("/news/index", "anon");
        // 网上信访
        filterChainDefinitionMap.put("/online/petition", "anon");
        // 咨询台
        filterChainDefinitionMap.put("/information/counter", "anon");
        //前台商标交易查询页面  
        filterChainDefinitionMap.put("/shop_brand_search/**", "anon");
        filterChainDefinitionMap.put("/shop_patent_search/**", "anon");
        filterChainDefinitionMap.put("/bgoods/**", "anon");
        filterChainDefinitionMap.put("/pgoods/**", "anon");
        
        
        filterChainDefinitionMap.put("/list.html", "anon");
        filterChainDefinitionMap.put("/detail.html", "anon");
        filterChainDefinitionMap.put("/shiping.html", "anon");
        
        /*=====================系统前台shiro访问控制=====================*/
        filterChainDefinitionMap.put("/Public/**", "anon");
        filterChainDefinitionMap.put("/goods_list.html", "anon");
        filterChainDefinitionMap.put("/goods_detail.html", "anon");
        filterChainDefinitionMap.put("/news_list.html", "anon");
        filterChainDefinitionMap.put("/news_detail.html", "anon");
        filterChainDefinitionMap.put("/about.html", "anon");
        filterChainDefinitionMap.put("/connect.html", "anon"); 
        /*=====================系统前台shiro访问控制=====================*/

        /*=====================系统后台shiro访问控制=====================*/
        filterChainDefinitionMap.put("/", "anon");
        /*=====================系统后台shiro访问控制=====================*/
        Map<String, Filter> filters = new LinkedHashMap<>();
        shiroFilterFactoryBean.setFilters(filters);
        // 所有请求需要认证
        filterChainDefinitionMap.put("/**", "authc");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }
    /**
     * thymeleaf模板引擎和shiro框架的整合
     */
    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }
    /**
     * 用来管理shiro一些bean的生命周期
     */
    @Bean
    public static LifecycleBeanPostProcessor getLifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    /**
     * 配置注解生效
     *
     * @return
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
 
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    /**
     * shiro缓存管理器
     *
     * @return
     */
    @Bean
    public EhCacheManager ehCacheManager() {
        EhCacheManager em = new EhCacheManager();
        em.setCacheManager(getEhCacheManager());
        return em;
    }

    /**
     * 缓存管理器 使用Ehcache实现
     */
    @Bean
    public CacheManager getEhCacheManager() {
        CacheManager cacheManager = CacheManager.getCacheManager("xsms-cache-manager");
        if (cacheManager == null) {
            try {
                cacheManager = CacheManager.create(ResourceUtils.getInputStreamForPath("classpath:ehcache/ehcache.xml"));
            } catch (IOException e) {
                throw new RuntimeException("initialize cacheManager failed");
            }
        }
        return cacheManager;
    }

    /**
     * shiro session的管理
     */
    @Bean
    public DefaultWebSessionManager sessionManager() {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        // session超时时间（单位毫秒）
        sessionManager.setGlobalSessionTimeout(60 * 1000 * 60 * 24);
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }
}
