layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    laydate.render({
        elem: '#startApplyTime',
        type: 'date',
        format: 'yyyy-MM-dd',
    });
    laydate.render({
        elem: '#endApplyTime',
        type: 'date',
        format: 'yyyy-MM-dd',
    });

    // 标签表格
    table.render({
        elem: '#proxyOrgSupportList',
        url: '/support/proxyOrg/list.html',
        method: 'post',
        where: {supportStatus: $('#navigationSupportStatus').val()},
        height: "full-180",
        id: "proxyOrgSupportList",
        page: true,
        limits: [10, 20, 50, 100],
        limit: 10,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
            {field: 'grantDate', title: '授权公告日/申请资助时间', width: 200, align: "center", templet: '#grantDateBar'},
            {field: 'proxyOrg', title: '代理机构/资助申请人', width: 300, align: "center", templet: '#proxyOrgBar'},
            {field: 'approvalAmount', title: '资助金额', edit: 'text', width: 120, align: "center"},
            {field: 'supportStatusStr', title: '资助状态', width: 86, align: "center", templet: '#supportStatusBar'},
            {
                field: 'remark', title: '备注', width: 66, align: "center", templet: function (ele) {
                    return !ele.remark ? '<div class="nodata-remark" data-remark="" data-id="' + ele.id + '">暂无</div>' : '<div class="curr-remark" data-remark="' + ele.remark + '" data-id="' + ele.id + '"><img src="/images/markmsg.png" </div';
                }
            },
        ]]
    });

    // 构造搜索条件
    function buildSearchCondition() {
        var condition = {
            keyword: $('#keyword').val(),
            supportStatus: $('#searchSupportStatus').val(),
            source: $('#searchSource').val(),
            startApplyTime: $('#startApplyTime').val(),
            endApplyTime: $('#endApplyTime').val(),
        }
        return condition;
    }

    // 刷新表格
    function reload() {
        $('.support-status').removeClass("hover");
        table.render({
            elem: '#proxyOrgSupportList',
            url: '/support/proxyOrg/list.html',
            method: 'post',
            where: buildSearchCondition(),
            height: "full-180",
            id: "proxyOrgSupportList",
            page: true,
            limits: [10, 20, 50, 100],
            limit: 10,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
                {field: 'grantDate', title: '授权公告日/申请资助时间', width: 200, align: "center", templet: '#grantDateBar'},
                {field: 'proxyOrg', title: '代理机构/资助申请人', width: 300, align: "center", templet: '#proxyOrgBar'},
                {field: 'approvalAmount', title: '资助金额', edit: 'text', width: 120, align: "center"},
                {field: 'supportStatusStr', title: '资助状态', width: 86, align: "center", templet: '#supportStatusBar'},
                {
                    field: 'remark', title: '备注', width: 66, align: "center", templet: function (ele) {
                        return !ele.remark ? '<div class="nodata-remark" data-remark="" data-id="' + ele.id + '">暂无</div>' : '<div class="curr-remark" data-remark="' + ele.remark + '" data-id="' + ele.id + '"><img src="/images/markmsg.png" </div';
                    }
                },
            ]]
        });
    }

    // 根据资助状态刷新表格
    function reloadBySupportStatus(supportStatus) {
        $("#keyword").val('');
        $("#searchSupportStatus").val('');
        $("#searchSource").val('');
        $("#startApplyTime").val('');
        $("#endApplyTime").val('');
        form.render();
        table.render({
            elem: '#proxyOrgSupportList',
            url: '/support/proxyOrg/list.html',
            method: 'post',
            where: {supportStatus: supportStatus},
            height: "full-180",
            id: "proxyOrgSupportList",
            page: true,
            limits: [10, 20, 50, 100],
            limit: 10,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
                {field: 'grantDate', title: '授权公告日/申请资助时间', width: 200, align: "center", templet: '#grantDateBar'},
                {field: 'proxyOrg', title: '代理机构/资助申请人', width: 300, align: "center", templet: '#proxyOrgBar'},
                {field: 'approvalAmount', title: '资助金额', edit: 'text', width: 120, align: "center"},
                {field: 'supportStatusStr', title: '资助状态/信息来源', align: "center", templet: '#supportStatusBar'},
                {
                    field: 'remark', title: '备注', width: 66, align: "center", templet: function (ele) {
                        return !ele.remark ? '<div class="nodata-remark" data-remark="" data-id="' + ele.id + '">暂无</div>' : '<div class="curr-remark" data-remark="' + ele.remark + '" data-id="' + ele.id + '"><img src="/images/markmsg.png" </div';
                    }
                },
            ]]
        });
    }

    //备注悬浮事件
    $("body").on("click", ".curr-remark,.nodata-remark", function (e) {
        var _this = $(this);
        var id = _this.attr("data-id")
        // $("#remakform input").val(_this.attr("data-remark"))
        layui.layer.open({
            type: 1
            , title: "备注"
            , id: 'layerDemo' + id //防止重复弹出
            , content: $("#remakform")
            , btn: '提交'
            , area: ["400px", "270px"]
            , btnAlign: 'c' //按钮居中
            , shade: 0 //不显示遮罩
            , success: function (layero, index) {
                $('#remakform')[0].reset();
                $("#remakform textarea").val(_this.attr("data-remark"));
                form.render();
            }
            , yes: function () {
                var cumarkhtml = $("#remakform textarea").val()
                // console.log(cumarkhtml)
                if (!cumarkhtml) {
                    layui.layer.msg("请输入备注内容")
                    return
                }
                $.post("/support/proxyOrg/remark.html", {
                    id: id,
                    remark: cumarkhtml
                }, function (data) {
                    if (!_this.attr("data-remark")) {
                        _this.parent().html('<div class="curr-remark" data-remark="' + cumarkhtml + '" data-id="' + _this.attr("data-id") + '"><img src="/images/markmsg.png" /></div>')
                    } else {
                        _this.attr("data-remark", cumarkhtml)
                    }
                    layui.layer.closeAll();
                })

            }
        });
    })
    var remarkindex = "";
    $("body").on("mouseenter", ".curr-remark", function () {
        var _this = $(this);
        remarkindex = layui.layer.tips(_this.attr("data-remark"), _this, {
            tips: [1, '#1e9fff'],
            time: 0,
            area: ['auto', 'auto'],
            maxWidth: '500px'
        });
    });
    $("body").on("mouseleave", ".curr-remark", function () {
        layui.layer.close(remarkindex)
    });
    //搜索
    $(".search_btn").on("click", function () {
        reload();
    });

    $("#keyword").keypress(function (e) {
        if (e.which == 13) {
            reload();
        }
    });

    $('.support-status').on("click", function () {
        $('.support-status').removeClass("hover");
        $(this).addClass('hover');
        $("#keyword").val('');
    })

    // 点击未申请
    $("#notApply").on("click", function () {
        $("#status").val(0);
        reloadBySupportStatus(0);
    });
    // 点击申请中
    $("#applying").on("click", function () {
        $("#status").val(1);
        reloadBySupportStatus(1);
    });
    // 点击审批通过
    $("#approve").on("click", function () {
        $("#status").val(2);
        reloadBySupportStatus(2);
    });
    // 点击驳回申请
    $("#rejected").on("click", function () {
        $("#status").val(3);
        reloadBySupportStatus(3);
    });
    // 点击视为放弃
    $("#abandon").on("click", function () {
        $("#status").val(4);
        reloadBySupportStatus(4);
    });
    // 点击资助完成
    $("#success").on("click", function () {
        $("#status").val(5);
        reloadBySupportStatus(5);
    });

    // 批量修改审批金额
    function batchUpdateApprovalAmount(appNos, approvalAmount, refreshFlag) {
        if (refreshFlag) {
            var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
            $.ajax({
                url: "/support/proxyOrg/batchUpdateApprovalAmount.html",
                type: "post",
                data: {appNos: appNos.join(","), approvalAmount: approvalAmount},
                success: function (res) {
                    if (res.code == 0) {
                        setTimeout(function () {
                            layer.close(index);
                            layer.msg("修改成功");
                            reload();
                        }, 1000);
                    } else {
                        setTimeout(function () {
                            layer.msg(res.msg);
                        }, 2000);
                    }
                },
            });
        } else {
            $.ajax({
                url: "/support/proxyOrg/batchUpdateApprovalAmount.html",
                type: "post",
                data: {appNos: appNos.join(","), approvalAmount: approvalAmount},
                success: function (res) {
                },
            });
        }
    }

    //监听单元格编辑
    table.on('edit(proxyOrgSupportList)', function (obj) {
        var value = obj.value
            , data = obj.data
            , field = obj.field;
        if (field == 'remark') {
            $.post("/support/proxyOrg/remark.html", {
                id: data.id,
                remark: value
            }, function (data) {
            })
        } else if(field == 'approvalAmount') {
            var approvalAmount = value.trim();
            if (!checkMoney(approvalAmount)) {
                layer.msg("请输入有效金额，例如：2000");
            } else {
                var appNos = [];
                appNos.push(data.appNo);
                batchUpdateApprovalAmount(appNos, approvalAmount, false);
            }
        }
    });

    // 批量审批通过
    $(".batch-pass").on("click", function () {
        var checkStatus = table.checkStatus('proxyOrgSupportList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var appNos = [];
            var appUserIds = [];
            $.each(checkStatus.data, function (i, val) {
                if (val.supportStatus == 1) {
                    appNos.push(val.appNo);
                    appUserIds.push(val.appUserId);
                } else {
                    layer.msg("请选择申请中的专利");
                    return;
                }
            });
            if (appNos.length > 0) {
                layer.prompt({
                    formType: 0,
                    title: '请输入资助金额'
                }, function (value, index, elem) {
                    var approvalAmount = value.trim();
                    if (!checkMoney(approvalAmount)) {
                        layer.msg("请输入有效金额，例如：2000");
                        return;
                    } else {
                        layer.close(index);
                        $.post("/support/proxyOrg/batchPass.html", {
                            appNos: appNos.join(","),
                            appUserIds: appUserIds.join(','),
                            approvalAmount: approvalAmount
                        }, function (data) {
                            if (data.code == 0) {
                                // reload();
                                location.reload();
                            } else {
                                layer.msg(data.msg);
                            }
                        })
                    }
                });
            }
        } else {
            layer.msg("请先选择要审批通过的专利");
        }
    });

    // 批量修改金额
    $(".batch-update-amount").on("click", function () {
        var checkStatus = table.checkStatus('proxyOrgSupportList');
        var len = checkStatus.data.length;
        if (len > 0) {
            $("#approvalAmount").val("");
            var idx = layui.layer.open({
                title: "批量修改资助金额",
                type: 1,
                area: ['400px', '170px'],
                content: $("#updateAmountForm"),
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);
                },
                shade: 0,
                btn: ['确定', '取消'],
                btnAlign: 'c',
                btn1: function (index, layero) {
                    var flag = false;
                    var approvalAmount = $("#approvalAmount").val();
                    if (approvalAmount != null && approvalAmount != "") {
                        if (!checkMoney(approvalAmount)) {
                            layer.msg("请输入有效资助金额，例如：2000");
                            return;
                        } else {
                            flag = true;
                        }
                    }
                    if (flag) {
                        layer.close(index);
                        var appNos = [];
                        $.each(checkStatus.data, function (i, val) {
                            appNos.push(val.appNo);
                        });
                        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
                        $.ajax({
                            url: "/support/proxyOrg/batchUpdateApprovalAmount.html",
                            type: "post",
                            data: {appNos: appNos.join(","), approvalAmount: approvalAmount},
                            success: function (res) {
                                if (res.code == 0) {
                                    setTimeout(function () {
                                        layer.close(index);
                                        layer.msg("修改成功");
                                        location.reload();
                                    }, 1000);
                                } else {
                                    setTimeout(function () {
                                        layer.msg(res.msg);
                                    }, 2000);
                                }
                            },
                        });
                    } else {
                        layer.msg("请输入资助金额");
                    }
                },
                btn2: function (index, layero) {
                    layer.close(index);
                },
                cancel: function (layero, index) {
                    layer.closeAll();
                }
            });
        } else {
            layer.msg("请先选择要修改的专利");
        }
    });

    // 批量驳回申请
    $(".batch-rejected").on("click", function () {
        var checkStatus = table.checkStatus('proxyOrgSupportList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var appNos = [];
            var appUserIds = [];
            $.each(checkStatus.data, function (i, val) {
                if (val.supportStatus == 1) {
                    appNos.push(val.appNo);
                    appUserIds.push(val.appUserId)
                } else {
                    layer.msg("请选择申请中的专利");
                    return;
                }
            });
            if (appNos.length > 0) {
                layer.confirm('确定批量驳回选中的专利授权资助申请', {icon: 3, title: '提示信息'}, function (index, row) {
                    $.post("/support/proxyOrg/batchRejected.html", {
                        appNos: appNos.join(','),
                        appUserIds: appUserIds.join(',')
                    }, function (data) {
                        if (data.code == 0) {
                            // reload();
                            location.reload();
                            layer.close(index);
                        } else {
                            layer.msg(data.msg);
                        }
                    })
                });
            }
        } else {
            layer.msg("请先选择要驳回申请的专利");
        }
    });

    // 批量视为放弃
    $(".batch-abandon").on("click", function () {
        var checkStatus = table.checkStatus('proxyOrgSupportList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var appNos = [];
            $.each(checkStatus.data, function (i, val) {
                if (val.supportStatus == 0) {
                    appNos.push(val.appNo);
                } else {
                    layer.msg("请选择未申请的专利");
                    return;
                }
            });
            if (appNos.length > 0) {
                layer.confirm('确定视为放弃选中的专利授权资助申请', {icon: 3, title: '提示信息'}, function (index, row) {
                    $.post("/support/proxyOrg/batchAbandon.html", {
                        appNos: appNos.join(',')
                    }, function (data) {
                        if (data.code == 0) {
                            // reload();
                            location.reload();
                            layer.close(index);
                        } else {
                            layer.msg(data.msg);
                        }
                    })
                });
            }
        } else {
            layer.msg("请先选择要视为放弃的专利");
        }
    });

    // 批量资助完成
    $(".batch-success").on("click", function () {
        var checkStatus = table.checkStatus('proxyOrgSupportList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var appNos = [];
            var appUserIds = [];
            $.each(checkStatus.data, function (i, val) {
                if (val.supportStatus == 2) {
                    appNos.push(val.appNo);
                    appUserIds.push(val.appUserId)
                } else {
                    layer.msg("请选择审批通过的专利");
                    return;
                }
            });
            if (appNos.length > 0) {
                layer.confirm('确定资助完成选中的专利授权资助申请', {icon: 3, title: '提示信息'}, function (index, row) {
                    $.post("/support/proxyOrg/batchSuccess.html", {
                        appNos: appNos.join(','),
                        appUserIds: appUserIds.join(',')
                    }, function (data) {
                        if (data.code == 0) {
                            // reload();
                            location.reload();
                            layer.close(index);
                        } else {
                            layer.msg(data.msg);
                        }
                    })
                });
            }
        } else {
            layer.msg("请先选择要资助完成的专利");
        }
    });

    // 批量删除
    $(".batch-delete").on("click", function () {
        var checkStatus = table.checkStatus('proxyOrgSupportList');
        var len = checkStatus.data.length;
        if (len > 0) {
            var appNos = [];
            $.each(checkStatus.data, function (i, val) {
                appNos.push(val.appNo);
            });
            layer.confirm('确定删除选中的专利吗', {icon: 3, title: '提示信息'}, function (index, row) {
                $.post("/support/proxyOrg/batchDelete.html", {
                    appNos: appNos.join(',')
                }, function (data) {
                    if (data.code == 0) {
                        // reload();
                        location.reload();
                        layer.close(index);
                    } else {
                        layer.msg(data.msg);
                    }
                })
            });
        } else {
            layer.msg("请先选择要删除的专利");
        }
    });

    // 点击资助状态弹窗
    $(document).on('click', '.update-support-status', function () {
        var appNo = $(this).attr("appNo");
        var supportStatus = $(this).attr("supportStatus");
        var idx = layui.layer.open({
            title: "修改资助状态",
            type: 1,
            area: ['600px', '450px'],
            content: $("#updateSupportStatusForm"),
            success: function (layero, index) {
                $("#supportStatus option").each(function () {
                    if ($(this).val() == supportStatus) {
                        $(this).attr("selected", true);
                    }
                });
                form.render();
            },
            shade: 0,
            btn: ['确定', '取消'],
            btnAlign: 'c',
            btn1: function (index, layero) {
                $.post("/support/proxyOrg/updateSupportStatus.html", {
                    appNo: appNo,
                    supportStatus: $("#supportStatus").val()
                }, function (data) {
                    if (data.code == 0) {
                        location.reload();
                    } else {
                        layer.msg(data.msg);
                    }
                })
            },
            btn2: function (index, layero) {
                layer.close(index);
            },
            cancel: function (layero, index) {
                layer.closeAll();
            }
        });
    });

    // 点击一键推送
    $(".push").on("click", function () {
        $.get("/support/proxyOrg/push.html", {}, function (data) {
            if (data.code == 0) {
                layer.msg("推送成功")
            } else {
                layer.msg(data.msg);
            }
        })

    });

    // 点击资助申请删除
    $(document).on('click', '.app-support-user', function () {
        var userId = $(this).attr("userId");
        var appNo = $(this).attr("appNo");
        layer.confirm('确定删除该资助申请人的申请记录吗', {icon: 3, title: '提示信息'}, function (index, row) {
            $.post("/support/proxyOrg/apply/delete.html", {
                userId: userId,
                appNo: appNo
            }, function (data) {
                if (data.code == 0) {
                    layer.close(index);
                    reload();
                } else {
                    layer.msg(data.msg);
                }
            })
        });
    });

    // 点击资助申请人
    $(document).on('click', '.app-user', function () {
        var userId = $(this).attr("userId");
        var certificationName = $(this).attr("certificationName");
        $.post("/certification/detailByUserId.html", {
            userId: userId
        }, function (res) {
            if (res.code == 0) {
                var data = res.data;
                layui.layer.open({
                    title: certificationName + "-实名认证详细信息",
                    type: 2,
                    area: ['100%', '100%'],
                    content: "/certification/detailPage.html",
                    success: function (layero, index) {
                        var body = layui.layer.getChildFrame('body', index);
                        var iframeWin = layero.find('iframe')[0].contentWindow;
                        body.find("#id").val(data.id);
                        var orgType = data.orgType;
                        if (orgType == 1) {
                            body.find("#orgType").html("公司企业");
                            body.find("#certificateFileTd").html("企业营业执照：");
                            body.find("#certificateImg").attr("src", data.certificatePath);
                            body.find("#openingPermitImg").attr("src", data.openingPermitPath);
                            body.find(".company").show();
                        } else if (orgType == 2) {
                            body.find("#orgType").html("事业单位");
                            body.find("#certificateFileTd").html("事业单位法人证书：");
                            body.find("#certificateImg").attr("src", data.certificatePath);
                            body.find("#openingPermitImg").attr("src", data.openingPermitPath);
                            body.find(".company").show();
                        } else if (orgType == 3) {
                            body.find("#orgType").html("机关团体");
                            body.find("#certificateFileTd").html("机关团体法人证书：");
                            body.find("#certificateImg").attr("src", data.certificatePath);
                            body.find("#openingPermitImg").attr("src", data.openingPermitPath);
                            body.find(".company").show();
                        } else if (orgType == 4) {
                            body.find("#orgType").html("个人");
                            body.find("#identityCardFrontImg").attr("src", data.identityCardFrontPath);
                            body.find("#identityCardBackImg").attr("src", data.identityCardBackPath);
                            body.find("#bankFrontImg").attr("src", data.bankFrontPath);
                            body.find("#bankBackImg").attr("src", data.bankBackPath);
                            body.find(".person").show();
                        } else if (orgType == 5) {
                            body.find("#orgType").html("个体工商户");
                            body.find("#identityCardFrontImg").attr("src", data.identityCardFrontPath);
                            body.find("#identityCardBackImg").attr("src", data.identityCardBackPath);
                            body.find("#bankFrontImg").attr("src", data.bankFrontPath);
                            body.find("#bankBackImg").attr("src", data.bankBackPath);
                            body.find(".person").show();
                        } else if (orgType == 6) {
                            body.find("#orgType").html("其他");
                            body.find("#attachmentImg").attr("src", data.attachmentPath);
                            body.find("#bankFrontImg").attr("src", data.bankFrontPath);
                            body.find("#bankBackImg").attr("src", data.bankBackPath);
                            body.find(".other").show();
                        }
                        body.find("#userName").html(data.userName);
                        body.find("#certificateNo").html(data.certificateNo);
                        body.find("#name").html(data.name);
                        body.find("#address").html(data.address);
                        body.find("#bank").html(data.bank);
                        //body.find("#accountHolder").val(data.accountHolder);
                        body.find("#accountNo").html(data.accountNo);
                        body.find("#contactPerson_name").html(data.contactPerson.name);
                        body.find("#contactPerson_phoneNo").html(data.contactPerson.phoneNo);
                        body.find("#contactPerson_email").html(data.contactPerson.email);
                        body.find("#contactPerson_address").html(data.contactPerson.address);
                        body.find("#mark").html(data.mark);
                        if (data.status == 1) {
                            body.find(".btn").show();
                        } else {
                            body.find("#mark").attr("readonly", "readonly");
                        }
                        setTimeout(function () {

                        }, 3000);
                    }
                });
            }
        })
    });

    //导出excel表格
    $(".getCheckData").on("click", function () { //获取选中数据
        var data = table.checkStatus('proxyOrgSupportList').data;
        var arrTableData = JSON.parse(JSON.stringify(data));
        var supportStatus = $('#status').val();
        var condition, index;
        if (supportStatus !== null && supportStatus !== '') {
            condition = {supportStatus: supportStatus}
        } else {
            condition = buildSearchCondition();
        }
        if (data.length === 0) {
            //为选择数据，导出全部
            $.ajax({
                url: "/support/proxyOrg/exportAll.html",
                type: "post",
                data: condition,
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
            return false;
        } else if (data.length !== 0) {
            var ids = '';
            for (var i = 0; i < arrTableData.length; i++) {
                ids += arrTableData[i].id + ",";
            }
            $.ajax({
                url: "/support/proxyOrg/excel.html",
                type: "post",
                data: {"ids": ids},
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
        }
    })
});