package com.lotut.system.shiro.domain;

import java.io.Serializable;

/**
 * 用户角色实体
 * @author Administrator
 */
public class SysUserRole implements Serializable {
    private static final long serialVersionUID = 3181949138629753826L;
    /**
     * 自增主键
     */
    private Long recordId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 角色id
     */
    private Long roleId;

    public SysUserRole() {
    }

    public SysUserRole(Long userId, Long roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "SysUserRole{" +
                "recordId=" + recordId +
                ", userId=" + userId +
                ", roleId=" + roleId +
                '}';
    }
}
