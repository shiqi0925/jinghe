layui.use(['form', 'layer', 'table', 'laytpl','jquery'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;

    //表格列表
    var tableIns = table.render({
        elem: '#tableList',
        url: '/admin/message/getPatentSaleList',
        method: 'post',
        cellMinWidth: 95,
        page: true,
        height: "full-125",
        limits: [20, 50, 100],
        limit: 20,
        id: "tableList",
        cols: [[
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'contacts', title: '查询人姓名', align: "center"},
            {field: 'phone', title: '联系电话', align: 'center'},
            {field: 'content', title: '查询内容', align: 'center'},
            {field: 'number', title: '注册号', align: 'center'},
            {field: 'type', title: '类型', align: 'center'},
            {field: 'createTime', title: '查询时间', align: 'center'},
            
            {field: 'isSee', title: '阅读状态', align: 'center', templet: function (d) {
                    if(d.isSee) {
                        return '<input type="checkbox" value="'+d.id+'" name="switch" lay-skin="switch" lay-filter="defaultSwitch" lay-text="已读|未读" checked>';
                    }
                    return '<input type="checkbox" value="'+d.id+'" name="switch" lay-skin="switch" lay-filter="defaultSwitch" lay-text="已读|未读">';
                }},
            {title: '操作', minWidth: 150, templet: '#listBar', fixed: "right", align: "center"}
        ]]
    });

    // 刷新表格
    function reload() {
        table.reload("tableList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $("#keyword").val()
            }
        })
    }

    //搜索
    $("#keyword").on("click", function () {
        reload();
    });

    $(window).keydown(function (e) {
        var key = window.event ? e.keyCode : e.which;
        if (key.toString() == "13") {
            return false;
        }
    });



 

    //列表操作
    table.on('tool(tableList)', function (obj) {
    	console.log(11)
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'edit') {
            editUser(data);
        } else if(layEvent === 'delete') {
        	layer.confirm('确定删除？', {icon: 3, title:'提示信息'}, function(index){
	                $.post("/admin/message/delete_patent_sale.html", {
	                    id: data.id
	                }, function (data) {
	                	console.log(data);
	                    if (data.code == 0) {
	                    	console.log("返回code  =0")
	                        reload();
	                    }
	                })
        		  
        		  layer.close(index);
        	});
        }
    });

    //监听是否默认
    form.on('switch(defaultSwitch)', function (data) {
        var checked = data.elem.checked;
        console.log("data:"+data);
        $.ajax({
            url: "/admin/message/update_see_patent_sale.html",
            type: "post",
            data: {id: data.value, status: checked},
            success: function (res) {
                // reload();
            }
        });
       
    });

});