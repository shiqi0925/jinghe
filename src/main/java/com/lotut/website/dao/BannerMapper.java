package com.lotut.website.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import com.lotut.website.domain.Banner;
import com.lotut.website.dto.BannerSearchCondition;


@Mapper
@Repository
public interface BannerMapper {
    int deleteByPrimaryKey(Long recordId);

    int insert(Banner record);

    Long insertSelective(Banner detail);

    Banner selectByPrimaryKey(@Param("id") Long id, @Param("websiteId") Long websiteId);

    int updateSelective(Banner detail);

    int update(Banner record);

    List<Banner> selectList(BannerSearchCondition condition);

    Long countBySearchCondition(BannerSearchCondition condition);

    List<Banner> listByPage(BannerSearchCondition condition);

    Banner selectById(Long id, Long websiteId);
    

    
}