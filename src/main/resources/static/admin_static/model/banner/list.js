  layui.config({
    base: '/admin_static/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'layer', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;

     
    $(document).on('click', '.delete', function () {
    	console.log($(this).attr('data-id'))
        $.ajax({
            url: "/admin/banner_delete.html",
            type: "post",
            data: {
            	id:$(this).attr('data-id')
            },
            success: function (res) {
                if (res.code == 0) {
                	top.layer.msg("删除成功！");
                	layer.open({
                		  content: '删除成功',
                		  yes: function(index, layero){
                			  location.reload();
                		  }
                		});  
                } else {
                	top.layer.msg("删除失败");
                }
            },
        });
    })

});