package com.lotut.news.dto;

import com.lotut.news.domain.WebsiteNews;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author lidc
 * @date 2020-03-16 16:52
 */
public class WebsiteNewsDetailsDto implements Serializable {
    private static final long serialVersionUID = -6427234009548430357L;

    private String parentChannelName;

    private String secondChannelName;

    private String title;

    private String publishTime;

    private String content;

    private List<Map<String,String>> attachments;

    private WebsiteNews beforeNews;

    private WebsiteNews nextNews;

    public String getParentChannelName() {
        return parentChannelName;
    }

    public void setParentChannelName(String parentChannelName) {
        this.parentChannelName = parentChannelName;
    }

    public String getSecondChannelName() {
        return secondChannelName;
    }

    public void setSecondChannelName(String secondChannelName) {
        this.secondChannelName = secondChannelName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Map<String, String>> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Map<String, String>> attachments) {
        this.attachments = attachments;
    }

    public WebsiteNews getBeforeNews() {
        return beforeNews;
    }

    public void setBeforeNews(WebsiteNews beforeNews) {
        this.beforeNews = beforeNews;
    }

    public WebsiteNews getNextNews() {
        return nextNews;
    }

    public void setNextNews(WebsiteNews nextNews) {
        this.nextNews = nextNews;
    }
}
