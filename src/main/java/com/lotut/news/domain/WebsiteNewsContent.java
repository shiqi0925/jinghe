package com.lotut.news.domain;

import java.io.Serializable;

/**
 * 用户网站新闻内容实体
 *
 * @author Administrator
 */
public class WebsiteNewsContent implements Serializable {
    private static final long serialVersionUID = 1977438642737680558L;
    /**
     * 新闻id
     */
    private Long newsId;
    /**
     * 内容
     */
    private String content;

    public WebsiteNewsContent(Long newsId, String content) {
        this.newsId = newsId;
        this.content = content;
    }

    public WebsiteNewsContent() {
        super();
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}