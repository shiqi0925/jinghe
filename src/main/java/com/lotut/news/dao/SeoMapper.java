package com.lotut.news.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import com.lotut.news.domain.Seo;


@Mapper
@Repository
public interface SeoMapper {

    List<Seo> selectList();

    Seo getById(long id);
    
    boolean save(Seo seo);
}