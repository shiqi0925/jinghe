package com.lotut.system.shiro.exception;

/**
 * 用户不存在异常类
 *
 * @author Wang JunJie
 * @date 2018/9/5 17:17.
 */
public class UserNotExistsException extends UserException {
    public UserNotExistsException() {
        super("user.not.exists", null);
    }
}
