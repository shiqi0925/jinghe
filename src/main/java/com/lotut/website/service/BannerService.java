package com.lotut.website.service;

import java.util.List;
import com.lotut.common.dto.Page;
import com.lotut.website.domain.Banner;
import com.lotut.website.dto.BannerDto;
import com.lotut.website.dto.BannerSearchCondition;

public interface BannerService {

    Page<Banner> getListByPage(BannerSearchCondition condition);

    List<Banner> selectList(BannerSearchCondition condition);

    Banner selectByPrimaryKey(Long id, Long websiteId);

    boolean save(BannerDto Dto);
    
 
}
