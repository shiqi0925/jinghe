layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    //保存
    $("#save").on('click', function () {
        $("#save-submit").click();
    });

    //监听提交-保存
    form.on('submit(save-submit)', function (data) {
        data.field.supportStatus = 1;
        $.ajax({
            url: "/enterpriseApp/saveApplicationForm.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('提交成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        closePage();
                    });
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    //临时保存
    $("#check-save").on('click', function () {
        $(":input").removeAttr("lay-verify");
        var textArea = document.getElementsByClassName("layui-textarea");
        for (var i = 0; i++; i < textArea.length) {
            textArea[i].removeAttr("lay-verify");
        }
        $("#check-save-submit").click();
    });

    //提交临时保存
    form.on('submit(check-save-submit)', function (data) {
        data.field.supportStatus = 5;
        $.ajax({
            url: "/enterpriseApp/saveApplicationForm.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('临时保存成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        closePage();
                    });
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    form.verify({
        // customNumber: [
        //     /^(([1-9]\d*)|0)(\.\d{0,2})?$/, "输入的数字不正确，请重新输入！<br>(且只能保留两位小数)"
        // ],
        number: function (value, item) {
            if (/^$/.test(value)) {
                return "必填项不能为空";
            }
            if (/^\s$/.test(value)) {
                return "不能含有空格！";
            }
            if (/^0\d+$/.test(value)) {
                return "第一个数不能为0！";
            }
            if (/^(([1-9]\d*)|0)(\.\d*)$/.test(value)) {
                return "不能输入小数！";
            }
            if (/^-(([1-9]\d*)|0)(\.\d*)?$/.test(value)) {
                return "不能输入负数！";
            }
            if (isNaN(value)) {
                return "请输入正确的数字！";
            }
        },
        phone: function (value, item) {
            if (/^$/.test(value)) {
                return "必填项不能为空";
            }
            if (!(/^1[3456789]\d{9}$/.test(value))) {
                return "请输入正确的手机号！";
            }
        },
    });

    function closePage() {
        var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
        parent.layui.table.reload('enterpriseAppList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
        parent.layer.close(index); //再执行关闭
    }
});