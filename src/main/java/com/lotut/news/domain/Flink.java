package com.lotut.news.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * seo
 *
 * @author Administrator
 */
public class Flink implements Serializable {
    private static final long serialVersionUID = 4959485506060450737L;
    /**
     * 自增主键
     */
    private Long id;

    private String title;
    /**
     *  
     */
    private String furl;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFurl() {
		return furl;
	}
	public void setFurl(String furl) {
		this.furl = furl;
	}
   
}