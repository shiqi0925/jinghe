package com.lotut.common.util;

import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 */
public class HttpServletUtil {
    public static boolean isAjax(HttpServletRequest req) {
        //判断是否为ajax请求，默认不是
        boolean isAjaxRequest = false;
        if (!StringUtils.isEmpty(req.getHeader("x-requested-with")) && req.getHeader("x-requested-with").equalsIgnoreCase("XMLHttpRequest")) {
            isAjaxRequest = true;
        }
        return isAjaxRequest;
    }
}
