package com.lotut.website.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lotut.website.dao.WebsiteMapper;
import com.lotut.website.domain.Banner;
import com.lotut.website.domain.Website;
import com.lotut.website.dto.BannerDto;
import com.lotut.website.dto.WebsiteDto;
import com.lotut.website.service.WebsiteService;

/**
 * @author lidc
 * @date 2020-03-12 13:20
 */
@Service
public class WebsiteServiceImpl implements WebsiteService {

    @Autowired
    private WebsiteMapper websiteMapper;

    @Override
    @Cacheable(cacheNames = "database_cache", key = "#domainUrl")
    public Website selectByDomainUrl(String domainUrl){
        return websiteMapper.selectByDomainUrl(domainUrl);
    }

    @Override
    public Website selectByUserId(Long userId) {
        return websiteMapper.selectByUserId(userId);
    }

	@Override
	public List<Website> selectWebsiteByUserId(Long userId) {
		// TODO Auto-generated method stub
		return websiteMapper.selectWebsiteByUserId(userId);
	}

	@Override
	public boolean save(WebsiteDto dto) {
        if(dto.getRecordId() != null) {
        	//websiteMapper.updateSelective(dto);  暂时不修改
        }else {
        	websiteMapper.insertSelective(dto);
        }
    	
        return true;
	}

	@Override
	public int selectWebsiteCountByUserId(Long userId) {
		
		int count = websiteMapper.selectWebsiteCountByUserId(userId);
		return count;
	}
 
}
