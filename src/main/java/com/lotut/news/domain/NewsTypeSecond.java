package com.lotut.news.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
 

/**
 * 用户网站新闻实体
 *
 * @author Administrator
 */
public class NewsTypeSecond implements Serializable {
    private static final long serialVersionUID = 4959485506060450737L;
    /**
     * 分类id
     */
    private Long secondTypeId;
    /**
     * 分类名
     */
    private String secondTypeName;
    
    private int secondStatus;
    /**
     * 父级id
     */
    private int secondParentType;
    /**
     * 说明
     */
    private String remarks;
	public Long getSecondTypeId() {
		return secondTypeId;
	}
	public void setSecondTypeId(Long secondTypeId) {
		this.secondTypeId = secondTypeId;
	}
	public String getSecondTypeName() {
		return secondTypeName;
	}
	public void setSecondTypeName(String secondTypeName) {
		this.secondTypeName = secondTypeName;
	}
 
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
    public String getTypeName(){

        return secondTypeName;
    }
    public Long getTypeId(){

        return secondTypeId;
    }
	public int getSecondStatus() {
		return secondStatus;
	}
	public int getStatus() {
		return secondStatus;
	}	
	public void setSecondStatus(int secondStatus) {
		this.secondStatus = secondStatus;
	}
	public int getSecondParentType() {
		return secondParentType;
	}
	public int getParentType() {
		return secondParentType;
	}
	public void setSecondParentType(int secondParentType) {
		this.secondParentType = secondParentType;
	}     
  

}