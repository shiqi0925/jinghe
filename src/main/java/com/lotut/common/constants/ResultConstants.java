package com.lotut.common.constants;
/**
 * 控制层操作成功后返回常量值提示
 * @copyright  
 * @author xieruifu
 * @create 2018-07-17
 */
public class ResultConstants {
	
	 public final static String SUCCESS = "操作成功";
	 public static final String FAILURE = "操作失败";
	 public static final String ERROR_USERNAME = "用户不存在";
	 public static final String ERROR_PASSWORD= "密码有误";
	 public static final String LOGIN_SUCCESS = "登录成功";
	
}
