package com.lotut.news.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.lotut.common.dto.Page;
import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.dto.WebsiteNewsSearchCondition;


@Mapper
@Repository
public interface WebsiteNewsMapper {
    int deleteByPrimaryKey(Long recordId);

    int insert(WebsiteNews record);

    Long insertSelective(WebsiteNews record);

    WebsiteNews selectByPrimaryKey(@Param("recordId") Long recordId);

    int updateByPrimaryKeySelective(WebsiteNews record);

    int updateByPrimaryKey(WebsiteNews record);

    List<WebsiteNews> selectList(WebsiteNewsSearchCondition condition);

    Long countBySearchCondition(WebsiteNewsSearchCondition condition);

    List<WebsiteNews> listByPage(@Param("condition") WebsiteNewsSearchCondition condition);

    @Select("select content from user_website_news_content where news_id = #{newsId}")
    String getNewsContent(Long newsId);

    @Select("select name,path from user_website_news_attachment where news_id = #{newsId}")
    List<Map<String,String>> getNewsAttachments(Long newsId);
    /**
     * 获取当前用户新闻总数
     * @param condition
     * @return
     */
    long getUserNewsCount(WebsiteNewsSearchCondition condition);
    /**
     * 分页显示当前用户新闻
     * @param condition
     * @return
     */
    List<WebsiteNews> getUserNews(WebsiteNewsSearchCondition condition);
    /**
     * 获取新闻详情
     * @param websiteId
     * @param newsId
     * @return
     */
    WebsiteNews getNewsById(@Param("newsId") Long newsId);
    /**
     * 更改新闻显示状态status
     * 0：隐藏，1：显示
     *
     * @param recordId
     * @param status
     * @return
     */
    int updateStatus(@Param("recordId") Long recordId,@Param("status") Boolean status);
    
    Page<WebsiteNews> getNewsListByParentId(@Param("parentId") long parentId,@Param("pageNo") int pageNo);
}