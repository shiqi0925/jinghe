layui.use(['form', 'layer', 'table', 'laytpl', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate;

    if ($("#role").val() === "sys_platform_admin") {
        var tableIns = table.render({
            elem: '#petitionLetterList',
            url: '/petitionLetter/getListByPage.html',
            method: 'get',
            cellMinWidth: 95,
            page: true,
            height: "full-85",
            limits: [20, 30, 40],
            limit: 10,
            id: "petitionLetterList",
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'name', title: '姓名', align: "center"},
                {field: 'title', title: '信访标题', align: "center"},
                // {field: 'recordId', title: '序号', align: 'center'},

                /*                {field: 'idNumber', title: '身份证号', align: 'center'},
                                {field: 'email', title: '邮箱', align: 'center'},*/
                {field: 'phone', title: '联系方式', width: 160, align: 'center'},
                /*                {field: 'address', title: '联系地址', align: 'center'},
                                {field: 'postcode', title: '邮编', align: 'center'},
                                {field: 'content', title: '内容', align: 'center'},*/
                /*                {
                                    field: 'readStatus', title: '读取状态', align: 'center',
                                    templet: function (d) {
                                        if (d.readStatus === 0) {
                                            return "未读";
                                        }
                                        if (d.readStatus === 1) {
                                            return "已读";
                                        }
                                    }
                                },
                                {field: 'petitionTime', title: '信访时间', align: 'center'},*/
                {field: 'petitionTime', title: '信访时间', width: 180, align: 'center'},
                {
                    field: 'caozuo',
                    title: '操作',
                    width: 170,
                    templet: '#petitionLetterListBar',
                    fixed: "right",
                    align: "center"
                }
            ]]
        });
    } else {
        var tableIns = table.render({
            elem: '#petitionLetterList',
            url: '/petitionLetter/getListByPage.html',
            method: 'get',
            cellMinWidth: 95,
            page: true,
            height: "full-85",
            limits: [20, 30, 40],
            limit: 10,
            id: "petitionLetterList",
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                // {field: 'recordId', title: '序号', align: 'center'},
                {field: 'name', title: '姓名', align: "center"},
                {field: 'title', title: '信访标题', align: "center"},
                /*                {field: 'idNumber', title: '身份证号', align: 'center'},
                                {field: 'email', title: '邮箱', align: 'center'},*/
                {field: 'phone', title: '联系方式', align: 'center'},
                /*                {field: 'address', title: '联系地址', align: 'center'},
                                {field: 'postcode', title: '邮编', align: 'center'},
                                {field: 'content', title: '内容', align: 'center'},*/
                {field: 'petitionTime', title: '信访时间', align: 'center'},
                {
                    field: 'caozuo',
                    title: '操作',
                    minWidth: 160,
                    templet: '#petitionLetterListBar2',
                    fixed: "right",
                    align: "center"
                }
            ]]
        });
    }

    // 刷新表格
    function reload() {
        table.reload("petitionLetterList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $("#keyword").val(),
                startTime: $("#startTime").val(),
                endTime: $("#endTime").val()
            }
        })
    }

    // 刷新当前页
    function reloadCurrentPage(currentPage) {
        table.reload("petitionLetterList", {
            page: {
                curr: currentPage
            },
            where: {
                keyword: $("#keyword").val(),
                startTime: $("#startTime").val(),
                endTime: $("#endTime").val()
            }
        })
    }

    // //搜索
    $("#search").on("click", function () {
        reload();
    });

    $(window).keydown(function (e) {
        var key = window.event ? e.keyCode : e.which;
        if (key.toString() === "13") {
            return false;
        }
    });

    //查看操作，管理员改为已读
    table.on('tool(petitionLetterList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        //置为已读
        if (layEvent === 'check') {
            $.post("/petitionLetter/updatePetitionLetterStatusByRecordId.html", {
                    id: data.recordId
                }, function (data) {
                    if (data.code === 0) {
                        reloadCurrentPage($(".layui-laypage-em").next().html());
                    }
                }
            );
        }
        if (layEvent === 'show') {
            var index = layui.layer.open({
                title: "查看详情",
                type: 2,
                area: ['100%', '100%'],
                content: '/petitionLetter/online/showPage.html?id=' + data.recordId,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);

                }
            });
        }
    });

    //时间
    var start = laydate.render({
        elem: '#startTime', //指定元素
        type: 'datetime',
        // range:true
    });
    var end = laydate.render({
        elem: '#endTime', //指定元素
        type: 'datetime'
    });

    $.each($("textarea"), function (i, n) {
        $(n).css("height", n.scrollHeight + "px");
    });

    //导出excel表格
    $(".getCheckData").on("click", function () { //获取选中数据
        var data = table.checkStatus('petitionLetterList').data;
        var index;
        if (data.length === 0) {
            //为选择数据，导出全部
            $.ajax({
                url: "/petitionLetter/exportAll.html",
                type: "post",
                data: {keyword: $("#keyword").val()},
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
            return false;
        } else if (data.length !== 0) {
            var arrTableData = JSON.parse(JSON.stringify(data));
            var ids = '';
            for (var i = 0; i < arrTableData.length; i++) {
                ids += arrTableData[i].recordId + ",";
            }
            $.ajax({
                url: "/petitionLetter/excel.html",
                type: "post",
                data: {"ids": ids},
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60 * 60 * 1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
        }
    })
});