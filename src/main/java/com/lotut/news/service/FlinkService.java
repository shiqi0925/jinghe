package com.lotut.news.service;

import java.util.List;

import com.lotut.news.domain.Flink;
import com.lotut.news.domain.Seo;


public interface FlinkService {
  
    /**
     * 查询列表
     * @param condition
     * @return
     */
    List<Flink> selectList();

   
    Flink getById(Long id);
    
    boolean save(Flink flink);
}
