package com.lotut.news.controller;

import com.lotut.common.util.EmptyUtil;
import com.lotut.common.util.FileUtil;
import com.lotut.system.shiro.util.ShiroUtil;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * ueditor控制层
 *
 * @author xwj
 * @date 2020/3/12
 */
@Controller
@RequestMapping("/ueditor")
public class UEditorNewsController {
    /**
     * ueditor配置
     */
    @Value("classpath:ueditor/config.json")
    private Resource config;
    /**
     * ueditor图片上传路径配置
     */
    @Value("${news.img.path}")
    private String imgPath;
    /**
     * ueditor文件上传路径配置
     */
    @Value("${news.file.path}")
    private String filePath;

    /**
     * 读取ueditor配置
     *
     * @param action
     * @return
     * @throws IOException
     */
    @GetMapping("/config")
    @ResponseBody
    public String editorConfig(String action) throws IOException {
        return IOUtils.toString(config.getInputStream(), Charset.forName("UTF-8"));
    }

    /**
     * ueditor上传图片
     *
     * @param img
     * @return
     */
    @PostMapping("/uploadImg")
    @ResponseBody
    public Map<String, Object> uploadImg(@RequestParam("img") MultipartFile img) {
        Map<String, Object> result = new HashMap<>(4);
        String path = FileUtil.nioUpload(img, buildUEditorFilePath(imgPath, ShiroUtil.getUserId()));
        if (EmptyUtil.isNotEmpty(path)) {
            result.put("state", "SUCCESS");
            result.put("url", path.replaceFirst(imgPath, "/news/img/"));
        } else {
            result.put("state", "ERROR");
            result.put("url", null);
        }
        return result;
    }

    /**
     * ueditor上传文件
     *
     * @param file
     * @return
     */
    @PostMapping("/uploadFile")
    @ResponseBody
    public Map<String, Object> uploadFile(@RequestParam("file") MultipartFile file) {
        Map<String, Object> result = new HashMap<>(4);
        String path = FileUtil.nioUpload(file, buildUEditorFilePath(filePath, ShiroUtil.getUserId()));
        if (EmptyUtil.isNotEmpty(path)) {
            result.put("state", "SUCCESS");
            result.put("url", path.replaceFirst(filePath, "/news/files/"));
        } else {
            result.put("state", "ERROR");
            result.put("url", null);
        }
        return result;
    }

    /**
     * 构造UEditor文件上传路径
     *
     * @param userId
     * @return
     */
    private String buildUEditorFilePath(String bashPath, Long userId) {
        StringBuilder sb = new StringBuilder(bashPath);
        LocalDateTime now = LocalDateTime.now();
        sb.append(userId).append("/").append(now.getYear()).append("/")
                .append(now.getMonthValue()).append("/").append(now.getDayOfMonth()).append("/");
        return sb.toString();
    }

}
