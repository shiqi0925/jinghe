package com.lotut.news.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

 

/**
 * 用户网站新闻实体
 *
 * @author Administrator
 */
public class NewsType implements Serializable {
    private static final long serialVersionUID = 4959485506060450737L;
    /**
     * 自增主键
     */
    private Long typeId;

    /**
     * 分类名字
     */
    private String typeName;
    /**
     * 一级频道id
     */
    private Long parentType;
    /**
     * 状态
     */
    private String status;
    
    private List<NewsTypeSecond> children;
    
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public Long getParentType() {
		return parentType;
	}
	public void setParentType(Long parentType) {
		this.parentType = parentType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<NewsTypeSecond> getChildren() {
		return children;
	}
	public void setChildren(List<NewsTypeSecond> children) {
		this.children = children;
	}
   

}