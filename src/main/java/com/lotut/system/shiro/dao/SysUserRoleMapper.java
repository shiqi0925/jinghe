package com.lotut.system.shiro.dao;

import com.lotut.system.shiro.domain.SysUserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysUserRoleMapper {
    @Insert("insert into sys_user_role(user_id,role_id) values(#{userId},#{roleId})")
    int save(SysUserRole sysUserRole);

    int batchSave(List<SysUserRole> list);

    @Delete("delete from sys_user_role where user_id=#{userId}")
    int removeByUserId(Long userId);

    @Delete("delete from sys_user_role where role_id=#{roleId}")
    int removeByRoleId(Long roleId);
}
