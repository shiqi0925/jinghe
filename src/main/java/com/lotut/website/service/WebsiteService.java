package com.lotut.website.service;

import java.util.List;

import com.lotut.system.shiro.util.ShiroUtil;
import com.lotut.website.domain.Website;
import com.lotut.website.dto.WebsiteDto;

/**
 * @author lidc
 * @date 2020-03-12 13:20
 */
public interface WebsiteService {

    Website selectByDomainUrl(String domainUrl);

    Website selectByUserId(Long userId);
    
    List<Website> selectWebsiteByUserId(Long userId);
    
    boolean save(WebsiteDto dto);
    
    int selectWebsiteCountByUserId(Long userId);
}
