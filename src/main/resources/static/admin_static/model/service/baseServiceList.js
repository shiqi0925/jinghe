  layui.config({
    base: '/admin/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['form','table'], function () {
	  var table=layui.table,$=layui.$,form = layui.form;
	  
	    table.render({
	        elem: '#tableList',
	        url: '/admin/baseServiceList',
	        method: 'post',
            where: {
                keyword: $("#keyword").val(),
                parentId:$(".parentId").val(),
                typeId:$(".typeId").val() 
            },
	        cellMinWidth: 95,id: "tableList",page: true,
	        limit: 20,height: 'full-195',limits: [20,50,100],skin: 'line',
	        done:function (res, curr, count) {
	        	console.log(res)
	        },
	        cols: [[
	            {align: 'center', type: 'numbers', title: '序号'},
	            {field: 'serviceName', title: '服务名称', align: "center"},
	            {field: 'currentTypeName', title: '服务分类', align: "center",templet: function(d){
	                return d.firstTypeName+'&nbsp;>&nbsp;'+d.secondTypeName
	            }
	            },
	            {field: 'isRecommend', title: '推荐状态', align: "center"},
	            {field: 'content', title: '说明', align: "center"},
	            {width: 160, fixed: 'right',align: 'center', toolbar: '#option', title: '操作'}
	        ]]
	    });  
	    //列表操作
	    table.on('tool(tableList)', function (obj) {
	        var layEvent = obj.event,
	            data = obj.data;
	        if (layEvent === 'edit') {
	            editData(data);
	        } else if(layEvent === 'delete') {
	            layer.confirm('确定删除该服务吗', {icon: 3, title: '提示信息'}, function (index, row) {
	                $.post("/frequentProxy/delete", {
	                    recordId: data.recordId
	                }, function (data) {
	                    if (data.code == 0) {
	                        reload();
	                        layer.close(index);
	                    }
	                })
	            });
	        }
	    });
	    function editData(edit) {
	    	var serviceId = edit.serviceId;
	        var index = layui.layer.open({
	            title: "编辑服务",
	            type: 2,
	            content: "/admin/base_service_edit?serviceId="+serviceId,
	            success: function (layero, index) {
	                setTimeout(function () {
	                    layui.layer.tips('点击此处返回新闻列表', '.layui-layer-setwin .layui-layer-close', {
	                        tips: 3
	                    });
	                }, 500)
	            }
	        });
	        layui.layer.full(index);
	    }
	    // 刷新表格
	    function reload() {
	        table.reload("tableList", {
	            page: {
	                curr: 1
	            },
	            where: {
	                keyword: $("#keyword").val(),
	                parentId:$(".parentId").val(),
	                typeId:$(".typeId").val() 
	            }
	        })
	    }

	    //搜索
	    $(".search_btn").on("click", function () {
	        reload();
	    });	 
	    // 监听一级分类选择
	    form.on('select(parentId)', function (data) {
	        var value = data.value;
	        $.ajax({
	            url: "/admin/servicetype",
	            type: "post",
	            data: {parentType: value},
	            success: function (res) {
	                var data = res.data;
	                if(data.length>0){
	                	$('#typeId').empty();
	                	$('#typeId').append("<option value=''>二级分类</option>");
	                    for (var len = data.length, i = 0; i < len; i++) {
	                        $('#typeId').append("<option value='" + data[i].typeId + "'>" + data[i].typeName + "</option>")
	                    }
	                    form.render();
	                }
	            }
	        });
	    })	    
  })