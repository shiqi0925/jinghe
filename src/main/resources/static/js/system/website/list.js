layui.use(['layer', 'form', 'upload'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        upload = layui.upload;

    layer.ready(function () {
        getWebsiteByUserId();
    });

    var src = '';

    function getWebsiteByUserId() {
        $.ajax({
            type: 'get',
            url: "/website/getWebsiteByUserId.html",
            success: function (res) {
                var website = res.data;
                src = website.topImagePath;
                if (src!==null||src!==''){
                    $("#imgSpan").show();
                }
                //给表单赋值
                form.val("form", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    // "username": website.username,
                    // "domainUrl": website.domainUrl,
                    // "backgroundPath": website.backgroundPath,
                    "topImagePath": website.topImagePath,
                    "topImageUrl": website.topImageUrl
                    // ,"esIndex": website.esIndex,
                    // "esType": website.esType
                });
            }
        });
    }

    //监听提交
    // $(".edit").on('click', function (data) {
    //     // $(".edit").hide();
    //     // $(".save").show();
    //     $(".cancel").show();
    //     // $("#backgroundPath").attr("readOnly", false);
    //     // $("#topImagePath").attr("readOnly", false);
    //     $("#topImageUrl").attr("readOnly", false);
    //     $("#uploadBackgroundImg").attr("style", "display:block;");
    //     $("#uploadTopImage").show();
    //     return false;
    // });

    form.on('submit(save)', function (data) {
        $.ajax({
            url: "/website/updateWebsiteByUserId.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    // setTimeout(function () {
                    //     top.layer.close(index);
                    //     top.layer.msg("保存成功！");
                    // }, 1500);

                    layer.msg('保存成功！', {icon: 1, time: 1000, shade: 0.4}, function () {
                        window.location.reload();
                    });
                }
            },
        });
        return false;
    });
    // $(".cancel").on('click', function (data) {
    //     window.location.reload();
    //     return false;
    // });

    //普通图片上传
    //背景图片
    // var uploadInst1 = upload.render({
    //     elem: '#uploadBackgroundImg',
    //     field: 'img',
    //     size: 1048576,
    //     accept: 'images',
    //     url: '/website/uploadImg.html',
    //     before: function (obj) {
    //         //预读本地文件示例，不支持ie8
    //         obj.preview(function (index, file, result) {
    //             $('#backgroundImg').attr('src', result); //图片链接（base64）
    //         })
    //     },
    //     done: function (res) {
    //         $('#backgroundPath').val(res.data);
    //     },
    //     error: function () {
    //         //演示失败状态，并实现重传
    //         var demoText = $('#demoText1');
    //         demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
    //         demoText.find('.demo-reload').on('click', function () {
    //             uploadInst.upload();
    //         });
    //     }
    // });

    //置顶图片
    var uploadInst2 = upload.render({
        elem: '#uploadTopImage',
        field: 'img',
        size: 1048576,
        accept: 'images',
        url: '/website/uploadImg.html',
        before: function (obj) {
            //预读本地文件示例，不支持ie8
            obj.preview(function (index, file, result) {
                src = result;
                $('#topImage').attr('src', result); //图片链接（base64）
                // $("#imgSpan").show();
            })
        },
        done: function (res) {
            $('#topImagePath').val(res.data);
        },
        error: function () {
            //演示失败状态，并实现重传
            var demoText = $('#demoText2');
            demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
            demoText.find('.demo-reload').on('click', function () {
                uploadInst.upload();
            });
        }
    });

    //返回
    $("#cancel_btn").on('click', function () {
        $(".layui-this i ", parent.document).click();
    })

    $("#topImage").on('click', function () {
        // alert(document.getElementById("topImage").src)
        // layer.msg( $("#topImage2") , {
        //     title: "详情",
        //     skin: 'layui-layer-nobg', //没有背景色
        //     area: ['1000px', '143px'],
        //     btn: ['返回']
        // });
        var imgHtml = "<img src='" + src + "' width='1000px' height='143px'/>";
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            area: ['1000px', '143px'],
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            content: imgHtml
        });
    })
});