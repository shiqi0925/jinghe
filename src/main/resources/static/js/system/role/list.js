layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    // 标签表格
    table.render({
        elem: '#roleList',
        url: '/sys/role/list',
        method: 'post',
        height: "full-125",
        id: "roleList",
        even: true,
        cols: [[
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'roleName', title: '角色名称', align: "center"},
            {field: 'roleDesc', title: '角色描述', align: "center"},
            {field: 'remark', title: '备注', align: "center"},
            {field: 'createTime', title: '创建时间', align: "center"},
            {field: 'updateTime', title: '更新时间', align: "center"},
            {title: '操作', minWidth: 240, templet: '#roleListBar', fixed: "right", align: "center"}
        ]]
    });

    //添加
    function addRole() {
        var index = layui.layer.open({
            title: "添加角色",
            type: 2,
            content: "/sys/role/add_page",
            success: function (data) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        });
        layui.layer.full(index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize", function () {
            layui.layer.full(index);
        })
    }
    $(".add_btn").click(function () {
        addRole();
    });

    // 角色编辑
    function updateRole(data) {
        var index = layui.layer.open({
            title: "角色编辑",
            type: 2,
            content: "/sys/role/update_page",
            success: function (layero, index) {
                var body = layui.layer.getChildFrame('body', index);
                body.find("#recordId").val(data.recordId);
                body.find("#roleName").val(data.roleName);
                body.find("#roleDesc").val(data.roleDesc);
                body.find("#remark").val(data.remark);
                form.render();
                setTimeout(function () {
                    layui.layer.tips('点击此处返回角色列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500);
            }
        });
        layui.layer.full(index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize", function () {
            layui.layer.full(index);
        })
    }

    //列表操作
    table.on('tool(roleList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'update') {
            updateRole(data);
        }
        if (layEvent === 'delete') {
            layer.confirm('确定删除该角色吗', {icon: 3, title: '提示信息'}, function (index, row) {
                $.get("/sys/role/delete", {
                    roleId: data.recordId
                }, function (data) {
                    if (data.code == 0) {
                        location.reload();
                        layer.close(index);
                    }
                })
            });
        }
    });

});