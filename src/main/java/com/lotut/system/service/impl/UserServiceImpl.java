package com.lotut.system.service.impl;

import com.lotut.common.dto.ReturnResult;
import com.lotut.system.dao.UserMapper;
import com.lotut.system.domain.User;
import com.lotut.system.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * @author xwj
 * @date 2020/3/4
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public ReturnResult login(String username, String password) {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, false);
        Subject subject = SecurityUtils.getSubject();
        //这里登录没有用上  站点id
        try {
            subject.login(token);
            return ReturnResult.successResult("登录成功");
        } catch (AuthenticationException e) {
        	System.out.print("2222222222222");
            String msg = "用户或密码错误";
            if (!StringUtils.isEmpty(e.getMessage())) {
            	System.out.print("33333333333333333");
            	System.out.print(e.getMessage());
                msg = e.getMessage();
            }
            return ReturnResult.errorResult(msg);
        }
    }
    @Override
    public ReturnResult websiteLogin(String username, String password, String sources) {
    	//所以用户可以登录www.zging.com，其他的只能前来源登录
    	if(sources != "www.zging.com") {
        	User user = findByUsernameSource(username, sources);
        	if(user== null) {
        		System.out.print("CCCCCCCCCCCCCCCCCCCCCC用户不存在");
        		return ReturnResult.errorResult("用户名不存在");
        	}	
    	}
    	String url = "";
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, false);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            if(sources == "www.zging.com") {
            	url = "/admin/index";
            }else {
            	url = "/admin/site.html";
            }
            return ReturnResult.successResult(url);
        } catch (AuthenticationException e) {
            String msg = "密码错误";
            if (!StringUtils.isEmpty(e.getMessage())) {
            	System.out.print("33333333333333333");
            	System.out.print(e.getMessage());
                msg = e.getMessage();
            }
            return ReturnResult.errorResult(msg);
        }
    }
    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }
    @Override
    public User findByUsernameSource(String username,String sources) {
        return userMapper.findByUsernameSource(username,sources);
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int save(User user) {
        return userMapper.save(user);
    }
}
