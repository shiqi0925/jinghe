package com.lotut.news.service.impl;

import com.lotut.common.dto.Page;
import com.lotut.common.util.EmptyUtil;
import com.lotut.news.dao.WebsiteNewsAttachmentMapper;
import com.lotut.news.dao.WebsiteNewsContentMapper;
import com.lotut.news.dao.WebsiteNewsMapper;
import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.domain.WebsiteNewsAttachment;
import com.lotut.news.domain.WebsiteNewsContent;
import com.lotut.news.dto.WebsiteNewsDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;
import com.lotut.news.service.WebsiteNewsService;
import com.lotut.system.shiro.util.ShiroUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xwj
 * @date 2020/3/11
 */
@Service
public class WebsiteNewsServiceImpl implements WebsiteNewsService {

    @Autowired
    private WebsiteNewsMapper websiteNewsMapper;
    @Autowired
    private WebsiteNewsContentMapper websiteNewsContentMapper;
    @Autowired
    private WebsiteNewsAttachmentMapper websiteNewsAttachmentMapper;

    @Override
    public Page<WebsiteNews> getNewsListByPage(WebsiteNewsSearchCondition condition) {
        Page<WebsiteNews> page = new Page<>(condition.getPage(), condition.getLimit());
        Long count = websiteNewsMapper.countBySearchCondition(condition);
        List<WebsiteNews> list = websiteNewsMapper.listByPage(condition);
        page.setTotalCount(count.intValue());
        page.setList(list);
        return page;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(WebsiteNewsDto newsDto) {
        // 保存新闻
        WebsiteNews news = new WebsiteNews();
        BeanUtils.copyProperties(newsDto, news);
/*        news.setCreateUser(ShiroUtil.getUserId());
        news.setUpdateUser(ShiroUtil.getUserId());*/
        /*if(news.getStatus()) {news.setPublishTime(new Date());}*/
        if(news.getRecordId() != null) {
        	websiteNewsMapper.updateByPrimaryKeySelective(news);
        	WebsiteNewsContent content = new WebsiteNewsContent(news.getRecordId(), newsDto.getContent());
        	websiteNewsContentMapper.updateByPrimaryKeySelective(content);
        }else {
        	System.out.print("777777777777777777777777777777777");
        	websiteNewsMapper.insertSelective(news);
            // 保存新闻内容
            WebsiteNewsContent content = new WebsiteNewsContent(news.getRecordId(), newsDto.getContent());
            websiteNewsContentMapper.insertSelective(content);
        }
        // 保存新闻附件
        if(EmptyUtil.isNotEmpty(newsDto.getAttachmentName()) && EmptyUtil.isNotEmpty(newsDto.getAttachmentPath())) {
            WebsiteNewsAttachment attachment = new WebsiteNewsAttachment(news.getRecordId(), newsDto.getAttachmentName(),
                    newsDto.getAttachmentPath());
            websiteNewsAttachmentMapper.insertSelective(attachment);
        }
        return true;
    }

    @Override
    public List<WebsiteNews> selectList(WebsiteNewsSearchCondition condition){
        return websiteNewsMapper.selectList(condition);
    }

 
    @Override
    public WebsiteNews selectByPrimaryKey(Long recordId){
        return websiteNewsMapper.selectByPrimaryKey(recordId);
    }
    @Override
    public String getNewsContent(Long newsId){
        return websiteNewsMapper.getNewsContent(newsId);
    }

    @Override
    public List<Map<String,String>> getNewsAttachments(Long newsId){
        return websiteNewsMapper.getNewsAttachments(newsId);
    }

    @Override
    public WebsiteNews getBeforeOrNextNews(WebsiteNewsSearchCondition condition){
        List<WebsiteNews> list = websiteNewsMapper.listByPage(condition);
        if(!list.isEmpty()) return list.get(0);
        return null;
    }

	@Override
	public Page<WebsiteNews> getUserNewsListByPage(WebsiteNewsSearchCondition condition) {
		Page<WebsiteNews> page = new Page<>(condition.getPage(), 9);
        Long count = websiteNewsMapper.getUserNewsCount(condition);
        condition.setLimit(9);
        List<WebsiteNews> list = websiteNewsMapper.getUserNews(condition);
        page.setTotalCount(count.intValue());
        page.setList(list);
        return page;
	}

	@Override
	public WebsiteNews getNewsById(Long newsId) {
		return websiteNewsMapper.getNewsById(newsId);
	}
    /**
     * 更改新闻显示状态status
     * 0：隐藏，1：显示
     *
     * @param recordId
     * @param status
     * @return
     */
    @Override
    public int updateStatus(Long recordId, Boolean status) {
        return websiteNewsMapper.updateStatus(recordId, status);
    }	
    
    /**
     * 查询一级栏目下的数据
     * @param condition
     * @return
     */
    @Override
    public Page<WebsiteNews> getNewsListByParentId(long parentId,int pageNo){
    	return websiteNewsMapper.getNewsListByParentId(parentId, pageNo);
    	
    }
    
}
