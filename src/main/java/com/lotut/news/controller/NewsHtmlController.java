package com.lotut.news.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lotut.common.constants.ResultConstants;
import com.lotut.common.constants.WebsiteConstants;
import com.lotut.common.dto.Page;
import com.lotut.common.dto.ReturnResult;
import com.lotut.common.util.BrowserSetUtil;
import com.lotut.common.util.FileUtil;
import com.lotut.website.domain.Website;
import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.dto.WebsiteNewsDetailsDto;
import com.lotut.news.dto.WebsiteNewsDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;
import com.lotut.news.service.WebsiteNewsChannelService;
import com.lotut.news.service.WebsiteNewsService;
import com.lotut.website.service.WebsiteService;
import com.lotut.system.shiro.util.ShiroUtil;

/**
 * 用户网站新闻控制层
 *
 * @author xwj
 * @date 2020/3/11
 */
@RequestMapping("/news")
@Controller
public class NewsHtmlController {
/*    private String prefix = "admin/news/";*/
    @Autowired
    private WebsiteNewsService websiteNewsService;
    @Autowired
    private WebsiteNewsChannelService websiteNewsChannelService;
    @Resource
    private WebsiteService websiteService;
    /**
     * 文件存储路径配置
     */
    @Value("${news.file.path}")
    private String fileDir;

    @GetMapping("/detail.html")
    public String update(Long id,Model model,HttpServletRequest request) {
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
    	String templateFileName = website.getTemplateFileName();
        if(website != null){
            Long websiteId = website.getRecordId();
            WebsiteNews news = websiteNewsService.selectByPrimaryKey(id);
            model.addAttribute("detail",news); 
            return "style/"+templateFileName+"/html/news/news_detail";
        }
        return "error/404";
    }
    @GetMapping("/")
    public String list(Long id,Model model,HttpServletRequest request) {
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
    	String templateFileName = website.getTemplateFileName();
        if(website != null){
            Long websiteId = website.getRecordId();
            WebsiteNews news = websiteNewsService.selectByPrimaryKey(id);
            model.addAttribute("detail",news); 
        }
    	return "style/"+templateFileName+"/html/news/news_list";
    } 
    
    /**
     * 跳转到新闻列表页面
     *
     * @return
     */
   /* @RequiresRoles("sys_news_admin")*/
/*    @GetMapping("/list")
    public String list() {
        return prefix + "list";
    }
*/

    /**
     * 下载新闻附件
     * @param fileName
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/files/{userId}/{year}/{month}/{day}/{fileName}")
    public void downloadFile(@PathVariable String userId, @PathVariable String fileName,@PathVariable String year,
                             @PathVariable String month,@PathVariable String day,
                             HttpServletRequest request, HttpServletResponse response) throws IOException {
        BrowserSetUtil.browserDownload(fileName,request,response);
        StringBuilder sb = new StringBuilder(fileDir);
        sb.append(userId).append("/").append(year).append("/").append(month).
                append("/").append(day).append("/").append(fileName);
        FileUtil.download(sb.toString(),response);
    }
    

 
}
