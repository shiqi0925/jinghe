package com.lotut.news.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lotut.common.constants.ResultConstants;
import com.lotut.common.dto.Page;
import com.lotut.common.dto.ReturnResult;
import com.lotut.common.util.BrowserSetUtil;
import com.lotut.common.util.FileUtil;
import com.lotut.website.domain.Website;
import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.dto.WebsiteNewsDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;
import com.lotut.news.service.WebsiteNewsChannelService;
import com.lotut.news.service.WebsiteNewsService;
import com.lotut.website.service.WebsiteService;
import com.lotut.system.shiro.util.ShiroUtil;

/**
 * 用户网站新闻控制层
 *
 * @author xwj
 * @date 2020/3/11
 */
@RequestMapping("/admin/news")
@Controller
public class WebsiteNewsController {
    private String prefix = "admin/news/";
    @Autowired
    private WebsiteNewsService websiteNewsService;
    @Autowired
    private WebsiteNewsChannelService websiteNewsChannelService;
    @Autowired
    private WebsiteService websiteService;

    /**
     * 文件存储路径配置
     */
    @Value("${news.file.path}")
    private String fileDir;

    /**
     * 跳转到新闻列表页面
     *
     * @return
     */
   /* @RequiresRoles("sys_news_admin")*/
    @GetMapping("/list")
    public String list(String type,Model model) {
    	model.addAttribute("type", type);
        return prefix + "list";
    }

    /**
     * 分页查询新闻列表
     */
   /* @RequiresRoles("sys_news_admin")*/
    @PostMapping(value = "/list")
    @ResponseBody
    public ReturnResult getNewsListByPage(WebsiteNewsSearchCondition condition) {
    	
        Page<WebsiteNews> page = websiteNewsService.getUserNewsListByPage(condition);
        return ReturnResult.successResultByPage(page);
    }



    /**
     * 跳转到新闻列表添加页面
     *
     * @return
     */
    /*@RequiresRoles("sys_news_admin")*/
    @GetMapping("/add/page")
    public String addPage(String type,Model model) {
    	model.addAttribute("type", type);
    	//Website website = websiteService.selectByUserId(ShiroUtil.getUserId());
        //List<WebsiteNewsChannel> parentChannelList = websiteNewsChannelService.getParentChannel(website.getRecordId());
        //model.addAttribute("parentChannelList", parentChannelList);
        return prefix + "add";
    }

    /**
     * 保存新闻
     * @param newsDto
     * @return
     */
   /* @RequiresRoles("sys_news_admin")*/
    @PostMapping("/save")
    @ResponseBody
    public ReturnResult save(WebsiteNewsDto newsDto) {
 
        boolean flag = websiteNewsService.save(newsDto);
        if(flag) {
            return ReturnResult.successResult();
        }
        return null;
    }

    /**
     * 下载新闻附件
     * @param fileName
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/files/{userId}/{year}/{month}/{day}/{fileName}")
    public void downloadFile(@PathVariable String userId, @PathVariable String fileName,@PathVariable String year,
                             @PathVariable String month,@PathVariable String day,
                             HttpServletRequest request, HttpServletResponse response) throws IOException {
        BrowserSetUtil.browserDownload(fileName,request,response);
        StringBuilder sb = new StringBuilder(fileDir);
        sb.append(userId).append("/").append(year).append("/").append(month).
                append("/").append(day).append("/").append(fileName);
        FileUtil.download(sb.toString(),response);
    }
    
   /* @RequiresRoles("sys_news_admin")*/
    @GetMapping("/update")
    public String update(Long newsId,Model model) {
/*    	Website website = websiteService.selectByUserId(ShiroUtil.getUserId());*/
    	WebsiteNews news = websiteNewsService.getNewsById(newsId);
        /*List<WebsiteNewsChannel> parentChannelList = websiteNewsChannelService.getParentChannel(website.getRecordId());*/
       /* model.addAttribute("parentChannelList", parentChannelList);*/
    	model.addAttribute("news", news);
    	return prefix + "update";
    }
    /**
     * 更改新闻显示状态status
     * 0：隐藏，1：显示
     *
     * @param recordId
     * @param status
     * @return
     */
/*    @RequiresRoles("sys_platform_admin")*/
    @ResponseBody
    @RequestMapping("/updateStatus.html")
    public ReturnResult updateStatus(Long recordId, Boolean status) {
        if (websiteNewsService.updateStatus(recordId, status) == 1) {
            return ReturnResult.result(200, ResultConstants.SUCCESS);
        } else {
            return ReturnResult.result(400, ResultConstants.FAILURE);
        }
    }
}
