layui.use(['form','layer','jquery'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer
    $ = layui.jquery;

    //登录按钮
    form.on("submit(login)",function(data){
    	if($("#username").val() == ''){
    		layer.msg("请输入用户名");
    	}else if($("#password").val() == ''){
    		layer.msg("请输入密码");
    	}else{
            $.post("/login.html",{
                username : $("#username").val(),
                password : $("#password").val()
            },function(res){
                if (res.code == 0) {
                    location.href = '/user/index.html';
                } else {
                    layer.msg(res.msg);
                }
            });
    		
    	}

        return false;
    });
	$("#password").keypress(function (e) {
	    if (e.which == 13) {
	        $.post("/login.html",{
	            username : $("#username").val(),
	            password : $("#password").val()
	        },function(res){
	            if (res.code == 0) {
	                location.href = '/user/index.html';
	            } else {
	                layer.msg(res.msg);
	            }
	        });
	    }
	});	
    //表单输入效果
    $(".loginBody .input-item").click(function(e){
        e.stopPropagation();
        $(this).addClass("layui-input-focus").find(".layui-input").focus();
    });
    $(".loginBody .layui-form-item .layui-input").focus(function(){
        $(this).parent().addClass("layui-input-focus");
    });
    $(".loginBody .layui-form-item .layui-input").blur(function(){
        $(this).parent().removeClass("layui-input-focus");
        if($(this).val() != ''){
            $(this).parent().addClass("layui-input-active");
        }else{
            $(this).parent().removeClass("layui-input-active");
        }
    })
});
