layui.use(['layer', 'table', 'element'], function () {
    var $ = layui.jquery,
        table = layui.table;

    var cols, tableData;
    $(document).ready(function () {
        $("#apply").hide();
        $.get("/enterpriseApp/certification.html?", {date: new Date()}, function (res) {
            if (res.data === '已认证') {
                $("#apply").show();
                // 标签表格
                table.render({
                    elem: '#enterpriseAppList',
                    url: '/enterpriseApp/userListByPage.html',
                    method: 'post',
                    height: "full-85",
                    id: "enterpriseAppList",
                    page: true,
                    limits: [10, 20, 50, 100],
                    limit: 10,
                    done: function (res, curr, count) {
                        certification(res.msg);
                        tableData = res.data;
                    },
                    cols: [[
                        {align: 'center', type: 'checkbox'},
                        {align: 'center', type: 'numbers', title: '序号'},
                        {
                            field: 'declarationUnit', title: '申报单位', width: 240, align: "center",
                            templet: function (d) {
                                return "<span class='line-text' title='" + d.declarationUnit + "'>" + d.declarationUnit + "</span>";
                            }
                        },
                        {field: 'product', title: '主要产品', width: 150, align: "center"},
                        {
                            field: 'lastYearFinance.yearSalesVolume',
                            title: '上年销售额',
                            width: 100,
                            align: "center",
                            templet: function (d) {
                                if (d.lastYearFinance != null) {
                                    return d.lastYearFinance.yearSalesVolume;
                                }
                            }
                        },
                         {
                             field: 'lastYearFinance.patentYearSalesVolume',
                             title: '专利产品年销售额',
                             width: 150,
                             align: "center",
                             templet: function (d) {
                                 if (d.lastYearFinance != null) {
                                     return d.lastYearFinance.patentYearSalesVolume;
                                 }
                             }
                         },
                        {
                            field: 'patentDepartment.contacts',
                            title: '部门联系人',
                            width: 100,
                            align: "center",
                            templet: function (d) {
                                if (d.patentDepartment != null) {
                                    return d.patentDepartment.contacts;
                                }
                            }
                        },
                        {
                            field: 'patentDepartment.phone',
                            title: '部门电话',
                            width: 120,
                            align: "center",
                            templet: function (d) {
                                if (d.patentDepartment != null) {
                                    return d.patentDepartment.phone;
                                }
                            }
                        },
                        {field: 'insertTime', title: '填写/提交日期', width: 170, align: "center"},
                        {
                            field: 'supportStatus', title: '申请状态', width: 100, align: "center", templet: function (d) {
                                if (d.supportStatus === 0) {
                                    return "未申请";
                                }
                                if (d.supportStatus === 1) {
                                    return "申请中";
                                }
                                if (d.supportStatus === 2) {
                                    return "审批通过";
                                }
                                if (d.supportStatus === 3) {
                                    return "驳回申请";
                                }
                                if (d.supportStatus === 4) {
                                    return "资助完成";
                                }
                                if (d.supportStatus === 5) {
                                    return "未提交";
                                }
                            }
                        },
                        {
                            title: '操作',
                            width: 170,
                            templet: '#enterpriseAppListBar',
                            fixed: "right",
                            align: "center"
                        }
                    ]],
                    parseData: function (res) {
                        cols = this.cols[0]; //当前表格的表头
                    }
                });
            } else {
                certification(res.data);
            }
        });
    });

    //列表操作
    table.on('tool(enterpriseAppList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        //修改
        if (layEvent === 'update') {
            var index = layui.layer.open({
                title: "修改",
                type: 2,
                area: ['100%', '100%'],
                content: '/enterpriseApp/updatePage.html?id=' + data.id,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);
                }
            });
        }

        //查看详情
        if (layEvent === 'show') {
            var index = layui.layer.open({
                title: "查看详情",
                type: 2,
                area: ['100%', '100%'],
                content: '/enterpriseApp/toShowPage.html?id=' + data.id,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500);
                }
            });
        }
    });

    //填写申请
    $("#apply").on("click", function () {
        // 判断是否通过实名认证
        $.get("/enterpriseApp/certification.html", {date: new Date()}, function (res) {
            if (res.data === "已认证") {
                var index = layui.layer.open({
                    title: "填写申请",
                    type: 2,
                    area: ['100%', '100%'],
                    content: '/enterpriseApp/toSavePage.html',
                    success: function (layero, index) {
                        setTimeout(function () {
                            layui.layer.tips('点击此处返回申请列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        }, 500);
                    }
                });
            } else {
                certification(res.data);
            }
        });
    });

    function certification(msg) {
        if (msg === '个人认证') {
            layer.open({
                title: '提示'
                , content: '抱歉，您是个人认证不能填写申请表！'
                , btn: ['返回']
                , yes: function (index, layero) {
                    // layer.closeAll("iframe");
                    // //刷新父页面
                    // parent.location.reload();
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === '未认证') {
            layer.open({
                title: '提示'
                , content: "抱歉，您未进行企业认证无法填写！<br/>（注意:非个人认证才可填写申请表）"
                , btn: ['实名认证', '取消']
                , yes: function (index, layero) {
                    layer.close(index);
                    location.href = '/certification/indexPage.html';
                }
                , btn2: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "个人认证审核中") {
            layer.open({
                title: '提示'
                , content: "抱歉，您提交的待审核信息为个人认证，无法填写申请表！<br/>（注意:非个人认证才可填写申请表）"
                , btn: ['取消']
                , yes: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "非个人认证审核中") {
            layer.open({
                title: '提示'
                , content: "抱歉，您的认证信息正在审核中！"
                , btn: ['取消']
                , yes: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                }, success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "驳回") {
            layer.open({
                title: "提示",
                content: "抱歉，你提交的审核被驳，请重新填写！<br/><span" +
                    " style='color:rgba(242,179,68,1);font-size:13px;'>（为了确保您的认证顺利通过，请认真填写信息）</span> ",
                btn: ['实名认证', '取消'],
                yes: function (index, layero) {
                    layer.close(index);
                    location.href = '/certification/indexPage.html';
                },
                btn2: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                },
                success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                },
                cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        } else if (msg === "临时保存") {
            layer.open({
                title: "提示"
                , content: "请完成认证信息的填写！"
                , btn: ['去完成', '取消']
                , yes: function (index, layero) {
                    layer.close(index);
                    location.href = '/certification/indexPage.html';
                },
                btn2: function (index, layero) {
                    $(".layui-this i ", parent.document).click();
                },
                success: function (layero) {
                    layero.find('.layui-layer-btn').css('text-align', 'center')
                }, cancel: function () {
                    $(".layui-this i ", parent.document).click();
                }
            });
        }
    }

    //导出excel表格
    // $(".getCheckData").on("click", function () { //获取选中数据
    //     var cols2 = JSON.stringify(cols);
    //     var tableHead = cols2;//表头数据
    //     var data = table.checkStatus('enterpriseAppList').data;
    //     if (data.length === 0) {
    //         //为选择数据，导出全部
    //         $.ajax({
    //             url: "/enterpriseApp/exportAll.html",
    //             type: "post",
    //             data: {"tableHead": tableHead, "supportStatus": $("#status").val(), keyword: $("#keyword").val()},
    //             success: function (res) {
    //                 if (res.code === 0) {
    //                     layer.msg("表格导出成功！");
    //                     $("#status").val('');
    //                     location.reload();
    //                 } else {
    //                     lay.msg("导出失败！");
    //                 }
    //             }
    //         });
    //         return false;
    //     } else if (data.length !== 0) {
    //         var arrTableData = JSON.parse(JSON.stringify(data));
    //         var ids = '';
    //         for (var i = 0; i < arrTableData.length; i++) {
    //             ids += arrTableData[i].id + ",";
    //         }
    //         $.ajax({
    //             url: "/enterpriseApp/excel.html",
    //             type: "post",
    //             data: {tableHead: tableHead, "ids": ids},
    //             success: function (res) {
    //                 if (res.code === 0) {
    //                     layer.msg("表格导出成功！");
    //                     location.reload();
    //                 } else {
    //                     lay.msg("导出失败！");
    //                 }
    //             }
    //         });
    //     }
    // })
});