package com.lotut.system.shiro.service;

import com.lotut.system.domain.User;
import com.lotut.system.service.UserService;
import com.lotut.system.shiro.exception.UserNotExistsException;
import com.lotut.system.shiro.exception.UserPasswordNotMatchException;
import com.lotut.system.shiro.util.BcryptPasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 
 * @date 2020/3/4
 */
@Component
public class LoginService {
    @Autowired
    private UserService userService;

    /**
     * 用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    public User login(String username, String password) {
        // 用户名或密码为空
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new UserNotExistsException();
        }
        // 查询用户信息
        User user = userService.findByUsername(username);
        if(user == null) {
            throw new UserNotExistsException();
        }
        if (!matches(user, password)) {
            throw new UserPasswordNotMatchException();
        }
        return user;
    }

    /**
     * 判断密码是否正确
     *
     * @param user
     * @param newPassword
     * @return
     */
    private boolean matches(User user, String newPassword) {
        return BcryptPasswordUtil.matches(newPassword, user.getPassword());
    }

}
