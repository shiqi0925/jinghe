// +----------------------------------------------------------------------
// | desc: layout js
// +----------------------------------------------------------------------
// | Author:张亮
// +----------------------------------------------------------------------

/**
 *desc:初始化加载
 *@param void;
 *@return void;
 */
$(function () {
 
    //导航左侧栏切换
    func.sidebarTab();
    //block1交易切换
    func.block1Server();
   
});

/**
 *desc:构造函数
 *@param void;
 *@return void;
 */
function Func() {
};

/**
 *desc:导航左侧栏切换
 *@param void;
 *@return void;
 */
Func.prototype.sidebarTab = function () {
    $(".sidebar .single").hover(function () {
        if(!$(this).hasClass('single6')){
          $(".sidebar .single").removeClass('on');
        };
        $(this).addClass('on');
    }, function () {
        $(".sidebar .single").removeClass('on');
        $(".sidebar .single").eq(0).addClass('on');
    });
};

/**
 *block1切换
 */
Func.prototype.block1Server = function() {
$('#block1_server').slide({
 titCell: '.hd ul li',
 mainCell: '.bd ul',
 delayTime: 0,
 interTime: 6000
})
}

 

 
   

// +----------------------------------------------------------------------
// | Desc:回到顶部
// +----------------------------------------------------------------------
Func.prototype.top = function() {
  $('html,body').animate(
    {
      scrollTop: 0
    },
    800
  )
}


/**
 *desc:构造函数实例化
 */
var func = new Func();
