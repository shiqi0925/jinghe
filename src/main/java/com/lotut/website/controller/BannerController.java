package com.lotut.website.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lotut.common.constants.WebsiteConstants;
import com.lotut.common.dto.Page;
import com.lotut.common.dto.ReturnResult;
/*import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.dto.WebsiteNewsDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;*/
import com.lotut.system.shiro.util.ShiroUtil;
import com.lotut.website.domain.Banner;
import com.lotut.website.domain.Website;
import com.lotut.website.dto.BannerDto;
import com.lotut.website.dto.BannerSearchCondition;
import com.lotut.website.service.BannerService;
import com.lotut.website.service.WebsiteService;


/**
 * 网站前端页面控制
 *
 * @author ShiQi
 * @date 2020/4/10
 */

@Controller
public class BannerController {
/*    private String prefix = "admin/news/";*/

    @Resource
    private WebsiteService websiteService;
    
    @Resource
    private BannerService bannerService;
        
	
	@RequestMapping("/admin/banner_list.html")
    public String getList(Model model,HttpServletRequest request) {
		//查询bannerlist
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
		if(website != null){
			Long websiteId = website.getRecordId();
			BannerSearchCondition condition = new BannerSearchCondition();
			condition.setWebsiteId(websiteId);
			condition.setIsDelete(false);
			condition.setPage(1);
			condition.setLimit(100);
			List<Banner> list = bannerService.selectList(condition);
			
			model.addAttribute("bannerList", list);
			
			return "admin/banner/list";	
			
		}
		return "error/404";

	}	
	
	
	@RequestMapping("/admin/banner_edit.html")
    public String getIndex(Long id,Model model,HttpServletRequest request) {
		
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
		Banner detail = new Banner();
		if(id != null) {
			Banner bannerDetail=bannerService.selectByPrimaryKey(id, website.getRecordId());
			detail = bannerDetail;
		}
		model.addAttribute("detail", detail);
		
		model.addAttribute("id", id);
		return "admin/banner/edit";
	}	

    @PostMapping(value = "/admin/banner_save.html")
    @ResponseBody
    public ReturnResult bannerSave(BannerDto detailDto,HttpServletRequest request) {
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
        detailDto.setWebsiteId(website.getRecordId());
        boolean flag = bannerService.save(detailDto);
        if(flag) {
            return ReturnResult.successResult();
        }
        return null;
    } 
 
 
    @PostMapping("/admin/banner_delete.html")
    @ResponseBody
    public ReturnResult delete(Long id,HttpServletRequest request) {
    	String domainUrl = request.getServerName();
    	Website website = websiteService.selectByDomainUrl(domainUrl);
    	if(website != null){
            if(website != null){
            	BannerDto dto = new BannerDto();
            	dto.setWebsiteId(website.getRecordId());
            	dto.setId(id);
            	dto.setIsDelete(true);
            	boolean flag = bannerService.save(dto);
                if(flag) {
                    return ReturnResult.successResult();
                }
                return null;
            }	
    		
    	}
        return null;
    }  
 

 
}
