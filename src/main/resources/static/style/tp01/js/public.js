    $(".sidebar .single").hover(function () {
        $(".sidebar .single").removeClass('on');
        $(this).addClass('on');
    });	
    <!--顶部搜索框-->
    new listDown({
        selectDom: '.pull_down',
        optionDom: '.all_sorts',
        acquireDataDom: 'data',
        showDataDom: '.select_sort',
        hideSelectDom: '.hideSelectDom'
    }).init({
        unfoldOrClose: true,
        orderClose: true
    });
    new listDown({
        selectDom: '.pop_pull_down',
        optionDom: '.pop_all_sorts',
        acquireDataDom: 'pop_data',
        showDataDom: '.pop_select_sort',
        hideSelectDom: '.pop_hideSelectDom',
        orderCloseDom: '.pop_pull_down'
    }).init({
        unfoldOrClose: true,
        orderClose: true
    });
    function get_type(type,obj,sign) {
        var name=$(obj).html();
        $('.pop_select_sort').html(name);
        $('#type').val(type);
        if(sign==1){
            if(type==1){
                $('.pop_title').html("商标免费查询");
                $('.free_check').html('免费查询');
                //$('.search_frame .input input').inputPlaceholder('input_brand','请输入商标名称');
            }
            if(type==2){
                $('.pop_title').html("专利免费检索");
                $('.free_check').html('免费查询');
                //$('.search_frame .input input').inputPlaceholder('input_brand','请输入专利名称');
            }
        }else{
            if(type==1){
                $('.pop_title').html("商标免费查询");
                $('.free_check').html('免费查询');
                //$('.search_frame .input input').inputPlaceholder('pop_name','请输入商标名称');
            }
            if(type==2){
                $('.pop_title').html("专利免费检索");
                $('.free_check').html('免费查询');
                //$('.search_frame .input input').inputPlaceholder('pop_name','请输入专利名称');
            }

        };


        //标志切换
        var biaozhi='/html/images/';
        if(type==1){
            biaozhi=biaozhi+'icon_search_1.png';
        }else if(type==2){
            biaozhi=biaozhi+'icon_search_2.png';
        };
        $("#biaozhi").attr("src",biaozhi);


    }
    $('.free_check').on('click',function(){
    	var type = $('.select_sort').html();
    	var keyword = $('#input_brand').val();
    	if(type == "商标"){
    		//window.location.href = '/shop_brand_search/---1-'+keyword+'.html';
    		window.open('/shop_brand_search/---1-'+keyword+'.html');//新页面打开
    	}else{
    		//window.location.href = '/shop_patent_search/---1-'+keyword+'.html';
    		window.open('/shop_patent_search/---1-'+keyword+'.html');
    	}
    })
    //免费搜索提交表单
    $('.search_message_button').on('click',function(){
    	//var type = $('.select_sort').html();
    	//var keyword = $('#input_brand').val();
    	layui.use(['layer', 'form'], function(){
    		  var layer = layui.layer
    		  ,form = layui.form;
	    	layer.open({
	    		  title: '提示'
	    		  ,content: '提交成功'
	    		});     
    		}); 	
    })    