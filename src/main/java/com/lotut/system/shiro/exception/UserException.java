package com.lotut.system.shiro.exception;

import com.lotut.common.exception.base.BaseException;

/**
 * 用户异常类
 *
 * @author Wang JunJie
 * @date 2018/9/5 16:57.
 */
public class UserException extends BaseException {
    private static final long serialVersionUID = 3944328784951581245L;

    public UserException(String code, Object[] args) {
        super("user", code, args, null);
    }
}
