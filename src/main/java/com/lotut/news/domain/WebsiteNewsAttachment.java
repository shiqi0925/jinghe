package com.lotut.news.domain;

import java.io.Serializable;

/**
 * 用户网站新闻附件实体
 *
 * @author Administrator
 */
public class WebsiteNewsAttachment implements Serializable {
    private static final long serialVersionUID = 2418242035932619651L;
    /**
     * 自增主键
     */
    private Long recordId;
    /**
     * 网站新闻id
     */
    private Long newsId;
    /**
     * 附件名称
     */
    private String name;
    /**
     * 附件地址
     */
    private String path;

    public WebsiteNewsAttachment(Long recordId, Long newsId, String name, String path) {
        this.recordId = recordId;
        this.newsId = newsId;
        this.name = name;
        this.path = path;
    }

    public WebsiteNewsAttachment(Long newsId, String name, String path) {
        this.newsId = newsId;
        this.name = name;
        this.path = path;
    }

    public WebsiteNewsAttachment() {
        super();
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }
}