package com.lotut.news.service.impl;


import com.lotut.news.dao.FlinkMapper;
import com.lotut.news.dao.SeoMapper;
import com.lotut.news.domain.Flink;
import com.lotut.news.domain.Seo;
import com.lotut.news.service.FlinkService;
import com.lotut.news.service.SeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public class FlinkServiceImpl implements FlinkService {

    @Autowired
    private FlinkMapper flinkMapper;

    @Override
    public List<Flink> selectList(){
        return flinkMapper.selectList();
    }

	@Override
	public Flink getById(Long id) {
		return flinkMapper.getById(id);
	}

	@Override
	public boolean save(Flink flink) {
		
		if(flink.getId() != null) {
			return flinkMapper.save(flink);
			
		}else {
			return flinkMapper.add(flink);
		}
		
	}

 
  
    
}
