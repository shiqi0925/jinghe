/*
 * Project：ipsearch
 * File: AESUtil.java
 * Date: 2019/3/14
 * Copyright (c) 2019. 合肥龙图腾信息技术有限公司版权所有.
 */

package com.lotut.common.util.aes;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * @author Wang JunJie
 * @date 2019/3/14 16:55
 */
public class AESUtil {

    private static final String KEY_ALGORITHM = "AES";
    /**
     * 默认的加密算法
     */
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    /**
     * 密码
     */
    public static final String PASSWORD = "lotutfms";

    /**
     * AES 加密操作
     *
     * @param content 待加密内容
     * @param password 加密密码
     * @return 返回Base64转码后的加密数据
     */
    public static byte[] encrypt(String content, String password) {
        try {
            // 创建密码器
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);
            // 初始化为加密模式的密码器
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(password));
            // 加密
            return cipher.doFinal(byteContent);
        } catch (Exception ex) {
            Logger.getLogger(AESUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     * AES 解密操作
     *
     */
    public static byte[] decrypt(byte[] content, String password) {
        try {
            //实例化
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            //使用密钥初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(password));
            //执行操作
            return cipher.doFinal(content);
        } catch (Exception ex) {
            Logger.getLogger(AESUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    /**
     * AES 解密操作
     *
     */
    public static String decrypt(String secretAppKey) {

        try {
            byte[] decryptFrom = parseHexStr2Byte(secretAppKey);
            //实例化
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            //使用密钥初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey());
            //执行操作
            byte[]  secret=cipher.doFinal(Objects.requireNonNull(decryptFrom));
            return  new String(secret);
        } catch (Exception ex) {
            Logger.getLogger(AESUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * 生成加密秘钥
     *
     */
    private static SecretKey getSecretKey() {
        try {
            KeyGenerator generator = KeyGenerator.getInstance( "AES" );
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG" );
            secureRandom.setSeed(AESUtil.PASSWORD.getBytes());
            generator.init(128,secureRandom);
            return generator.generateKey();
        }  catch (Exception e) {
            throw new RuntimeException( " 初始化密钥出现异常 " );
        }
    }



    /**
     * 生成加密秘钥
     *
     */
    private static SecretKey getSecretKey(final String password) {
        try {
            KeyGenerator generator = KeyGenerator.getInstance( "AES" );
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG" );
            secureRandom.setSeed(password.getBytes());
            generator.init(128,secureRandom);
            return generator.generateKey();
        }  catch (Exception e) {
            throw new RuntimeException( " 初始化密钥出现异常 " );
        }
    }

    /**
     * 将16进制转换为二进制
     */
    public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1) {
            return null;
        }
        byte[] result = new byte[hexStr.length()/2];
        for (int i = 0;i< hexStr.length()/2; i++) {
            int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);
            int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

    /**
     * 将二进制转换成16进制
     */
    public static String parseByte2HexStr(byte[] buf) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 专利加密
     * @param patentAppNo
     * @param patentType
     * @param patentPublishDate
     * @return
     */
    public static String encryptParam(String patentAppNo, String patentType, String patentPublishDate) {
        String param = patentAppNo + "&" + patentType + "&" + patentPublishDate;
        return parseByte2HexStr(encrypt(param, PASSWORD));
    }

    /**
     * 缩略图请求参数加密
     */
    public static String encryptThumbnailParam(String patentAppNo, String patentType, String patentPublishDate) {
        String param = patentAppNo + "&" + patentType + "&" + patentPublishDate + "&thumbnail";
        return parseByte2HexStr(encrypt(param, PASSWORD));
    }

    /**
     * 阅读全文请求参数加密
     */
    public static String encryptPdfReadParam(String patentAppNo, String patentType, String patentPublishDate) {
        String param = patentAppNo + "&" + patentType + "&" + patentPublishDate + "&pdfRead";
        return parseByte2HexStr(encrypt(param, PASSWORD));
    }

    /**
     * 双屏阅读请求参数加密
     */
    public static String encryptPdfDoubleReadParam(String patentAppNo, String patentType, String patentPublishDate) {
        String param = patentAppNo + "&" + patentType + "&" + patentPublishDate + "&pdfDoubleRead";
        return parseByte2HexStr(encrypt(param, PASSWORD));
    }

    /**
     * pdf单个下载请求参数加密
     */
    public static String encryptPdfDownloadParam(String patentAppNo, String patentType, String patentPublishDate, String patentName) {
        String param = patentAppNo + "&" + patentType + "&" + patentPublishDate + "&" + patentName + "&downloadPdf";
        return parseByte2HexStr(encrypt(param, PASSWORD));
    }

    /**
     * 商标图样请求参数加密
     * @param tmAppNo
     * @param tmAppDate
     */
    public static String encryptParam(String tmAppNo,String tmAppDate) {
        String param = tmAppNo + "&" + tmAppDate;
        return parseByte2HexStr(Objects.requireNonNull(encrypt(param, PASSWORD)));
    }

    /**
     * 专利分析报告订单id加密
     * @param orderId 专利分析报告订单id
     * @return
     */
    public static String encryptPatentAnalyzerReportOrderId(String orderId) {
        return parseByte2HexStr(encrypt(orderId, PASSWORD));
    }

    /**
     * 专利分析报告订单id解密
     * @param q 专利分析报告订单id加密串
     * @return
     */
    public static String decryptPatentAnalyzerReportOrderId(String q) {
        return decrypt(q);
    }
    
    /**
     * 专利缴纳年费订单id加密
     * @param orderId 专利缴纳年费订单id
     * @return
     */
    public static String encryptPatentAnnualFeeOrderId(String orderId) {
        return parseByte2HexStr(encrypt(orderId, PASSWORD));
    }

    /**
     * 专利缴纳年费订单id解密
     * @param q 专利缴纳年费订单id加密串
     * @return
     */
    public static String decryptPatentAnnualFeeOrderId(String q) {
        return decrypt(q);
    }

    /**
     * 商标报告订单id加密
     * @param orderId 专利分析报告订单id
     * @return
     */
    public static String encryptTmReportOrderId(String orderId) {
        return parseByte2HexStr(encrypt(orderId, PASSWORD));
    }

    /**
     * 商标报告订单id解密
     * @param q 专利分析报告订单id加密串
     * @return
     */
    public static String decryptTmReportOrderId(String q) {
        return decrypt(q);
    }
    
    /**
     * 订单id加密
     * @param orderId 钱包充值订单id
     * @return
     */
    public static String encryptWalletRechargeOrderId(String orderId) {
        return parseByte2HexStr(encrypt(orderId, PASSWORD));
    }

    /**
     * 钱包充值订单id解密
     * @param q 钱包充值订单id加密串
     * @return
     */
    public static String decryptWalletRechargeOrderId(String q) {
        return decrypt(q);
    }

    
}
