  layui.config({
    base: '/admin_static/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index', //主入口模块
    treeTable: 'treeTable/treeTable'
  }).use(['index','table','treeTable'], function () {
	 // /admin/service_list?typeId=1
	  var table=layui.table,$=layui.$,treeTable=layui.treeTable;
	  
	  getChannel()
	  function getChannel(){
	        $.ajax({
	            url: '/admin/news/channel_list',
	            type: 'post',
	            dataType: 'json',
	            success: function (res) {
	            	console.log(res)
	            	renderTable(res.data);
	            }
	          });  
		  
	  }
	  var renderTable = function (data) {
		  insTb = treeTable.render({
	          elem: '#serviceList',
	          data:data,
	          tree: {
	              iconIndex: 2
	          },
	          cols: [
	              {type: 'numbers',title: '序号',width: 80, align: "center",},
	              {type: 'checkbox', align: "center"},
	              {field: 'typeName', title: '频道名称',width:200, align: "center"},
	                {field: 'status', title: '是否显示', align: "center",templet:function (d) {
	                    if(d.status) {
	                        return '<input type="checkbox" value="'+d.id+'" name="switch" lay-skin="switch" lay-filter="statusSwitch" lay-text="是|否" checked>';
	                    }else{
	                    	 return '<input type="checkbox" value="'+d.id+'" name="switch" lay-skin="switch" lay-filter="statusSwitch" lay-text="是|否">';
	                    }
	                }},
	           
	      ],
	          style: 'margin-top:0;'
	      });
	  }
	  
	  
	  
	  
	  
  })