package com.lotut.news.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lotut.common.constants.ResultConstants;
import com.lotut.common.dto.Page;
import com.lotut.common.dto.ReturnResult;
import com.lotut.common.util.BrowserSetUtil;
import com.lotut.common.util.FileUtil;
import com.lotut.website.domain.Website;
import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.news.domain.Seo;
import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.dto.WebsiteNewsDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;
import com.lotut.news.service.SeoService;
import com.lotut.news.service.WebsiteNewsChannelService;
import com.lotut.news.service.WebsiteNewsService;
import com.lotut.website.service.WebsiteService;
import com.lotut.system.shiro.util.ShiroUtil;

/**
 * 用户网站新闻控制层
 *
 * @author xwj
 * @date 2020/3/11
 */
@RequestMapping("/admin/seo")
@Controller
public class WebsiteSeoController {
    private String prefix = "admin/seo/";
    @Autowired
    private SeoService seoService;
    @Autowired
    private WebsiteNewsChannelService websiteNewsChannelService;
    @Autowired
    private WebsiteService websiteService;

 
    /**
     * 跳转到新闻列表页面
     *
     * @return
     */
   /* @RequiresRoles("sys_news_admin")*/
    @GetMapping("/list")
    public String list(String type,Model model) {
    	model.addAttribute("type", type);
        return prefix + "list";
    }

    /**
     * 查询列表
     */
   /* @RequiresRoles("sys_news_admin")*/
    @PostMapping(value = "/list")
    @ResponseBody
    public ReturnResult getNewsListByPage(WebsiteNewsSearchCondition condition) {
    	
    	List<Seo> page = seoService.selectList();
        return ReturnResult.successResult(page);
    }
    @GetMapping("/update")
    public String update(Long id,Model model) {
    	Seo news = seoService.getById(id);
    	model.addAttribute("news", news);
    	return prefix + "update";
    }

    @PostMapping("/save")
    @ResponseBody
    public ReturnResult save(Seo seo) {
 
        boolean flag = seoService.save(seo);
        if(flag) {
            return ReturnResult.successResult();
        }
        return null;
    }

}
