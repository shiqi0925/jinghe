package com.lotut.common.constants;

/**
 * 专利分析常量
 * @author xwj
 * @date 2019/10/26
 */
public class ExcelConstants {
    /**
     * 专利分析文件存储基本路径
     * 本地：C:/image/par/
     * 线上：/opt/media/analyzer/
     */
/*    public static final String ANALYZER_FILE_BASE_PATH = "/opt/media/analyzer/";*/

/*    public static final String ANALYZER_WORD_TEMPLATE_Path = ANALYZER_FILE_BASE_PATH + "template/";*/
    public static final String EXCLE_TEMPLATE_Path = "E:\\1java\\excel\\template";
//    /**
//     * 普通用户专利分析报告下载所需的人民币（单位：分）2000
//     */
//    public static final Integer NORMAL_REPORT_AMOUNT_RMB = 200;
//
//    /**
//     * ZVIP1专利分析报告下载所需的人民币（单位：分）1000
//     */
//    public static final Integer ZVIP1_REPORT_AMOUNT_RMB = 100;
//
//    /**
//     * ZVIP2专利分析报告下载所需的人民币（单位：分）500
//     */
//    public static final Integer ZVIP2_REPORT_AMOUNT_RMB = 50;
//
//    /**
//     * PVIP专利分析报告下载所需的人民币（单位：分）2000
//     */
//    public static final Integer PVIP_REPORT_AMOUNT_RMB = 200;
//
//
//    /**
//     * 普通用户专利分析报告下载所需的ip币 200
//     */
//    public static final Integer NORMAL_REPORT_AMOUNT_IPC = 20;
//
//    /**
//     * ZVIP1专利分析报告下载所需的ip币 100
//     */
//    public static final Integer ZVIP1_REPORT_AMOUNT_IPC = 10;
//
//    /**
//     * ZVIP2专利分析报告下载所需的ip币 50
//     */
//    public static final Integer ZVIP2_REPORT_AMOUNT_IPC = 5;
//
//    /**
//     * PVIP专利分析报告下载所需的ip币 200
//     */
//    public static final Integer PVIP_REPORT_AMOUNT_IPC = 20;


}
