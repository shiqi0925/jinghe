
package com.lotut.website.dto;

import java.io.Serializable;

import com.lotut.common.dto.BaseSearchCondition;

/**
 * banner
 * 
 * @author sq
 * @version 1.0.0 2020-04-10
 */

public class BannerSearchCondition extends BaseSearchCondition{
    /** 版本号 */
    private static final long serialVersionUID = 6231922319224316417L;

    /** id */
    
    private Long id;

    /** 站点id */
    
    private Long websiteId;

    /** 图片路劲 */
    
    private String picPath;

    /** 标题 */
    
    private String bannerTitle;

    /** 图片链接 */
    
    private String pageUrl;

    /** 排序，升序 */
    
    private Integer orderSort;

    /** 添加时间 */
    
    private Integer bannerAddtime;

    /** 是否删除：0=未删除，1=已删除 */
    
    private Boolean isDelete;

    /** 是否显示：0=不显示，1=显示 */
    
    private Boolean isSee;

    /** 暂时只有PC首页 1 */
    
    private Integer bannerType;

    /**
     * 获取id
     * 
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取站点id
     * 
     * @return 站点id
     */
    public Long getWebsiteId() {
        return this.websiteId;
    }

    /**
     * 设置站点id
     * 
     * @param websiteId
     *          站点id
     */
    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    /**
     * 获取图片路劲
     * 
     * @return 图片路劲
     */
    public String getPicPath() {
        return this.picPath;
    }

    /**
     * 设置图片路劲
     * 
     * @param picPath
     *          图片路劲
     */
    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    /**
     * 获取标题
     * 
     * @return 标题
     */
    public String getBannerTitle() {
        return this.bannerTitle;
    }

    /**
     * 设置标题
     * 
     * @param bannerTitle
     *          标题
     */
    public void setBannerTitle(String bannerTitle) {
        this.bannerTitle = bannerTitle;
    }

    /**
     * 获取图片链接
     * 
     * @return 图片链接
     */
    public String getPageUrl() {
        return this.pageUrl;
    }

    /**
     * 设置图片链接
     * 
     * @param pageUrl
     *          图片链接
     */
    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    /**
     * 获取排序，升序
     * 
     * @return 排序
     */
    public Integer getOrderSort() {
        return this.orderSort;
    }

    /**
     * 设置排序，升序
     * 
     * @param orderSort
     *          排序
     */
    public void setOrderSort(Integer orderSort) {
        this.orderSort = orderSort;
    }

    /**
     * 获取添加时间
     * 
     * @return 添加时间
     */
    public Integer getBannerAddtime() {
        return this.bannerAddtime;
    }

    /**
     * 设置添加时间
     * 
     * @param bannerAddtime
     *          添加时间
     */
    public void setBannerAddtime(Integer bannerAddtime) {
        this.bannerAddtime = bannerAddtime;
    }

    /**
     * 获取是否删除：0=未删除，1=已删除
     * 
     * @return 是否删除：0=未删除
     */
    public Boolean getIsDelete() {
        return this.isDelete;
    }

    /**
     * 设置是否删除：0=未删除，1=已删除
     * 
     * @param isDelete
     *          是否删除：0=未删除
     */
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取是否显示：0=不显示，1=显示
     * 
     * @return 是否显示：0=不显示
     */
    public Boolean getIsSee() {
        return this.isSee;
    }

    /**
     * 设置是否显示：0=不显示，1=显示
     * 
     * @param isSee
     *          是否显示：0=不显示
     */
    public void setIsSee(Boolean isSee) {
        this.isSee = isSee;
    }

    /**
     * 获取暂时只有PC首页 1
     * 
     * @return 暂时只有PC首页 1
     */
    public Integer getBannerType() {
        return this.bannerType;
    }

    /**
     * 设置暂时只有PC首页 1
     * 
     * @param bannerType
     *          暂时只有PC首页 1
     */
    public void setBannerType(Integer bannerType) {
        this.bannerType = bannerType;
    }
}