layui.use(['form', 'layer'], function () {
    var form = layui.form
    layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;
 
    // 监听checkbox
    form.on('checkbox(isAttr)', function (data) {
        console.log(data.value)
        console.log(data.elem.checked);
        if(data.elem.checked==true){
        	$('.attr-box').show();
        }else{
        	$('.attr-box').hide();
        }
    })
 


    // 直接保存
    form.on("submit(save)", function (data) {
        $.ajax({
            url: "/admin/site_save",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code == 0) {
                	layer.msg("保存成功");
                } else {
 
                }
            },
        });
        return false;
    });
 
})