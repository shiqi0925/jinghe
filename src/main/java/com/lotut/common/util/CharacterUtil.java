package com.lotut.common.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

/**
 * @author Administrator
 */
public class CharacterUtil {

    private static final Logger logger = LoggerFactory.getLogger(CharacterUtil.class);

    /**
     * 下载文件
     *
     * @param path     文件路径
     * @param response http响应
     */
    public static void download(String path, HttpServletResponse response) {
        try (InputStream is = new FileInputStream(path);
             OutputStream os = response.getOutputStream()) {
            IOUtils.copy(is, os);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static boolean isNumer(String str) {
    	for (int i = 0; i < str.length(); i++) {
    		System.out.println(str.charAt(i));
    		if (!Character.isDigit(str.charAt(i))) {
    			return false;
    			}
		}
    	return true;
   }

}
