package com.lotut.system.shiro.enums;

/**
 * 模块枚举
 *
 * @author xwj
 * @date 2020/3/3
 */
public enum RoleEnum {
    SYS_ADMIN(1, "sys_admin"), SYS_NORMAL_USER(2, "sys_normal_user");
    /**
     * 模块id
     */
    private Integer roleId;
    /**
     * 模块名称
     */
    private String roleName;

    RoleEnum(Integer roleId, String roleName) {
        this.roleId = roleId;
        this.roleName = roleName;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
