package com.lotut.website.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lotut.common.dto.Page;
import com.lotut.news.domain.Flink;
import com.lotut.news.domain.Seo;
import com.lotut.news.domain.WebsiteNews;
import com.lotut.news.dto.WebsiteNewsDto;
import com.lotut.news.dto.WebsiteNewsSearchCondition;
import com.lotut.news.service.FlinkService;
import com.lotut.news.service.SeoService;
import com.lotut.news.service.WebsiteNewsService;
import com.lotut.website.service.WebsiteService;

/**
 * 网站前端页面控制
 *
 * @author ShiQi
 * @date 2020/4/10
 */

@Controller
public class HtmlController {
    private String agent = "agent_platform";
    @Autowired
    private WebsiteNewsService websiteNewsService;    
    @Autowired
    private SeoService seoService;
    @Autowired
    private FlinkService flinkService;    
    @RequestMapping("/")
    public String index(Long id,Model model,HttpServletRequest request) {
    	
    	/**首页产品**/
    	WebsiteNewsSearchCondition  productCondition = new WebsiteNewsSearchCondition();
    	productCondition.setChannelId((long) 1);
    	productCondition.setPage(1);
    	Page<WebsiteNews> productPage = websiteNewsService.getUserNewsListByPage(productCondition);
    	model.addAttribute("productPage", productPage);
    	/**首页车间**/
    	WebsiteNewsSearchCondition  workCondition = new WebsiteNewsSearchCondition();
    	workCondition.setChannelId((long) 2);
    	workCondition.setPage(1);
    	Page<WebsiteNews> workPage = websiteNewsService.getUserNewsListByPage(workCondition);
    	model.addAttribute("workPage", workPage);	
    	/**首页新闻动态**/
    		/**取两个有图的**/
    	WebsiteNewsSearchCondition  picNewsCondition = new WebsiteNewsSearchCondition();
    	picNewsCondition.setChannelId((long) 5);
    	picNewsCondition.setPage(1);
    	picNewsCondition.setThumbnailStatus(1);
    	Page<WebsiteNews> picNewsPage = websiteNewsService.getUserNewsListByPage(picNewsCondition);
    	model.addAttribute("picNewsPage", picNewsPage);
    		/**取一页新闻**/
    	WebsiteNewsSearchCondition  newsCondition = new WebsiteNewsSearchCondition();
    	newsCondition.setChannelId((long) 5);
    	newsCondition.setPage(1);
    	Page<WebsiteNews> newsPage = websiteNewsService.getUserNewsListByPage(newsCondition);
    	model.addAttribute("newsPage", newsPage);
    	/**首页常见问题动态**/
    	WebsiteNewsSearchCondition  askCondition = new WebsiteNewsSearchCondition();
    	askCondition.setChannelId((long) 17);
    	askCondition.setPage(1);
    	Page<WebsiteNews> askPage = websiteNewsService.getUserNewsListByPage(askCondition);
    	model.addAttribute("askPage", askPage);
    	
    	/*查询当前页的seo*/
    	Seo seo = seoService.getById((long) 1);
    	model.addAttribute("seo", seo);
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
    	
        return "html/index"; 
    }
    
    @RequestMapping("/about.html")
    public String about(Model model) {
    	/*查询当前页的seo*/
    	Seo seo = seoService.getById((long) 2);
    	model.addAttribute("seo", seo);
    	
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
        return "html/about";
    }	    
    @RequestMapping("/connect.html")
    public String connect(Model model) {
    	Seo seo = seoService.getById((long) 10);
    	model.addAttribute("seo", seo);
    	
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
        return "html/connect";
    }	
    
    @RequestMapping("/goods_list.html")
    public String goodsListPage() {
        return "html/goods_list";
    }	
    
    @RequestMapping("/list.html")
    public String workshopListPage(WebsiteNewsSearchCondition condition,Model model) {
    	//根类型查 内容
    	String typeName="";
    	System.out.print(condition.getPage());
    	 	if(condition.getPage() == null) {
    	 		condition.setPage(1);
    	 	}
        	Page<WebsiteNews> page = websiteNewsService.getUserNewsListByPage(condition);    	
        	model.addAttribute("page", page);
        	model.addAttribute("type", condition.getChannelId());
        	long seoId = 3;
        	if(condition.getChannelId() == 2) {
        		typeName = "生产车间";
        		seoId = 4;
        	}else if(condition.getChannelId() == 3) {
        		typeName = "荣誉资质";
        		seoId = 5;
        	}else if(condition.getChannelId() == 4) {
        		typeName = "工程案例";
        		seoId = 6;
        	}else if(condition.getChannelId() == 5) {
        		typeName = "新闻资讯";
        		seoId = 8;
        	}else if(condition.getChannelId() == 1) {
        		typeName = "产品中心";
            	
        	}else {
        		//System.out.print(page.getList().get(0).getSecondChannelName());
        		if(page.getList().size()>0) {
        			typeName = page.getList().get(0).getSecondChannelName();
        		}else {
        			typeName = "产品中心";
        		}

        	}
        	/*查询当前页的seo*/
        	Seo seo = seoService.getById(seoId);
        	model.addAttribute("seo", seo);
    	model.addAttribute("typeName", typeName);
    	
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
    	if(condition.getChannelId() == 5 || condition.getChannelId() == 17) {
    		return "html/news_list";
    	}
        return "html/goods_list";
    }   
    @RequestMapping("/detail.html")
    public String getDetail(long id,Model model) {
    	//根类型查 内容
    	WebsiteNews detail = websiteNewsService.getNewsById(id);
    	model.addAttribute("detail", detail);
    	
    	//保存新闻阅读量
    	WebsiteNewsDto dto = new WebsiteNewsDto();
    	dto.setRecordId(detail.getRecordId());
    	dto.setContent(detail.getContent());
    	dto.setViewCount(detail.getViewCount()+1);
    	boolean updateReadNO = websiteNewsService.save(dto);
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
        if(detail.getSecondChannelId() == 6 || detail.getSecondChannelId() == 7 || detail.getSecondChannelId() == 8 || detail.getSecondChannelId() == 9 || detail.getSecondChannelId() == 10) {
        	return "html/product_detail";
        }else {
        	return "html/news_detail";
        }
    	
        
    }     
    
    @RequestMapping("/goods_detail.html")
    public String goodsDetailPage(Model model) {
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
        return "html/goods_detail";
    }	 
    @RequestMapping("/news_list.html")
    public String newsListPage(Model model) {
    	
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
        return "html/news_list";
    }	   
    @RequestMapping("/news_detail.html")
    public String newsDetailPage(Model model) {
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
        return "html/news_detail";
    }	    
    @RequestMapping("/shiping.html")
    public String newsVideoPage(Model model) {
    	/*查询当前页的seo*/
    	Seo seo = seoService.getById((long) 9);
    	model.addAttribute("seo", seo);
    	/*友情链接*/
    	List<Flink> flinkList = flinkService.selectList();
    	model.addAttribute("flinkList", flinkList);
        return "html/shiping";
    }	    
    @RequestMapping("/login.html")
    public String loginPage() {
        return "html/user/login";
    }
/* 
    @PostMapping("/login.html")
    @ResponseBody
    public ReturnResult login(String username, String password) {
        return userService.login(username, password);
    }
*/
    
    @RequestMapping("/quanxian.html")
    public String quanxianPage() {
        return "html/quanxian";
    }    
}
