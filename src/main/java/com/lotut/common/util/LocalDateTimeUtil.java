/*
 * Project：ipsearch
 * File: LocalDateUtil.java
 * Date: 2018/9/4
 * Copyright (c) 2018. 合肥龙图腾信息技术有限公司版权所有.
 */

package com.lotut.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import org.springframework.util.StringUtils;

/**
 * @author Wang JunJie
 * @date 2018/9/4 14:24.
 */
public class LocalDateTimeUtil {

    /**
     * 默认使用系统当前时区
     */
    private static final ZoneId ZONE = ZoneId.systemDefault();

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";

    private static final String DATE_FORMAT_OTHER = "yyyy/MM/dd";

    /**
     * 将LocalDateTime格式化为yyyy-MM-dd HH:mm:ss格式
     */
    public static LocalDateTime formatLocalDateTime(LocalDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT_DEFAULT);
        String localTime = date.format(formatter);
        return LocalDateTime.parse(localTime, formatter);
    }

    /**
     * 将字符串日期装换为LocalDateTime：yyyy-MM-dd HH:mm:ss格式
     *
     * @param date
     * @return
     */
    public static LocalDateTime formatLocalDateTime(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT_DEFAULT);
        return LocalDateTime.parse(date, formatter);
    }

    /**
     * 将字符串日期安装指定格式转换为LocalDate格式
     *
     * @param date 字符串日期
     * @return
     */
    public static LocalDate parseStringToLocalDate(String date) {
        Integer ten = 10;
        if (date.length() <= ten) {
            date += " 00:00:00";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT_DEFAULT);
        return LocalDateTime.parse(date, formatter).toLocalDate();
    }

    /**
     * 将当前日期格式化为：yyyy-MM-dd HH:mm:ss的字符串形式
     *
     * @return
     */
    public static String formatLocalDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return formatter.format(LocalDateTime.now());
    }

    /**
     * 将日期格式化为指定pattern的字符串形式
     *
     * @param date    日期
     * @param pattern 格式化pattern
     * @return
     */
    public static String formatLocalDateTime(LocalDateTime date, String pattern) {
        if (date !=null){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            return formatter.format(date);
        }else{
            return  "";
        }

    }


    /**
     * 将Date转换成yyyy-MM-dd HH:mm:ss格式的LocalDateTime
     */
    public static LocalDateTime dateParseToLocalDateTime(Date d) {
        Instant instant = d.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZONE);
        return formatLocalDateTime(localDateTime);
    }

    /**
     * 系统当前的日期格式化
     */
    public static String dateByFormat() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        LocalDateTime now = LocalDateTime.now();
        return now.format(dateTimeFormatter);
    }

    /**
     * 根据指定格式格式化时间
     * @param pattern
     * @return
     */
    private static String formatByPattern(String pattern){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime now = LocalDateTime.now();
        return now.format(dateTimeFormatter);
    }

    public static String dateByFormatOther(){
        return formatByPattern(DATE_FORMAT_OTHER);
    }

    /**
     * 系统当前的时间格式化
     */
    public static String timeByFormat() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_DEFAULT);
        LocalDateTime now = LocalDateTime.now();
        return now.format(dateTimeFormatter);
    }

    /**
     * 将Date转换成LocalDateTime
     */
    public static LocalDateTime dateToLocalDateTime(Date d) {
        Instant instant = d.toInstant();
        return LocalDateTime.ofInstant(instant, ZONE);
    }

    /**
     * 将Date转换成指定格式的字符串形式
     *
     * @param d       Date日期
     * @param pattern 格式
     * @return
     */
    public static String dateToString(Date d, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(d);
    }
    
    /**
     * 将Date转换成指定格式的字符串形式
     *
     * @param d       Date日期
     * @param pattern 格式
     * @return
     */
    public static String dateToString(Date d) {
    	if(d != null) {
    		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            return sdf.format(d);
    	}
    	return null;        
    }

    /**
     * 将LocalDateTime转换成Date
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        if(localDateTime != null){
            Instant instant = localDateTime.atZone(ZONE).toInstant();
            return Date.from(instant);
        }
        return null;
    }

    /**
     * 获取指定日期n天前的日期
     *
     * @param date 日期
     * @param day  日期差
     * @return
     */
    public static String getDaysAgoDate(LocalDateTime date, int day) {
        LocalDateTime ago = date.minusDays(day);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return dateTimeFormatter.format(ago);
    }

    /**
     * 将 "2018-09-20" 形式的字符串转化为date;
     */
    public static Date stringToDate(String str) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sdf.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将字符串形式的日期减掉指定小时
     *
     * @param dateString 日期格式
     * @param hours      要减去的小时
     * @return
     */
    public static String minusHours(String dateString, int hours) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hours);
        Date time = calendar.getTime();
        String timeStr = formatter.format(time);
        return timeStr;
    }

    /**
     * 将字符串形式的日期减掉指定月份
     *
     * @param dateString 日期格式
     * @param month      要减去的月份
     * @return
     */
    public static String minusMonths(String dateString, int month) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, month);
        Date time = calendar.getTime();
        String timeStr = formatter.format(time);
        return timeStr;
    }

    /**
     * 将字符串形式的日期减掉指定年份
     *
     * @param dateString 日期格式
     * @param years      要减去的年份
     * @return
     */
    public static String minusYears(String dateString, int years) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, years);
        Date time = calendar.getTime();
        String timeStr = formatter.format(time);
        return timeStr;
    }

    /**
     * 计算日期差值
     *
     * @param left  日期
     * @param right 日期
     * @param type  差值类型：年（year）、月（month）、日（day）
     * @return 日期差值
     */
    public static int calTimeDif(String left, String right, String type) {
        LocalDate localDateLeft = parseStringToLocalDate(left);
        LocalDate localDateRight = parseStringToLocalDate(right);
        Period period = Period.between(localDateLeft, localDateRight);
        String year = "year";
        String month = "month";
        if (type.equals(year)) {
            return period.getYears();
        } else if (type.equals(month)) {
            return period.getMonths();
        }
        return period.getDays();
    }

    public static boolean isValidDate(String str) {
        boolean convertSuccess=true;
        // 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，
            // 比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(str);
        } catch (ParseException e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            convertSuccess=false;
        }
        return convertSuccess;
    }


    /**
     * 复合搜索入参验证
     * @param strValue  appDate publishDate
     * @return 是否是复合要求的参数
     */
    public static boolean isValid(String strValue) {
        String strConstant="to";
        String str2Constant="-";
        int arrayLength=2;
        if (StringUtils.isEmpty(strValue)) {
            return false;
        }
        if(!strValue.contains(strConstant)) {
            return false;
        }
        String[] dateArray=strValue.split("to");
        if (dateArray.length!=arrayLength){
            return false;
        }
        if (!str2Constant.equals(dateArray[0]) && !isValidDate(dateArray[0])){
            return false;
        }
        return str2Constant.equals(dateArray[1]) || isValidDate(dateArray[1]);
    }

    /**
     * 计算日期相差秒数
     *
     * @param left  日期
     * @param right 日期
     * @return 日期差值
     */
    public static long calTimeDifSeconds(LocalDateTime left, LocalDateTime right) {
        return Duration.between(left, right).getSeconds();
    }

    /**
     * 计算日期相差天数
     *
     * @param left  日期
     * @param right 日期
     * @return 日期差值
     */
    public static long calTimeDifDays(LocalDateTime left, LocalDateTime right) {
        return Duration.between(left, right).toDays();
    }

    /**
     * 将当前日期格式化为：yyyy-MM-dd的字符串形式
     *
     */
    public static String formatLocalDateTime2Str(LocalDateTime localDateTime) {
        if(localDateTime != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
            return formatter.format(localDateTime);
        }
        return null;
    }

    /**
     * 将yyyy-MM-dd的字符串日期形式格式化为2018年01月01日
     * @param date
     * @return
     */
    public static String formatCnDate(String date) {
        StringBuilder cnDate = new StringBuilder();
        cnDate.append(date.substring(0,4)).append("年");
        cnDate.append(date.substring(5,7)).append("月");
        cnDate.append(date.substring(8)).append("日");
        return cnDate.toString();
    }

    public static void main(String[] args) {
        System.out.println(formatCnDate("2018-12-11"));
    }

}
