package com.lotut.website.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lotut.common.constants.WebsiteConstants;
import com.lotut.common.dto.ReturnResult;
/*import com.lotut.news.domain.WebsiteNewsChannel;
import com.lotut.project.domain.Project;
import com.lotut.project.domain.ProjectSearchCondition;
import com.lotut.project.service.ProjectService;*/
import com.lotut.system.shiro.util.ShiroUtil;
import com.lotut.website.domain.Website;
import com.lotut.website.dto.BannerDto;
import com.lotut.website.dto.WebsiteDto;
import com.lotut.website.service.WebsiteService;


/**
 * 网站前端页面控制
 *
 * @author ShiQi
 * @date 2020/4/10
 */

@Controller
public class AdminController {
/*    private String prefix = "admin/news/";*/

    @Resource
    private WebsiteService websiteService;
    
/*    @Resource
    private ProjectService projectService;   */ 
	@RequestMapping("/admin/site.html")
    public String getSiteIndex(Model model) {
		model.addAttribute("user", ShiroUtil.getUser());
		return "admin/site";
	}	
	
	@RequestMapping("/admin/index")
    public String getUserIndex(Model model) {
		
		return "admin/index";
	}	

	@RequestMapping("admin/console")
    public String getConsole(Model model) {
		return "admin/console";
	}	
	@RequestMapping("admin/my_account")
	@RequiresRoles("sys_news_admin")
    public String getMyAccount(Model model) {
		return "admin/account/my_account";
	}		
	@RequestMapping("admin/my_site_list")
    public String getMySite(Model model) {
		//查询列表   
		List<Website> websiteList = websiteService.selectWebsiteByUserId(ShiroUtil.getUserId());
		model.addAttribute("websiteList", websiteList);
		return "admin/account/my_site";
	}
	/**
	 * 前端编辑页面
	 * @author ShiQi
	 * @date 2020/4/10
	 */
	@RequestMapping("admin/edit_site")
    public String editSite(Model model) {
		//编辑站点，包括新增和修改   如果是新增，验证是否有权限，如果是编辑验证是否在有效期   
		//前期只有新增
		return "admin/account/edit_site";
	}
	 
    
/*    @GetMapping("/login")
    public String getabc(Model model) {
		return "system/login";
	}*/




 

 
}
