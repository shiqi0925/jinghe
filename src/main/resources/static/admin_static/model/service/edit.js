layui.use(['form', 'layer', 'upload'], function () {
    var form = layui.form
    layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;
    var upload = layui.upload;
    chechAttrShow()
 
    // 监听checkbox
    form.on('checkbox(isAttr)', function (data) {
        console.log(data.value)
        console.log(data.elem.checked);
        if(data.elem.checked==true){
        	$('.attr-box').show();
        }else{
        	$('.attr-box').hide();
        }
    })

    //添加服务选项
    $(document).on('click', '.attr-add', function () {
    	var currAttrName = $('.attr-add-name').val();
    	if(currAttrName ==''){
    		layer.msg('请输入要添加的选项名');
    		return false;
    	}else{
            var html ='<tr>'+
	        '<td class="attr-name">'+currAttrName+'</td>'+
	        '<td><input type="text" name="title" lay-verify="title" autocomplete="off" class="layui-input attr-price"></td>'+
	        '<td class="attr-brief"><input type="text" name="title" lay-verify="title" autocomplete="off" class="layui-input"></td>'+
	        '<td>删除</td>'+
	      '</tr>';	     	
          $(".attr-tr-box").append(html);	
          $('.attr-add-name').val('');
    	}
        //form.render('select');//需要渲染一下
    });
    // 上传封面图片
    var uploadThumbnail = upload.render({
        elem: '#thumbnailBtn'
        , field: 'img'
        , size: 1048576
        , accept: 'images'
        , url: '/ueditor/uploadImg'
        , before: function (obj) {
        }
        , done: function (res) {
            if (res.state == 'SUCCESS') {
                layer.msg('上传成功');
                $('#thumbnailPath').val(res.url);
                $('#slideshowFlagTrue').attr("checked", "checked");
                $('#thumbnailImg').attr('src', res.url);
                $('#thumbnailImg').attr('width', '400px');
                $('#thumbnailImg').attr('height', '150px');
            } else {
                layer.msg('上传失败，请重试');
            }
        }
        , error: function () {
            //请求异常回调
        }
    });


    // 直接保存
    form.on("submit(save)", function (data) {
    	var isAttr;
    	if($('.is_attr').is(':checked')){
    		isAttr = 1;
    	}else{
    		isAttr = 0;
    	}
    	var attrList = getAttrList();
    	data.field.type=$('.typeId').val();
    	data.field.isAttr = isAttr;
    	data.field.attrList = attrList;
    	console.log(data.field);
        $.ajax({
            url: "/admin/serviceSave",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code == 0) {
                	layer.msg('修改成功！');
                } else {
 
                }
            },
        });
        return false;
    });
    function getAttrList(){
    	//遍历 .attr-tr-box  下面的tr 
    	var arrtList = new Array();	
    	$(' .attr-tr-box tr').each(function() {
    		arrtList.push({"attrName":$(this).find('.attr-name').html(),"attrPrice":$(this).find('.attr-price').val(),"attrBrief":$(this).find('.attr-brief input').val()});//专业能力数组赋值
    	})
    	console.log(arrtList);
    	return JSON.stringify(arrtList)	
    }
    $("#cancel_btn").click(function () {
        layer.closeAll("iframe");
        //刷新父页面
        parent.location.reload();
    });
    // 监听一级分类选择
    form.on('select(parentId)', function (data) {
        var value = data.value;
        $.ajax({
            url: "/admin/servicetype",
            type: "post",
            data: {parentType: value},
            success: function (res) {
                var data = res.data;
                if(data.length>0){
                	$('#typeId').empty();
                	$('#typeId').append("<option value=''>二级分类</option>");
                    for (var len = data.length, i = 0; i < len; i++) {
                        $('#typeId').append("<option value='" + data[i].typeId + "'>" + data[i].typeName + "</option>")
                    }
                    form.render();
                }
            }
        });
    })    
})