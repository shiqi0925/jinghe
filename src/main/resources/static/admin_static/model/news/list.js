  layui.config({
    base: '/admin_static/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'layer', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;
    
    //代理机构列表
    var tableIns = table.render({
        elem: '#newsList',
        url: '/admin/news/list',
        method: 'post',
        where: {
        	channelId: $('#type').val()
        },
        cellMinWidth: 95,
        page: true,
        height: "full-125",
        limits: [20, 50, 100],
        limit: 20,
        id: "newsList",
        cols: [[
            {align: 'center', type: 'numbers', title: '编号'},
            {field: 'title', title: '标题', align: "center"},
            {field: 'publishTime', title: '发布时间', align: 'center'},
/*            {field: 'status', title: '状态', align: 'center', templet: function (d) {
                    if(d.status) {
                        return '<input type="checkbox" value="'+d.recordId+'" name="switch" lay-skin="switch" lay-filter="defaultSwitch" lay-text="显示|隐藏" checked>';
                    }
                    return '<input type="checkbox" value="'+d.recordId+'" name="switch" lay-skin="switch" lay-filter="defaultSwitch" lay-text="显示|隐藏">';
                }},*/
            {title: '操作', minWidth: 150, templet: '#newsListBar', fixed: "right", align: "center"}
        ]]
    });

    // 刷新表格
    function reload() {
        table.reload("newsList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $("#keyword").val()
            }
        })
    }

    //搜索
    $("#keyword").on("click", function () {
        reload();
    });

    $(window).keydown(function (e) {
        var key = window.event ? e.keyCode : e.which;
        if (key.toString() == "13") {
            return false;
        }
    });

    //添加新闻
    $(".add_news_btn").on("click", function () {
        var index = layui.layer.open({
            title: "添加新闻",
            type: 2,
            content: "/admin/news/add/page?type="+$('#type').val(),
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回新闻列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        });
        layui.layer.full(index);
    });

    function editUser(edit) {
    	var recordId = edit.recordId;
        var index = layui.layer.open({
            title: "编辑用户",
            type: 2,
            content: "/admin/news/update?newsId="+recordId,
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回新闻列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        });
        layui.layer.full(index);
    }

    //列表操作
    table.on('tool(newsList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'edit') {
            editUser(data);
        } else if(layEvent === 'delete') {
            layer.confirm('确定删除该代理机构吗', {icon: 3, title: '提示信息'}, function (index, row) {
                $.post("/frequentProxy/delete", {
                    recordId: data.recordId
                }, function (data) {
                    if (data.code == 0) {
                        reload();
                        layer.close(index);
                    }
                })
            });
        }
    });

    //监听是否默认
    form.on('switch(defaultSwitch)', function (data) {
        var checked = data.elem.checked;
        if (checked) {
            $.ajax({
                url: "/admin/news/updateStatus.html",
                type: "post",
                data: {recordId: data.value, status: true},
                success: function (res) {
                    // reload();
                }
            });
        } else {
            $.ajax({
                url: "/admin/news/updateStatus.html",
                type: "post",
                data: {recordId: data.value, status: false},
                success: function (res) {
                    // reload();
                }
            });
        }
    });

});