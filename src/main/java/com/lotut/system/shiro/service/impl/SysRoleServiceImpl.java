package com.lotut.system.shiro.service.impl;

import com.lotut.system.shiro.dao.SysRoleMapper;
import com.lotut.system.shiro.dao.SysUserRoleMapper;
import com.lotut.system.shiro.domain.SysRole;
import com.lotut.system.shiro.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int save(SysRole sysRole) {
        return sysRoleMapper.save(sysRole);
    }

    @Override
    public List<SysRole> list() {
        return sysRoleMapper.list();
    }

    @Override
    public Set<String> selectRoleNamesByUserId(Long userId) {
        List<SysRole> sysRoles = sysRoleMapper.selectRolesByUserId(userId);
        Set<String> roleNameSet = new HashSet<>();
        sysRoles.stream().forEach(item -> {
            roleNameSet.add(item.getRoleName());
        });
        return roleNameSet;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteById(Long roleId) {
        sysRoleMapper.remove(roleId);
        sysUserRoleMapper.removeByRoleId(roleId);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysRole role) {
        return sysRoleMapper.update(role);
    }
}
