layui.use(['form', 'layer', 'table', 'laytpl','jquery'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;

    
    
    //监听提交
    form.on('submit(brand_buy)', function(data){
    	console.log("商标求购 表单提交")
        $.ajax({
            url: "/trade/saveBrandBuy",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        top.layer.close(index);
                        top.layer.msg("提交成功！");
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg("提交失败！");
                    }, 2000);
                }
            },
        });
    	
    	
    	
    	
    	
      return false;
    });
    
    form.on('submit(patent_buy)', function(data){
    	console.log("专利求购 表单提交")
      return false;
    });   
    
    form.on('submit(brand_sale)', function(data){
    	console.log("商标出售 表单提交")
      return false;
    });  
 
    form.on('submit(patent_sale)', function(data){
    	console.log("专利出售 表单提交")
      return false;
    });  
    
    //监听是否默认
    form.on('switch(defaultSwitch)', function (data) {
        var checked = data.elem.checked;
        console.log("data:"+data);
        $.ajax({
            url: "/admin/message/update_see_brand_sale.html",
            type: "post",
            data: {id: data.value, status: checked},
            success: function (res) {
                // reload();
            }
        });
       
    });

});