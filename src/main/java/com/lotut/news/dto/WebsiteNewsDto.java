package com.lotut.news.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author xwj
 * @date 2020/3/12
 */
public class WebsiteNewsDto {
    /**
     * 自增主键
     */
    private Long recordId;
    /**
     * 网站id
     */
    private Long userWebsiteId;
    /**
     * 二级频道id
     */
    @NotNull
    private Long secondChannelId;
    /**
     * 标题
     */
    @NotEmpty
    private String title;
    /**
     * 内容
     */
    @NotEmpty
    private String content;
    /**
     * 封面图片地址
     */
    private String thumbnailPath;
    /**
     * 是否轮播，0：否，1：是
     */
    @NotNull
    private Boolean slideshowFlag;
    /**
     * 作者
     */
    private String author;
    /**
     * 附件名称
     */
    private String attachmentName;
    /**
     * 附件文件路径
     */
    private String attachmentPath;

    /**
     * 自增主键
     */
    private Long viewCount;
    
    
    private boolean status;
    
    private String digest;
    private String abstractCon;

    public Long getUserWebsiteId() {
        return userWebsiteId;
    }

    public void setUserWebsiteId(Long userWebsiteId) {
        this.userWebsiteId = userWebsiteId;
    }

    public Long getSecondChannelId() {
        return secondChannelId;
    }

    public void setSecondChannelId(Long secondChannelId) {
        this.secondChannelId = secondChannelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public Boolean getSlideshowFlag() {
        return slideshowFlag;
    }

    public void setSlideshowFlag(Boolean slideshowFlag) {
        this.slideshowFlag = slideshowFlag;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getViewCount() {
		return viewCount;
	}

	public void setViewCount(Long viewCount) {
		this.viewCount = viewCount;
	}

	public String getAbstractCon() {
		return abstractCon;
	}

	public void setAbstractCon(String abstractCon) {
		this.abstractCon = abstractCon;
	}
	
    
}
