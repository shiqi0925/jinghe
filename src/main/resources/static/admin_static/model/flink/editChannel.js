layui.use(['form', 'layer', 'upload'], function () {
    var form = layui.form
    layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;
    var upload = layui.upload;


 
    // 直接保存
    form.on("submit(save)", function (data) {
 
        $.ajax({
            url: "/admin/news/addNewsChannel",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code == 0) {
                    setTimeout(function () {
                        /*top.layer.close(index);*/
                        top.layer.msg("保存成功！");
                        /*layer.closeAll("iframe");*/
                        //刷新父页面
                       /* parent.location.reload();*/
                    }, 2000);
                } else {
                    setTimeout(function () {
                        top.layer.msg("保存失败！");
                    }, 2000);
                }
            },
        });
        return false;
    });
    $("#cancel_btn").click(function () {
        layer.closeAll("iframe");
        //刷新父页面
        parent.location.reload();
    });
})