package com.lotut.news.dao;

import com.lotut.news.domain.WebsiteNewsContent;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface WebsiteNewsContentMapper {
    int deleteByPrimaryKey(Long newsId);

    int insert(WebsiteNewsContent record);

    int insertSelective(WebsiteNewsContent record);

    WebsiteNewsContent selectByPrimaryKey(Long newsId);

    int updateByPrimaryKeySelective(WebsiteNewsContent record);

    int updateByPrimaryKeyWithBLOBs(WebsiteNewsContent record);
}