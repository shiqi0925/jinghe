package com.lotut.news.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 用户网站新闻实体
 *
 * @author Administrator
 */
public class WebsiteNews implements Serializable {
    private static final long serialVersionUID = 4959485506060450737L;
    /**
     * 自增主键
     */
    private Long recordId;

    /**
     * 网站id
     */
    private Long userWebsiteId;
    /**
     * 一级频道id
     */
    private Long firstChannelId;
    /**
     * 一级频道名称
     */
    private String firstChannelName;
    /**
     * 二级频道id
     */
    private Long secondChannelId;
    /**
     * 二级频道名称
     */
    private String secondChannelName;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容id
     */
    private Long contentId;
    /**
     * 封面图片地址
     */
    private String thumbnailPath;
    /**
     * 是否轮播，0：否，1：是
     */
    private Boolean slideshowFlag;
    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date publishTime;
    /**
     * 状态，false：隐藏，true：显示
     */
    private Boolean status;
    /**
     * 浏览量
     */
    private Long viewCount;
    /**
     * 分享量
     */
    private Long shareCount;
    /**
     * 创建人
     */
    private Long createUser;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
    /**
     * 更新人
     */
    private Long updateUser;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
    
    
    private String abstractCon;
    private String path;

    private String fileName;
    
    private String content;

    public WebsiteNews() {
        super();
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getUserWebsiteId() {
        return userWebsiteId;
    }

    public void setUserWebsiteId(Long userWebsiteId) {
        this.userWebsiteId = userWebsiteId;
    }

    public Long getFirstChannelId() {
		return firstChannelId;
	}

	public void setFirstChannelId(Long firstChannelId) {
		this.firstChannelId = firstChannelId;
	}

	public String getFirstChannelName() {
		return firstChannelName;
	}

	public void setFirstChannelName(String firstChannelName) {
		this.firstChannelName = firstChannelName;
	}

	public Long getSecondChannelId() {
        return secondChannelId;
    }

    public void setSecondChannelId(Long secondChannelId) {
        this.secondChannelId = secondChannelId;
    }



	public String getSecondChannelName() {
		return secondChannelName;
	}

	public void setSecondChannelName(String secondChannelName) {
		this.secondChannelName = secondChannelName;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath == null ? null : thumbnailPath.trim();
    }

    public Boolean getSlideshowFlag() {
        return slideshowFlag;
    }

    public void setSlideshowFlag(Boolean slideshowFlag) {
        this.slideshowFlag = slideshowFlag;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public String getPublishTimeStr(){
        if(publishTime == null) return null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(publishTime);
    }

    public String getPublishYear(){
        if(publishTime == null) return null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return sdf.format(publishTime).substring(2,4);
    }

    public String getPublishMonth(){
        if(publishTime == null) return null;
        SimpleDateFormat sdf = new SimpleDateFormat("MM月");
        return sdf.format(publishTime);
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getViewCount() {
        return viewCount;
    }

    public void setViewCount(Long viewCount) {
        this.viewCount = viewCount;
    }

    public Long getShareCount() {
        return shareCount;
    }

    public void setShareCount(Long shareCount) {
        this.shareCount = shareCount;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAbstractCon() {
		return abstractCon;
	}

	public void setAbstractCon(String abstractCon) {
		this.abstractCon = abstractCon;
	}

}