  layui.config({
    base: '/admin_static/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'layer', 'table', 'laytpl', 'upload'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        upload = layui.upload,
        table = layui.table;

     
    // 直接保存
    form.on("submit(editDetail)", function (data) {
        $.ajax({
            url: "/admin/banner_save.html",
            type: "post",
            data: data.field,
            success: function (res) {
                if (res.code == 0) {
                	top.layer.msg("保存成功！");
                } else {
                    setTimeout(function () {
                        top.layer.msg("保存失败");
                    }, 2000);
                }
            },
        });
        return false;
    });
    // 上传封面图片
    var uploadPic = upload.render({
        elem: '.imgBtn'
        , field: 'img'
        , size: 1048576
        , accept: 'images'
        , url: '/upload_img.html'
        , before: function (obj) {
        }
        , done: function (res) {
            if (res.state == 'SUCCESS') {
                layer.msg('上传成功');
                $('#picPath').val(res.url);
                $('#slideshowFlagTrue').attr("checked", "checked");
                $('.upload-preview-img').attr('src', res.url);
            } else {
                layer.msg('上传失败，请重试');
            }
        }
        , error: function () {
            //请求异常回调
        }
    });    


});