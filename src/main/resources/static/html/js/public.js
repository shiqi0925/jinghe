//验证
var Valid = function(){
    var methods = {};
    methods.isUrl = function(str){//验证域名合法
        return /^(http:\/\/)?(www\.)?.+\..+$/.test(str);
    };

    methods.isEmail = function(str){//验证邮箱
        return /^.+@.+\.[\w]{2,4}$/.test(str);
    };

    methods.isMobile = function(str){//验证手机号码
        return /^1[3|4|5|8][0-9]\d{8}$/.test(str);
    };

    methods.isCode6 = function(str){//验证手机验证码，6位数字
        return /^\d{6}$/.test(str);
    };

    methods.isInt = function(str){//验证正整数
        return /^\d+$/.test(str);
    };

    methods.isNumber = function(str){//验证是否是数字（包括负数和小数）
        return /^(-)?\d+(\.\d+)?$/.test(str);
    };

    methods.isCardNo = function(str){//验证身份证号码
        var len = str.length;
        if(len == 15){
            return /^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{2})(\w)$/.test(str);
        }else if(len == 18){
            return /^(\d{6})(19|20)(\d{2})((0\d)|(1[0-2]))(([0-2]\d)|(3[0-1]))(\d{3})(\w)$/.test(str);
        }else{
            return false;
        }
    };

    methods.isCnChar = function(str){//验证中文
        return /^[\u4e00-\u9fa5]+$/.test(str);
    };

    methods.isEqualto = function(str1,str2){//验证一致性
        return str1 === str2?true:false;
    };

    return methods;
}();


//placeholder浏览器兼容(添加label方式)
var placeHolder = (function(){
    var methods = {};
    methods.init = function(){
        var $ele = $("input[data-placeholder]");
        $ele.each(function(){
            var $this = $(this),
                id = this.id,
                placeholder = $(this).data("placeholder");

            var fontSize = $this.css("font-size"),
                lineH = $this.css("height");

            if($this.parent().attr("id")){
                var pid = $this.parent().attr("id");
                if(pid.indexOf("wrap_") == 0){//表示已经添加过父元素了
                    $this.parent().find("label.place-label").remove();
                    $(this).parent().removeAttr("id");
                }
            }

            $this.wrap('<div id="wrap_'+id+'"></div>');
            var $parent = $("#wrap_"+id);
            $parent.css("position","relative");
            var options = {
                "position"      : "absolute",
                "left"          : "10px",
                "top"           : "0",
                "color"         : "#999",
                "font-weight"   : "normal",
                "margin-bottom" : "0",
                "font-size"     : fontSize,
                "line-height"   : lineH,
                "cursor":"auto"
            };

            if($this.data("options")){
                $.extend(true, options, eval('(' + $this.data("options") + ')'));
            }

            var $label = $('<label class="place-label" for="'+id+'">'+placeholder+'</label>');
            $label.css(options);
            $parent.append($label);

            if($this.val() == ""){
                $label.show();
            }else{
                $label.hide();
            }

            $this.on("focus blur keyup",function(){
                if($this.val() == ""){
                    $label.show();
                }else{
                    $label.hide();
                }
            }).on("keydown",function(){
                $label.hide();
            })
        })
    }

    methods.init();
    return methods;
})();


//弹出框
var Pop = (function(){
    var methods = {},$obj,$objalert,popScrollTop;//popScrollTop是刚弹框时候的window滚动高度
    methods.open = function(id){
        $obj = $("#"+id);
        $("body").append("<div id='bgframe' style='display:none;'></div>");
        $("#bgframe").show();
        popScrollTop = $(window).scrollTop();//就open方法可能会出现弹窗超出屏幕高度的情况
        resetpop($obj);
    }
    methods.openalert = function(id){
        $objalert = $("#"+id);
        $("body").append("<div id='bgalert' style='display:none;'></div>");
        $("#bgalert").show();
        resetpop($objalert);
    }

    methods.open2 = function(id){//特殊需求，弹出框不用黑色背景遮罩
        $obj = $("#"+id);
        popScrollTop = $(window).scrollTop();//就open方法可能会出现弹窗超出屏幕高度的情况
        resetpop($obj);
    }

    methods.exit = function(){//open方法的关闭
        $obj.hide();
        $("#bgframe").remove();
    }

    methods.exitalert = function(){//alert和confirm方法的关闭
        $objalert.hide();
        $("#bgalert").remove();
    }

    methods.alert = function(){//alert和confirm的弹窗层级要比open的高
        var content = arguments[0];
        var title = arguments[1]?arguments[1]:"&nbsp;";
        var type = "";
        if(arguments[2]){
            switch(arguments[2]){
                case 0:
                    type = "";
                    break;
                case 1:
                    type = "ialer";
                    break;
                case 2:
                    type = "isuc";
                    break;
                default:
                    type = "";
                    break;
            }
        }
        if(!$("#alert").length>0){//如果不存在
            var alertStr = '<div class="upwinalert upwin1 upalert" id="alert" style="display: none;">'+
                '<h3><font>&nbsp;</font>'+
                '<a href="javascript:;" onclick="Pop.exitalert();"><i class="ico-close"></i></a>'+
                '</h3>'+
                '<div class="up-content ta-c">'+
                '<div class="'+type+' mt30"></div>'+
                '<p>&nbsp;</p>'+
                '<a href="javascript:;" class="pub-btn3-b w100 mb30 pub-alert" onclick="Pop.exitalert();">确定</a>'+
                '</div>'+
                '</div>';
            $("body").append(alertStr);
        }
        $("#alert").find("font").html(title);
        $("#alert").find("p").html(content);
        methods.openalert("alert");
        if(typeof(arguments[3])=="function"){
            var callback = arguments[3];
            $("#alert").find(".pub-alert").off("click").one("click",function(){
                callback();
            })
        }
        if(typeof(arguments[4])=="function"){//点击取消回调函数
            var callback2 = arguments[4];
            $("#alert").find("a:first").off("click").on("click",function(){
                callback2();
            })
        }
    }

    methods.confirm = function(str,callback){//alert和confirm的弹窗层级要比open的高
        var content = str;
        if(!$("#confirm").length>0){//如果不存在
            var confirmStr = '<div class="upwinalert upwin1 upalert" id="confirm" style="display: none;">'+
                '<h3><font>&nbsp;</font>'+
                '<a href="javascript:;" onclick="Pop.exitalert();"><i class="ico-close"></i></a>'+
                '</h3>'+
                '<div class="up-content ta-c">'+
                '<div class="iconfir mt30"></div>'+
                '<p>&nbsp;</p>'+
                '<div>'+
                '<a href="javascript:;" class="pub-btn3-b w100 mb30 mr20 pub-confirm" onclick="Pop.exitalert();">确定</a>'+
                '<a href="javascript:;" class="pub-btn3-b w100 mb30" onclick="Pop.exitalert();">取消</a>'+
                '</div>'+
                '</div>'+
                '</div>';
            $("body").append(confirmStr);
        }
        $("#confirm").find("p").html(content);
        methods.openalert("confirm");
        if(typeof(callback)=="function"){
            $("#confirm").find(".pub-confirm").off("click").on("click",function(){
                callback();
            })
        }
    }

    function resetpop($ob){
        var curDivHeight = $ob.outerHeight(true);
        var browserHeight = $(window).height();
        var posInfo = getPos($ob);
        var posLeft = posInfo.left;
        var posTop = posInfo.top;
        if(curDivHeight > browserHeight){//如果弹框高度超出浏览器高度，弹框顶格
            posTop = popScrollTop;
        }
        $ob.css({
            "left" : posLeft,
            "top"  : posTop
        }).show();
    }

    function getPos($o){
        //浏览器可视区域宽度&高度
        var windowWidth = $(window).width();//注意：这里是方法，而非属性
        var windowHeight = $(window).height();

        //当前的浏览器滚动条偏移值
        var scrollLeft = $(window).scrollLeft();
        var scrollTop = $(window).scrollTop();

        //外部宽度（默认包括补白和边框）,计算边距在内
        var curDivWidth = $o.outerWidth(true);
        var curDivHeight = $o.outerHeight(true);

        return {
            "left" : (windowWidth - curDivWidth) / 2 + scrollLeft,
            "top"  : (windowHeight - curDivHeight) / 2 + scrollTop
        }
    }

    //浏览器大小改变事件
    $(window).resize(function(){
        //如果弹出框可见，则重新定位
        if ($obj && $obj.is(":visible")){
            resetpop($obj);
        }

        if ($objalert && $objalert.is(":visible")){
            resetpop($objalert);
        }
    });

    //浏览器滚动改变事件
    $(window).scroll(function(){
        //如果弹出框可见，则重新定位
        if ($obj && $obj.is(":visible")){
            resetpop($obj);
        }

        if ($objalert && $objalert.is(":visible")){
            resetpop($objalert);
        }
    });

    return methods;

})();


//倒计时发送
function sendCode(id,seconds){
    var $this = $("#"+id);
    var wait = seconds;
    $this.addClass("forbid").attr("disabled","disabled");
    time();

    function time(){
        wait--;
        if(wait<0){
            $this.removeClass("forbid").removeAttr("disabled").val("获取验证码");
            return false;
        }
        $this.val("发送成功("+wait+")");

        setTimeout(function(){
            time();
        },1000);
    }
}


//流程图控制连线长度
function flowPointLine(id){
    var $obj = $("#"+id);
    var otherW = arguments[1]?arguments[1]:0;
    $obj.find("li").not(".last").each(function(){
        var width = $(this).data("width");
        $(this).css("margin-right",width+"px");
        $(this).find("em").css({
            "width":(width+otherW)+"px",
            "margin-left":(-otherW/2)+"px"
        });
    })
}


//切换元素内容
function toggleText(id,str1,str2){
    $("#"+id).html($("#"+id).html()==str1?str2:str1);
}


//拍卖页面倒计时
function countDown(id,leftTime,callback){
    var $id = $("#"+id),runtime = 0,wait = leftTime/1000;
    time();

    function time(){
        var str = "";
        wait--;
        if(wait<0){
            if(typeof(callback)=="function"){
                callback();
                return false;
            }else{
                return false;
            }
        }
        var nD=Math.floor(wait/(60*60*24));
        var nH=Math.floor(wait/(60*60))%24;
        var nM=Math.floor(wait/(60)) % 60;
        var nS=wait % 60;

        if(nD){
            str+= " <font class='col-ff0000'>" + nD + "</font>" + "天";
        }
        if(nD || nH){
            str+= " <font class='col-ff0000'>" + nH + "</font>" + "小时";
        }
        if(nD || nH || nM){
            str+= " <font class='col-ff0000'>" + nM + "</font>" + "分";
        }
        str+= " <font class='col-ff0000'>" + nS + "</font>" + "秒";
        $id.html(str);

        setTimeout(function(){
            time();
        },1000)
    }
}


//首页倒计时
function IndexcountDown(id,leftTime,callback){
    var $id = $("#"+id),runtime = 0,wait = leftTime/1000;
    time();

    function time(){
        var str = "";
        wait--;
        if(wait<0){
            if(typeof(callback)=="function"){
                callback();
                return false;
            }else{
                return false;
            }
        }
        var nD=Math.floor(wait/(60*60*24));
        var nH=Math.floor(wait/(60*60))%24;
        var nM=Math.floor(wait/(60)) % 60;
        var nS=wait % 60;
        if(nD){
            str+= "<font class='col-ff0000'>" + nD + "</font>" + "天";
        }
        if(nD || nH){
            str+= "<font class='col-ff0000'>" + nH + "</font>" + "时";
        }
        if(nD || nH || nM){
            str+= "<font class='col-ff0000'>" + nM + "</font>" + "分";
        }
        if(nD == 0){
            str+= "<font class='col-ff0000'>" + nS + "</font>" + "秒";
        }
        $id.html(str);
        setTimeout(function(){
            time();
        },1000)
    }
}

//拍卖页面加价步长控制
function auctionStep(){

    allowStep();

    $("a[data-step]").on("click",function(){
        var $this = $(this);
        var arrow = $this.data("step");
        var $input = $("#"+$this.data("for"));
        if(arrow == "up"){//如果向上加
            $input.val(Number($input.val())+Number($this.data("range")));
        }else{
            if(!$this.data("min") || $input.val()>$this.data("min")){
                $input.val(Number($input.val())-Number($this.data("range")));
            }
        }
        allowStep();
    })

    function allowStep(){
        $("a[data-step]").each(function(){
            var $_this = $(this);
            if($_this.data("min") && $_this.data("min") == $("#"+$_this.data("for")).val()){
                $_this.css("cursor","not-allowed");
            }else{
                $_this.css("cursor","pointer");
            }
        })
    }

}


//表格全选
function checkAll(boxId,allId1,allId2){
    var $box = $("#"+boxId),$allCheck1 = $("#"+allId1),$allCheck2 = $("#"+allId2);
    var $checkList = $box.find("input[type=checkbox]").not("#"+allId1+",#"+allId2+",:disabled");
    $allCheck1.on("click",function(){
        $checkList.prop("checked",this.checked);
        $allCheck2.prop("checked",this.checked);
    })
    $allCheck2.on("click",function(){
        $checkList.prop("checked",this.checked);
        $allCheck1.prop("checked",this.checked);
    })
    $checkList.on("click",function(){
        if($checkList.length == $checkList.filter(":checked").length){
            $("#"+allId1+",#"+allId2).prop("checked",true);
        }else{
            $("#"+allId1+",#"+allId2).prop("checked",false);
        }

    })
}

//限制下拉框高度
function limitSelect(){
    var cls = arguments[0]?arguments[0]:"limitSelect";
    var $limitSelect = $("."+cls);
    $limitSelect.each(function(){
        var $select1 = $(this).find(".select1");
        var $select2 = $(this).find(".select2");
        $select1.on("click",function(){
            if($select2.is(":visible")){
                $select2.hide();
            }else{
                $select2.show();
            }
        })
        $select2.on("click",function(){
            $select1.val($(this).val());
            $(this).hide();
        })
    })
}


function onlyInt(obj){//只能输入正整数
    var $this = $(obj);
    $this.val(isNaN(parseInt(obj.value))?"":parseInt(obj.value));
}

//图片懒加载
function lazyLoading(){
    $("img[data-src]").slice(0,10).each(function(){
        var $this = $(this);
        if(($(window).scrollTop()+$(window).height())>$this.offset().top){
            $this.attr("src",$this.data("src")).removeAttr("data-src");
        }
    })
}

//仿marquee走马灯效果
function showMarquee(id,time){
    var wait = time?time:25;
    var scroll = document.getElementById(id);
    var scrollIndex;
    scrollIndex = setInterval(function(){
        if(scroll.scrollTop == scroll.scrollHeight-scroll.clientHeight){
            scroll.scrollTop = 1;
        }
        scroll.scrollTop+=1;
    },wait);
    $(scroll).on("mouseover",function(){
        clearInterval(scrollIndex);
    }).on("mouseout",function(){
        scrollIndex = setInterval(function(){
            if(scroll.scrollTop == scroll.scrollHeight-scroll.clientHeight){
                scroll.scrollTop = 1;
            }
            scroll.scrollTop+=1;
        },wait);
    })

}


//顶部下拉
$(function(){
    var $inp = $('input[name="input_brand"]');
    $inp.bind('keydown',function(e){
        var key = e.which;
        if (key == 13){
            $('.pop_window').fadeIn();
            var content=$("#input_brand").val();
            zscqfwIndex.inputPlaceholder('pop_name',content);
        }
    });
});
$(function(){

    // 搜索框里的placeholder
    zscqfwIndex.inputPlaceholder('input_brand','请输入商标名称');
    zscqfwIndex.inputPlaceholder('pop_name','请输入注册名称');
    zscqfwIndex.inputPlaceholder('contact_person','请输入联系人');
    zscqfwIndex.inputPlaceholder('contact_phone','请输入您的联系手机号');

    // 移动到li上面改变类名
    $('.brang_register_detail li').hover(function(){
        var _this = $(this);
        zscqfwIndex.changeName(_this,'on');
    });

    // 点击专利li改变下拉框
    $('.patent_items li .frame_title').click(function(){
        var _this = $(this).parent();
        zscqfwIndex.changeName(_this,'on');
    });

    $('.team .team_mid .all_member li').mouseenter(function(){
        $(this).find('.cell_info_show').fadeIn(200);
        $(this).find('.dot').dotdotdot({
            wrap: 'letter'
        });
    }).mouseleave(function(){
        $(this).find('.cell_info_show').fadeOut(200);
    });
    /**
     * 需求提交
     */
    $("#check_btn").click(function () {
        var style=$("#type").val();
        var describe=$("#pop_name").val();
        var name=$("#contact_person").val();
        var mobile= $("#contact_phone").val();
        $.post('ajax/mobile_add',{mobile:mobile,style:style,name:name,describe:describe},function(data){
            if(data.status == 1){
                layer.msg(data.msg,{icon: 1,time:2000},function(){
                    location.reload();
                    $('.pop_window').hide();
                });
            }else{
                layer.msg(data.msg,{icon: 0,time:2000});
            }
        })
    });
    // 获取焦点时候的css变化


    var selectorAll = [
        {
            name:$('.down #pop_name'),
            text:'请填写名称'
        },
        {
            name:$('.down #contact_person'),
            text:'请填写联系人'
        },
        {
            name:$('.down #contact_phone'),
            text:'请填写联系号码'
        }];
    selectorAll.forEach(function(current){
        current.name.focus(function(event) {
            var length = parseInt($('.addP').length);
            if(length == 0){
                zscqfwIndex.focus(current.name.parent(),current.text);
            }

        }).blur(function(){
            current.name.parent().removeClass('on');
            current.name.parent().next('.addP').remove();
        });
    });
    // 关闭弹窗
    $('.pop_window .brand_frenn_check .close_btn').click(function(){
        $('.pop_window').hide();
    });

    // 点击按钮打开弹窗
    $('.search_frame .free_check').click(function(){
        $('.pop_window').fadeIn();
        var content=$("#input_brand").val();
        if(content==''|| content==null){
            content=$("#input_brand").attr('placeholder');
            zscqfwIndex.inputPlaceholder('pop_name',content);
        }else{
            $("#pop_name").val(content);
        }
        //
        // $("#input_brand").val(content);
    });

    // 在hover到li上面重新加载dotdotdot.js的

});

function get_type(type,obj,sign) {
    var name=$(obj).html();
    $('.pop_select_sort').html(name);
    $('#type').val(type);
    if(sign==1){
        if(type==1){
            $('.pop_title').html("商标免费查询");
            $('.free_check').html('免费查询');
            zscqfwIndex.inputPlaceholder('input_brand','请输入商标名称');
        }
        if(type==2){
            $('.pop_title').html("专利免费检索");
            $('.free_check').html('免费查询');
            zscqfwIndex.inputPlaceholder('input_brand','请输入专利名称');
        }
    }else{
        if(type==1){
            $('.pop_title').html("商标免费查询");
            $('.free_check').html('免费查询');
            zscqfwIndex.inputPlaceholder('pop_name','请输入商标名称');
        }
        if(type==2){
            $('.pop_title').html("专利免费检索");
            $('.free_check').html('免费查询');
            zscqfwIndex.inputPlaceholder('pop_name','请输入专利名称');
        }

    };


    //标志切换
    var biaozhi='/static/images/';
    if(type==1){
        biaozhi=biaozhi+'icon_search_1.png';
    }else if(type==2){
        biaozhi=biaozhi+'icon_search_2.png';
    };
    $("#biaozhi").attr("src",biaozhi);


}

/* 创建一个构造函数 */
function ZscqfwIndex(){};

/* placeholder兼容 */
ZscqfwIndex.prototype.inputPlaceholder = function (id, text) {
    if($('#' + id).attr('readonly')){
        if ($('#' + id).val() == "") {
            $('#' + id).css({"color": "#bbbbbb",'font-size':'14px'});
            // $('#' + id).val(text);
            $('#' + id).attr('placeholder',text);
        }
    }else{
        // console.log(document.getElementById(id).tagName)
        if ($('#' + id).val() == "") {
            $('#' + id).css({"color": "#bbbbbb",'font-size':'14px'});
            // $('#' + id).val(text);
            $('#' + id).attr('placeholder',text);
        }else{
            $('#' + id).css({"color": "#bbbbbb",'font-size':'14px'});
            // $('#' + id).val(text);
            $('#' + id).attr('placeholder',text);
        }
        $('#' + id).focus(function () {
            $('#' + id).css({"color": "#000000"}).parents(".searchBlock").css({"border-color": "#3c89ff", "box-shadow": "0px 10px 24px 0px rgba(60, 137, 255, 0.1)"});
            if ($(this).val() == text) {
                $(this).val("");
            }
        });

        $('#' + id).blur(function () {
            $('#' + id).parents(".searchBlock").css({"border-color": "#ddd", "box-shadow": "0px 10px 24px 0px rgba(0, 0, 0, 0.06)"});
            if ($(this).val() == "") {
                // $(this).val(text);
                $(this).attr('placeholder',text);
                $('#' + id).css({"color": "#bbbbbb",'font-size':'14px'});
            }else {
                // if (value == defaultValue) {
                // 	$('#' + id).css({"color": "#bbbbbb", 'font-size': '14px'});
                // } else {
                $('#' + id).css({"color": "#333", 'font-size': '14px'});
                // }
            }
        });
    }


};


// 切换class
ZscqfwIndex.prototype.changeName = function(curname,changename){
    $(curname).addClass(changename).siblings().removeClass(changename);
};

// 弹窗框的input获取焦点
ZscqfwIndex.prototype.focus = function(selector,text){
    $(selector).addClass('on');
    add_p(selector,text)
}

// 获取焦点后往后面加个p标签
var add_p = function(selector,text){
    // var addP = document.createElement('p');
    // console.log(addP)
    // addP.classList.add("addP");
    // addP.innerHTML = text;
    var insertHtml='<p class="addP">' + text + '</p>'
    selector.after(insertHtml);
    // selector.insertAfter('.addP')
}

var zscqfwIndex = new ZscqfwIndex();





// +----------------------------------------------------------------------
// | Desc:构造方法
// +----------------------------------------------------------------------
function Func(){}



// +----------------------------------------------------------------------
// | Desc:滚动楼层选中
// +----------------------------------------------------------------------


// +----------------------------------------------------------------------
// | Desc:点击楼层选中
// +----------------------------------------------------------------------
Func.prototype.clickScroll=function(eq){
    var contentH=$(".block"+eq).offset().top-199;
    $('html,body').animate({
        scrollTop:contentH
    }, 800);
}



// +----------------------------------------------------------------------
// | Desc:回到顶部
// +----------------------------------------------------------------------
Func.prototype.top=function(){
    $('html,body').animate({
        scrollTop:0
    }, 800);
}


// +----------------------------------------------------------------------
// | Desc:实例化
// +----------------------------------------------------------------------
var func = new Func();






//下拉美化
;(function($){
    //默认参数
    var defaluts = {
        select: "select",
        select_text: "select_text",
        select_ul: "select_ul"
    };
    $.fn.extend({
        "select": function(options){
            var opts = $.extend({}, defaluts, options);
            return this.each(function(){
                var $this = $(this);
                //模拟下拉列表
                if ($this.data("value") !== undefined && $this.data("value") !== '') {
                    $this.val($this.data("value"));
                }
                var _html = [];
                _html.push("<div class=\"" + $this.attr('class') + "\">");
                _html.push("<div class=\""+ opts.select_text +"\">" + $this.find(":selected").text() + "</div>");
                _html.push("<ul class=\""+ opts.select_ul +"\">");
                $this.children("option").each(function () {
                    var option = $(this);
                    if($this.data("value") == option.val()){
                        _html.push("<li class=\"cur\" data-value=\"" + option.val() + "\">" + option.text() + "</li>");
                    }else{
                        _html.push("<li data-value=\"" + option.val() + "\">" + option.text() + "</li>");
                    }
                });
                _html.push("</ul>");
                _html.push("</div>");
                var select = $(_html.join(""));
                var select_text = select.find("." + opts.select_text);
                var select_ul = select.find("." + opts.select_ul);
                $this.after(select);
                $this.hide();
                //下拉列表操作
                select.click(function (event) {
                    $(this).find("." + opts.select_ul).slideToggle().end().siblings("div." + opts.select).find("." + opts.select_ul).slideUp();
                    event.stopPropagation();
                });
                $("body").click(function () {
                    select_ul.slideUp();
                });
                select_ul.on("click", "li", function () {
                    var li = $(this);
                    var val = li.addClass("cur").siblings("li").removeClass("cur").end().data("value").toString();
                    if (val !== $this.val()) {
                        select_text.text(li.text());
                        $this.val(val);
                        $this.attr("data-value",val);
                    }
                });
            });
        }
    });
})(jQuery);
