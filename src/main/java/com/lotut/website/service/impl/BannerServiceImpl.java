package com.lotut.website.service.impl;

import com.lotut.website.domain.Banner;
import com.lotut.common.dto.Page;
import com.lotut.website.dto.BannerDto;
import com.lotut.website.dto.BannerSearchCondition;
import com.lotut.website.dao.BannerMapper;
import com.lotut.website.service.BannerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerMapper bannerMapper;

    @Override
	public Page<Banner> getListByPage(BannerSearchCondition condition) {
        Page<Banner> page = new Page<>(condition.getPage(), condition.getLimit());
        Long count = bannerMapper.countBySearchCondition(condition);
        List<Banner> list = bannerMapper.listByPage(condition);
        page.setTotalCount(count.intValue());
        page.setList(list);
        return page;
	}

	@Override
	public List<Banner> selectList(BannerSearchCondition condition) {
		return bannerMapper.listByPage(condition);
	}

	@Override
	public Banner selectByPrimaryKey(Long id, Long websiteId) {
		return bannerMapper.selectByPrimaryKey(id,websiteId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean save(BannerDto detailDto) {
		Banner detail = new Banner();
        BeanUtils.copyProperties(detailDto, detail);
        if(detail.getId() != null) {
        	bannerMapper.updateSelective(detail);
        }else {
        	bannerMapper.insertSelective(detail);
        }
    	
        return true;
	}
	
}

