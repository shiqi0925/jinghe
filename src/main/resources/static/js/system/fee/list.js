layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    laydate.render({
        elem: '#startDeadLine',
        type: 'date',
        format: 'yyyy-MM-dd',
    });
    laydate.render({
        elem: '#endDeadLine',
        type: 'date',
        format: 'yyyy-MM-dd',
    });

    // 标签表格
    table.render({
        elem: '#annualFeeList',
        url: '/patent/annual/fee/list.html',
        method: 'post',
        where: {statisticsStatus: $('#navigationStatisticsStatus').val()},
        height: "full-176",
        id: "annualFeeList",
        page: true,
        limits: [10, 20, 50, 100],
        limit: 10,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
            {field: 'appPerson', title: '申请人/共享人', align: "center", templet: '#appPersonBar'},
            {field: 'appDate', title: '申请日/授权日', width: 120, align: "center", templet: '#appDateBar'},
            {
                field: 'deadLine', title: '年度/提醒日', width: 140, align: "center", templet: function (d) {
                    return '第' + d.annual + '年' + '<br>' + d.deadLine;
                }
            },
            {
                field: 'statusStr', title: '缴费状态', width: 90, align: "center", templet: function (d) {
                    return '<div style="margin-top:18px;">' + d.statusStr + '</div>';
                }
            },
            {field: 'option', title: '操作', width: 200, align: "center", templet: '#annualFeeListBar'}
        ]]
    });

    // 构造搜索条件
    function buildSearchCondition() {
        var condition = {
            keyword: $('#keyword').val(),
            patentType: $('#patentType').val(),
            status: $('#status').val(),
            startDeadLine: $('#startDeadLine').val(),
            endDeadLine: $('#endDeadLine').val(),
        }
        return condition;
    }

    // 刷新表格
    function reload() {
        $('#statisticsStatus').val('');
        $('.status').removeClass("hover");
        table.render({
            elem: '#annualFeeList',
            url: '/patent/annual/fee/list.html',
            method: 'post',
            where: buildSearchCondition(),
            height: "full-176",
            id: "annualFeeList",
            page: true,
            limits: [10, 20, 50, 100],
            limit: 10,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
                {field: 'appPerson', title: '申请人/共享人', align: "center", templet: '#appPersonBar'},
                {field: 'appDate', title: '申请日/授权日', width: 120, align: "center", templet: '#appDateBar'},
                {
                    field: 'deadLine', title: '年度/提醒日', width: 140, align: "center", templet: function (d) {
                        return '第' + d.annual + '年' + '<br>' + d.deadLine;
                    }
                },
                {
                    field: 'statusStr', title: '缴费状态', width: 90, align: "center", templet: function (d) {
                        return '<div style="margin-top:18px;">' + d.statusStr + '</div>';
                    }
                },
                {field: 'option', title: '操作', align: "center", templet: '#annualFeeListBar'}
            ]]
        });
    }

    // 根据统计状态刷新表格
    function reloadByStatisticsStatus(statisticsStatus) {
        $('#statisticsStatus').val(statisticsStatus);
        $("#keyword").val('');
        $("#patentType").val('');
        $("#status").val('');
        $("#startDeadLine").val('');
        $("#endDeadLine").val('');
        form.render();
        table.render({
            elem: '#annualFeeList',
            url: '/patent/annual/fee/list.html',
            method: 'post',
            where: {statisticsStatus: statisticsStatus},
            height: "full-176",
            id: "annualFeeList",
            page: true,
            limits: [10, 20, 50, 100],
            limit: 10,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', align: "center", templet: '#appNoBar'},
                {field: 'appPerson', title: '申请人/共享人', align: "center", templet: '#appPersonBar'},
                {field: 'appDate', title: '申请日/授权日', width: 120, align: "center", templet: '#appDateBar'},
                {
                    field: 'deadLine', title: '年度/提醒日', width: 140, align: "center", templet: function (d) {
                        return '第' + d.annual + '年' + '<br>' + d.deadLine;
                    }
                },
                {
                    field: 'statusStr', title: '缴费状态', width: 90, align: "center", templet: function (d) {
                        return '<div style="margin-top:18px;">' + d.statusStr + '</div>';
                    }
                },
                {field: 'option', title: '操作', align: "center", templet: '#annualFeeListBar'}
            ]]
        });
    }

    //搜索
    $(".search_btn").on("click", function () {
        reload();
    });

    $("#keyword").keypress(function (e) {
        if (e.which == 13) {
            reload();
        }
    });

    // 点击统计
    $(".status").on("click", function () {
        $('.status').removeClass("hover");
        $(this).addClass('hover');
        var status = $(this).attr("status");
        reloadByStatisticsStatus(status);
    });

    //列表操作
    table.on('tool(annualFeeList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'payed') {
            $.post("/patent/annual/fee/payed.html", {
                id: data.id
            }, function (data) {
                if (data.code == 0) {
                    location.reload();
                } else {
                    layer.msg(data.msg);
                }
            })
        }
        if (layEvent === 'not-pay') {
            $.post("/patent/annual/fee/notPay.html", {
                id: data.id
            }, function (data) {
                if (data.code == 0) {
                    location.reload();
                } else {
                    layer.msg(data.msg);
                }
            })
        }
        if (layEvent === 'abandon') {
            layer.confirm('确定置为放弃吗', {icon: 3, title: '提示信息'}, function (index, row) {
                $.post("/patent/annual/fee/abandon.html", {
                    patentId: data.patentId
                }, function (data) {
                    layer.close(index);
                    if (data.code == 0) {
                        location.reload();
                    } else {
                        layer.msg(data.msg);
                    }
                })
            });
        }
    });

    // 点击共享人
    $(document).on('click', '.share-user', function () {
        var userId = $(this).attr("userId");
        var certificationName = $(this).attr("certificationName");
        $.post("/certification/detailByUserId.html", {
            userId: userId
        }, function (res) {
            if (res.code == 0) {
                var data = res.data;
                layui.layer.open({
                    title: certificationName + "-实名认证详细信息",
                    type: 2,
                    area: ['100%', '100%'],
                    content: "/certification/detailPage.html",
                    success: function (layero, index) {
                        var body = layui.layer.getChildFrame('body', index);
                        var iframeWin = layero.find('iframe')[0].contentWindow;
                        body.find("#id").val(data.id);
                        var orgType = data.orgType;
                        if (orgType == 1) {
                            body.find("#orgType").html("公司企业");
                            body.find("#certificateFileTd").html("企业营业执照：");
                            body.find("#certificateImg").attr("src", data.certificatePath);
                            body.find("#openingPermitImg").attr("src", data.openingPermitPath);
                            body.find(".company").show();
                        } else if (orgType == 2) {
                            body.find("#orgType").html("事业单位");
                            body.find("#certificateFileTd").html("事业单位法人证书：");
                            body.find("#certificateImg").attr("src", data.certificatePath);
                            body.find("#openingPermitImg").attr("src", data.openingPermitPath);
                            body.find(".company").show();
                        } else if (orgType == 3) {
                            body.find("#orgType").html("机关团体");
                            body.find("#certificateFileTd").html("机关团体法人证书：");
                            body.find("#certificateImg").attr("src", data.certificatePath);
                            body.find("#openingPermitImg").attr("src", data.openingPermitPath);
                            body.find(".company").show();
                        } else if (orgType == 4) {
                            body.find("#orgType").html("个人");
                            body.find("#identityCardFrontImg").attr("src", data.identityCardFrontPath);
                            body.find("#identityCardBackImg").attr("src", data.identityCardBackPath);
                            body.find("#bankFrontImg").attr("src", data.bankFrontPath);
                            body.find("#bankBackImg").attr("src", data.bankBackPath);
                            body.find(".person").show();
                        } else if (orgType == 5) {
                            body.find("#orgType").html("个体工商户");
                            body.find("#identityCardFrontImg").attr("src", data.identityCardFrontPath);
                            body.find("#identityCardBackImg").attr("src", data.identityCardBackPath);
                            body.find("#bankFrontImg").attr("src", data.bankFrontPath);
                            body.find("#bankBackImg").attr("src", data.bankBackPath);
                            body.find(".person").show();
                        } else if (orgType == 6) {
                            body.find("#orgType").html("其他");
                            body.find("#attachmentImg").attr("src", data.attachmentPath);
                            body.find("#bankFrontImg").attr("src", data.bankFrontPath);
                            body.find("#bankBackImg").attr("src", data.bankBackPath);
                            body.find(".other").show();
                        }
                        body.find("#userName").html(data.userName);
                        body.find("#certificateNo").html(data.certificateNo);
                        body.find("#name").html(data.name);
                        body.find("#address").html(data.address);
                        body.find("#bank").html(data.bank);
                        //body.find("#accountHolder").val(data.accountHolder);
                        body.find("#accountNo").html(data.accountNo);
                        body.find("#contactPerson_name").html(data.contactPerson.name);
                        body.find("#contactPerson_phoneNo").html(data.contactPerson.phoneNo);
                        body.find("#contactPerson_email").html(data.contactPerson.email);
                        body.find("#contactPerson_address").html(data.contactPerson.address);
                        body.find("#mark").html(data.mark);
                        if (data.status == 1) {
                            body.find(".btn").show();
                        } else {
                            body.find("#mark").attr("readonly", "readonly");
                        }
                        setTimeout(function () {

                        }, 3000);
                    }
                });
            }
        })
    });

    //导出excel表格
    $(".getCheckData").on("click", function () { //获取选中数据
        var data = table.checkStatus('annualFeeList').data;
        var statisticsStatus = $('#statisticsStatus').val();
        var condition,index;
        if(statisticsStatus !== null && statisticsStatus !== '') {
            condition = {statisticsStatus: statisticsStatus}
        } else {
            condition = buildSearchCondition();
        }
        if (data.length === 0) {
            //为选择数据，导出全部
            $.ajax({
                url: "/patent/annual/fee/exportAll.html",
                type: "post",
                data: condition,
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60*60*1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
            return false;
        } else if (data.length !== 0) {
            var arrTableData = JSON.parse(JSON.stringify(data));
            var ids = '';
            for (var i = 0; i < arrTableData.length; i++) {
                ids += arrTableData[i].id + ",";
            }
            $.ajax({
                url: "/patent/annual/fee/excel.html",
                type: "post",
                data: {"ids": ids},
                beforeSend: function () {
                    index = layer.msg('表格导出中，请稍后...', {
                        icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false,
                        offset: 'auto',
                        time: 60*60*1000
                    })
                },
                success: function (res) {
                    if (res.code === 0) {
                        $('#excelPath').val(res.data);
                        $("#downloadExcelForm").submit();
                        layer.close(index);
                        parent.layer.msg("表格导出成功！");
                    } else {
                        lay.msg("导出失败！");
                    }
                }
            });
        }
    });
});