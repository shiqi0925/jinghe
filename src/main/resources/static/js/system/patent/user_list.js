layui.use(['form', 'layer', 'table', 'element', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table,
        laydate = layui.laydate,
        element = layui.element;

    // 标签表格
    table.render({
        elem: '#userPatentList',
        url: '/user/patent/list.html',
        method: 'post',
        where: {statisticsStatus: $('#navigationStatisticsStatus').val()},
        height: "full-140",
        id: "userPatentList",
        page: true,
        limits: [20, 50, 100],
        limit: 20,
        cols: [[
            {align: 'center', type: 'checkbox'},
            {align: 'center', type: 'numbers', title: '序号'},
            {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
            {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
            {field: 'lprs', title: '授权日/法律状态', align: "center", templet: '#lprsBar'},
            {field: 'appPerson', title: '申请人/发明人', width: 240, align: "center", templet: '#appPersonBar'},
            {field: 'proxyOrg', title: '代理机构', width: 220, align: "center", templet: '#proxyBar'},
            {field: 'address', title: '详细地址', align: "center", templet: '#addressBar'},
        ]]
    });



    function reload() {
        table.render({
            elem: '#userPatentList',
            url: '/user/patent/list.html',
            method: 'post',
            where: {keyword: $('#keyword').val()},
            height: "full-140",
            id: "userPatentList",
            page: true,
            limits: [20, 50, 100],
            limit: 20,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
                {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
                {field: 'lprs', title: '授权日/法律状态', align: "center", templet: '#lprsBar'},
                {field: 'appPerson', title: '申请人/发明人', width: 240, align: "center", templet: '#appPersonBar'},
                {field: 'proxyOrg', title: '代理机构', width: 220, align: "center", templet: '#proxyBar'},
                {field: 'address', title: '详细地址', align: "center", templet: '#addressBar'},
            ]]
        });
    }

    //搜索
    $(".search_btn").on("click", function () {
        $('.status').removeClass("hover");
        reload();
    });

    // 点击统计查询
    $(".status").on("click", function () {
        $('.status').removeClass("hover");
        $(this).addClass('hover');
        $("#keyword").val('');
        form.render();
        var statisticsStatus = $(this).attr("status");
        table.render({
            elem: '#userPatentList',
            url: '/user/patent/list.html',
            method: 'post',
            where: {statisticsStatus: statisticsStatus},
            height: "full-140",
            id: "userPatentList",
            page: true,
            limits: [20, 50, 100],
            limit: 20,
            cols: [[
                {align: 'center', type: 'checkbox'},
                {align: 'center', type: 'numbers', title: '序号'},
                {field: 'appNo', title: '专利类型/申请号/专利名称', width: 240, align: "center", templet: '#appNoBar'},
                {field: 'appDate', title: '申请日/公开日', width: 120, align: "center", templet: '#appDateBar'},
                {field: 'lprs', title: '授权日/法律状态', align: "center", templet: '#lprsBar'},
                {field: 'appPerson', title: '申请人/发明人', width: 240, align: "center", templet: '#appPersonBar'},
                {field: 'proxyOrg', title: '代理机构', width: 220, align: "center", templet: '#proxyBar'},
                {field: 'address', title: '详细地址', align: "center", templet: '#addressBar'},
            ]]
        });
    })

    // 列表操作
    table.on('tool(userPatentList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'delete') {
            layer.confirm('确定删除该专利吗', {icon: 3, title: '提示信息'}, function (index, row) {
                $.post("/user/patent/delete.html", {
                    patentId: data.id
                }, function (data) {
                    if (data.code == 0) {
                        reload();
                        layer.close(index);
                    }
                })
            });
        }
    });
});