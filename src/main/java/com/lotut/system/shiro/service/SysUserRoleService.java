package com.lotut.system.shiro.service;

import com.lotut.system.shiro.domain.SysUserRole;

import java.util.List;

/**
 * @author Administrator
 */
public interface SysUserRoleService {
    /**
     * 批量保存用户角色
     *
     * @param list
     */
    int batchSave(List<SysUserRole> list);

    /**
     * 删除用户角色
     *
     * @param userId 用户id
     */
    int removeByUserId(Long userId);
    /**
     * 保存用户角色
     * @param role
     * @return
     */
    int save(SysUserRole role);
}
