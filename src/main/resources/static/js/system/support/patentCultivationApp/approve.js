layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;
    //通过
    form.on('submit(success)', function (data) {
        $.ajax({
            url: "/patentCultivationApp/approve.html",
            type: "post",
            data: {"id": $('#id').val(), "supportStatus": 2},
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('已通过！', {icon: 1, time: 1000, shade: 0.4});
                    $(window.parent.document).find("#span1").text(parseInt($(window.parent.document).find("#span1").text()) - 1);
                    $(window.parent.document).find("#span2").text(parseInt($(window.parent.document).find("#span2").text()) + 1);
                    closePage();
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    //驳回
    form.on('submit(pass)', function (data) {
        $.ajax({
            url: "/patentCultivationApp/approve.html",
            type: "post",
            data: {"id": $('#id').val(), "supportStatus": 3},
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('已驳回！', {icon: 1, time: 1000, shade: 0.4});
                    $(window.parent.document).find("#span1").text(parseInt($(window.parent.document).find("#span1").text()) - 1);
                    $(window.parent.document).find("#span3").text(parseInt($(window.parent.document).find("#span3").text()) + 1);
                    closePage();
                } else {
                    setTimeout(function () {
                        top.layer.msg(res.msg);
                    }, 2000);
                }
            },
        });
        return false;
    });

    //取消
    $("#cancel_btn").click(function () {
        closePage();
    });

    function closePage() {
        var index = parent.layer.getFrameIndex(window.name); /* 先得到当前iframe层的索引 */
        parent.layui.table.reload('patentCultivationAppList', {page: {curr: $(".layui-laypage-em").next().html()}});   //主要代码
        parent.layer.close(index); //再执行关闭
    }
});