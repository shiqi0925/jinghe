package com.lotut.news.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.lotut.news.domain.Flink;
import com.lotut.news.domain.Seo;


@Mapper
@Repository
public interface FlinkMapper {

    List<Flink> selectList();

    Flink getById(long id);
    
    boolean save(Flink flink);
    boolean add(Flink flink);
}