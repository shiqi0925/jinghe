package com.lotut.news.domain;

import java.io.Serializable;
import java.util.List;


/**
 * 用户网站频道实体
 *
 * @author Administrator
 */
public class WebsiteNewsChannel implements Serializable , Comparable<WebsiteNewsChannel>{
    private static final long serialVersionUID = -2436430193640205772L;
    /**
     * 自增主键
     */
    private Long id;
    /**
     * 用户网站关联id
     */
    private Long userWebsiteId;
    
    private Long websiteId;
    /**
     * 频道名称
     */
    private String name;
    /**
     * 父频道id，默认0
     */
    private Long parentChannelId;
    /**
     * 显示状态，false：隐藏，true：显示
     */
    private Boolean status;
    /**
     * 显示顺序
     */
    private Integer displayOrder;
    /**
     * 是否默认，0：否，1：是
     */
    private Boolean defaultFlag;
    
    /**
     * 下级服务集合
     */
    private List<WebsiteNewsChannel> children;

    public WebsiteNewsChannel(Long id, Long userWebsiteId, String name, Long parentChannelId, Boolean status, Integer displayOrder, Boolean defaultFlag,List<WebsiteNewsChannel> children) {
        this.id = id;
        this.userWebsiteId = userWebsiteId;
        this.name = name;
        this.parentChannelId = parentChannelId;
        this.status = status;
        this.displayOrder = displayOrder;
        this.defaultFlag = defaultFlag;
        this.children = children;
    }

    public WebsiteNewsChannel() {
        super();
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserWebsiteId() {
        return userWebsiteId;
    }

    public void setUserWebsiteId(Long userWebsiteId) {
        this.userWebsiteId = userWebsiteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getParentChannelId() {
        return parentChannelId;
    }

    public void setParentChannelId(Long parentChannelId) {
        this.parentChannelId = parentChannelId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Boolean getDefaultFlag() {
        return defaultFlag;
    }

    public void setDefaultFlag(Boolean defaultFlag) {
        this.defaultFlag = defaultFlag;
    }
    
    public List<WebsiteNewsChannel> getChildren() {
		return children;
	}

	public void setChildren(List<WebsiteNewsChannel> children) {
		this.children = children;
	}

	@Override
    public int compareTo(WebsiteNewsChannel o) {
        int result = o.getDisplayOrder().compareTo(this.getDisplayOrder());
        if(result == 0){
            return o.getId().compareTo(this.getId());
        }
        return result;
    }

	public Long getWebsiteId() {
		return websiteId;
	}

	public void setWebsiteId(Long websiteId) {
		this.websiteId = websiteId;
	}


	
}