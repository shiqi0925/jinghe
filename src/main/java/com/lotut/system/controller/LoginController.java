package com.lotut.system.controller;

import com.lotut.common.dto.ReturnResult;
import com.lotut.system.service.UserService;
import com.lotut.system.shiro.util.ShiroUtil;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwj
 * @date 2020/3/10
 */
@RequestMapping(path = "/user")
@Controller
public class LoginController {
    @Autowired
    private UserService userService;
    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/loginApi")
    @ResponseBody
    public ReturnResult login(String username, String password, HttpServletRequest request) {
    	String domainUrl = request.getServerName();
    	System.out.print("来源域名："+domainUrl+"--");
        //return userService.login(username, password);
        return userService.websiteLogin(username, password, domainUrl);
    }

 

}
