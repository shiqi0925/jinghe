package com.lotut.system.controller;

import com.lotut.common.constants.WebsiteConstants;
import com.lotut.common.dto.ReturnResult;
import com.lotut.system.constants.SysSettingConstants;
import com.lotut.system.domain.User;
import com.lotut.system.dto.UserDto;
import com.lotut.system.service.UserService;
import com.lotut.system.shiro.domain.SysUserRole;
import com.lotut.system.shiro.enums.RoleEnum;
import com.lotut.system.shiro.service.SysUserRoleService;
import com.lotut.system.shiro.util.BcryptPasswordUtil;
import com.lotut.system.shiro.util.ShiroUtil;
import com.lotut.website.domain.Website;
import com.lotut.website.service.WebsiteService;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * 用户控制层
 *
 * @author xwj
 * @date 2020/3/6
 */
@RequestMapping("/user")
@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private WebsiteService websiteService;
    @Autowired
    private SysUserRoleService sysUserRoleService;    
    /**
     * 跳转到登录页
     *
     * @return
     */
    @GetMapping("/login.html")
    public String loginPage() {
        return "user/login";
    }

    /**
     * 跳转到用户注册页面
     * @return
     */
    @GetMapping("/register.html")
    public String registerPage() {
        return "user/register";
    }

    /**
     * 跳转到注册成功页面
     * @return
     */
    @GetMapping("/register/success")
    public String registerSuccessPage() {
        return "user/register_success";
    }

    /**
     * 跳转到找回密码页面
     * @return
     */
    @GetMapping("/pass/reset")
    public String passResetPage() {
        return "user/reset_pass";
    }

    /**
     * 跳转到重置成功页面
     * @return
     */
    @GetMapping("/pass/reset/success")
    public String passResetSuccessPage() {
        return "user/reset_pass_success";
    }

    /**
     * 管理员菜单
     */
    @Value("classpath:menu/menu.json")
    private Resource menu;

    /**
     * 跳转到后台管理页面
     *
     * @return
     */
    @GetMapping("/index")
    public String login(Model model) {
        model.addAttribute("user", ShiroUtil.getUser());
        return "system/index";
    }

    /**
     * 获取用户后台管理菜单
     */
    @RequestMapping(path = "/menu")
    @ResponseBody
    public String menu() throws IOException {
        // 判断用户角色
        return IOUtils.toString(menu.getInputStream(), Charset.forName("UTF-8"));
    }

    /**
     * 后台管理主页
     */
    @RequestMapping(path = "/index/main")
    public String main(){
        return "system/main";
    }

    /**
     * 注册用户
     * @param userDto
     * @return
     */
    @PostMapping("/save")
    @ResponseBody
    public ReturnResult save(@Valid UserDto userDto,HttpServletRequest request) {
        // 判断用户名是否存在
        User record = userService.findByUsername(userDto.getUsername());
        if (record != null) {
            return ReturnResult.errorResult("用户名已存在");
        }    	
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        String domainUrl = request.getServerName();
        System.out.print("注册来源："+domainUrl);
    	Website website = websiteService.selectByDomainUrl(domainUrl);
    	System.out.print("注册站点id："+website.getRecordId());
        user.setWebsiteId(website.getRecordId());
        user.setVisiblePassword(user.getPassword());
        user.setPassword(BcryptPasswordUtil.encode(user.getPassword()));
        user.setSources(domainUrl);
        if(userService.save(user) == 1) {
        	System.out.print("新注册的用户的id："+user.getUserId());
        	//分配角色
            SysUserRole role = new SysUserRole(user.getUserId(), Long.valueOf(RoleEnum.SYS_NORMAL_USER.getRoleId()));
            sysUserRoleService.save(role);
        	
            return ReturnResult.successResult();
        }
        
        return ReturnResult.errorResult();
        
        
    }
    
    /**
     * 验证是否重复
     */
    @PostMapping("/validate")
    @ResponseBody
    public ReturnResult validate(String username) {
        User user = userService.findByUsername(username);
        if (user == null) {
            return ReturnResult.successResult();
        }
        return ReturnResult.errorResult();
    }
    /**
     * 退出
     */
/*    @RequiresAuthentication*/
    @GetMapping("/logout")
    public String sysLogout() {
    	System.out.print("进入退出登录");
        ShiroUtil.logout();
        return "redirect:/user/login";
    } 
    
}
