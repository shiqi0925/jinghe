package com.lotut.system.shiro.controller;

import com.lotut.common.dto.ReturnResult;
import com.lotut.system.shiro.domain.SysRole;
import com.lotut.system.shiro.service.SysRoleService;
import com.lotut.system.shiro.util.ShiroUtil;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 系统角色控制层
 *
 * @author Administrator
 */
@RequestMapping("/sys/role")
@Controller
public class SysRoleController {
    private String prefix = "system/role/";

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 跳转到角色列表页面
     *
     * @return
     */
    @RequiresRoles("sys_admin")
    @GetMapping("/list")
    public String list() {
        return prefix + "list";
    }

    /**
     * 获取角色列表
     */
    @RequiresRoles("sys_admin")
    @PostMapping(value = "/list")
    @ResponseBody
    public ReturnResult getSysRoleList() {
        List<SysRole> list = sysRoleService.list();
        return ReturnResult.successResult(list);
    }

    /**
     * 跳转到角色添加页面
     *
     * @return
     */
    @RequiresRoles("sys_admin")
    @GetMapping("/add_page")
    public String addPage() {
        return prefix + "add";
    }

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    @RequiresRoles("sys_admin")
    @PostMapping(value = "/add")
    @ResponseBody
    public ReturnResult addRole(SysRole role) {
        role.setCreateUser(ShiroUtil.getUserId());
        role.setUpdateUser(ShiroUtil.getUserId());
        int result = sysRoleService.save(role);
        if (result == 1) {
            return ReturnResult.successResult();
        }
        return ReturnResult.errorResult();
    }

    /**
     * 删除角色
     */
    @RequiresRoles("sys_admin")
    @GetMapping(value = "/delete")
    @ResponseBody
    public ReturnResult deleteById(Long roleId) {
        boolean result = sysRoleService.deleteById(roleId);
        if (result) {
            return ReturnResult.successResult();
        }
        return ReturnResult.errorResult();
    }

    /**
     * 跳转到角色编辑页面
     *
     * @return
     */
    @RequiresRoles("sys_admin")
    @GetMapping("/update_page")
    public String updatePage() {
        return prefix + "update";
    }

    /**
     * 更新角色信息
     *
     * @param role
     * @return
     */
    @RequiresRoles("sys_admin")
    @PostMapping(value = "/update")
    @ResponseBody
    public ReturnResult updateRole(SysRole role) {
        role.setUpdateUser(ShiroUtil.getUserId());
        int result = sysRoleService.update(role);
        if (result == 1) {
            return ReturnResult.successResult();
        }
        return ReturnResult.errorResult();
    }
}
