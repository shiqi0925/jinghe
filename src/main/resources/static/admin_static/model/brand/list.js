  layui.config({
    base: '/admin_static/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'layer', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;
    var statusStr=$('#statusStr').val();            
    //代理机构列表
    var tableIns = table.render({
        elem: '#contentList',
        url: '/admin/brand/brandList',
        method: 'post',
        cellMinWidth: 95,
        page: true,
        where: {
            keyword: $("#keyword").val(),
            tmCategoryNo:$('.tmCategoryNo').val(),
            sellStatus:$('.sellStatus').val(),
            isTianmao:$('.isTianmao').val(),
        },
        height: "full-125",
        limits: [20, 50, 100],
        limit: 20,
        id: "contentList",
        cols: [[
        	{type: 'checkbox', fixed: 'left'},
            {align: 'center', type: 'numbers', title: '编号'},
            {field: 'brandNo', title: '申请号',width: 100, align: "center"},
            {field: 'tmName', title: '商标名',width: 100, align: "center",templet: function (d) {
            	return d.brandDetail.tmName;
            	}
            }, 
            {field: 'categoryText', title: '商标分类', width:110,align: "center"}, 
            {field: 'brandDetail', title: '交易状态',width: 130, align: "center",templet: function (d) {
            	//以select出现，并可以修改状态
            	var status=$.parseJSON(statusStr)
                var optionHtml='';
                for(var i=0;i<status.length;i++){
                 	if(status[i].id==d.sellStatus){
                 		optionHtml+='<option selected value="'+status[i].id+'" title="' + d.sellStatus + '" >'+status[i].name+'</option>';
                 	}else{
                 		optionHtml+='<option value="'+status[i].id+'" title="' + d.sellStatus + '" >'+status[i].name+'</option>';
                 	}
                 }
                return '<select name="sellStatus" data-brandno="'+d.brandNo+'" lay-verify="" lay-filter="selectSellStatus" >'+optionHtml+'</select>';
        		}
            },
            {field: 'price', title: '价格',width: 100, align: "center",edit: 'text'},
            {field: 'remark', title: '备注', align: "center",edit: 'text'},
            {field: 'isTianmao', title: '天猫记录',width: 100, align: "center",templet: '#switchTianmao', unresize: true},
            {title: '操作', minWidth: 150, templet: '#newsListBar', fixed: "right", align: "center"}
        ]],
        done:function (res,curr,count){
        	form.render('select');
        }
        
    });

    //监听单元格编辑
    table.on('edit(contentList)', function(obj){
      var value = obj.value //得到修改后的值
      ,data = obj.data //得到所在行所有键值
      ,field = obj.field; //得到字段
      //layer.msg('[ID: '+ data.brandNo +'] ' + field + ' 字段更改为：'+ value);
      changeSingleField(data.brandNo,field,value)
    });

    // 刷新表格
    function reload() {
        table.reload("contentList", {
            page: {
                curr: 1
            },
            where: {
                keyword: $("#keyword").val(),
                tmCategoryNo:$('.tmCategoryNo').val(),
                sellStatus:$('.sellStatus').val(),
                isTianmao:$('.isTianmao').val(),
            }
        })
    }

    //搜索
    $(".search_btn").on("click", function () {
        reload();
    });
    $(".export_btn").on("click", function () {
    	console.log("进入方法")
    	var keyword = $("#keyword").val();
    	var tmCategoryNo = $('.tmCategoryNo').val();
    	var sellStatus = $('.sellStatus').val();
    	var isTianmao = $('.isTianmao').val();
    	var date = new Date();
    	location.href = "/admin/brand/exportExcel?keyword=" + keyword+"&tmCategoryNo="+tmCategoryNo+"&sellStatus="+sellStatus+"&isTianmao="+isTianmao+"&date="+date;
    });
    form.on('select(selectSellStatus)', function (obj) {
        var elem = $(obj.elem);
        var trElem = elem.parents('tr');
        var brandNo = elem.attr("data-brandno")
        changeSingleField(brandNo,"sellStatus",obj.value)
    })
    
    //监听天猫操作
    form.on('switch(tianmao)', function(obj){
      //layer.tips(this.value + ' ' + this.name + '：'+ obj.elem.checked, obj.othis);
    	var elem = $(obj.elem);
    	var brandNo = elem.attr("data-brandno")
    	var isTianmao=0;
    	if(obj.elem.checked ==true){
    		isTianmao=1;
    	}
    	changeSingleField(brandNo,"isTianmao",isTianmao);
    });
    
    //传字段 修改
    function changeSingleField(brandNo,field,value){
    	console.log("进入 changeSingleField");
    	var data = {};
    	data[field]=value;
    	data["brandNo"]=brandNo
        $.ajax({
            url: "/admin/brand/updateBrand",
            type: "post",
            data: data,
            success: function (data) {
                if (data.code == 0) {
                	layer.open({
                		  title: '提示'
                		  ,content: '修改成功'
                		});   
                }
            }
        });
    	
    }
    
    

});